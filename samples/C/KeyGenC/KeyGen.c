//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

// This sample demonstrates product key generation and validation using the C++ API interface

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "licensing.h"

#define MAX_CUSTOMER_NAME_LEN 32
#define MAX_HARDWARE_ID_LEN   32

#define MAX_XML_TEMPLATE 16384
char xmlTemplate[ MAX_XML_TEMPLATE ];

int _tmain(int argc, _TCHAR * argv[])
{
	// Create and setup a license key template
	void * tmpl, * validator, * generator;
	size_t len;
	const char_t * licenseKey;
	int productId = 0x12;
	const char_t * hwid;
	int year, month, day;
	char * xmlTemplate;
	int err;

	// Entering the purchased developer key is the first step in using the SDK. 
	// The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
	// Replace the demo key with your purchased key.
	SDKRegistration_SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD"));

	if ((tmpl = KeyTemplate_Create()) == NULL)
		return 1;

	// A license key template file was not provided on the command line, 
	// so programatically setup a template to show library usage

	// Setup the license key appearance as XXXXXX-XXXXXX-XXXXXX-XXXXXX-XXXXXX-XXXXXX
	KeyTemplate_SetCharactersPerGroup(tmpl, 6);
	KeyTemplate_SetNumberOfGroups(tmpl, 6);
	KeyTemplate_SetGroupSeparator(tmpl, _T("-"));
	KeyTemplate_SetEncoding(tmpl, ENCODING_BASE32X);
	KeyTemplate_SetSignatureSize(tmpl, 158);
	KeyTemplate_SetDataSize(tmpl, 22);
	KeyTemplate_AddDataField(tmpl, _T("ProductId"), FIELD_TYPE_INTEGER, 8, 0);
	KeyTemplate_AddDataField(tmpl, _T("ExpirationDate"), FIELD_TYPE_DATE14, 14, 8);
	KeyTemplate_SetValidationDataSize(tmpl, (MAX_CUSTOMER_NAME_LEN + MAX_HARDWARE_ID_LEN) * 8);
	KeyTemplate_AddValidationField(tmpl, _T("Name"), FIELD_TYPE_STRING, 8 * MAX_CUSTOMER_NAME_LEN, 0);
	KeyTemplate_AddValidationField(tmpl, _T("HardwareId"), FIELD_TYPE_STRING, 8 * MAX_HARDWARE_ID_LEN, 8 * MAX_CUSTOMER_NAME_LEN);

	_tprintf(_T("Generating a sample private/public key pair for license key generation/validation..."));
	if ((err = KeyTemplate_GenerateSigningKeyPair(tmpl)) != STATUS_SUCCESS)
	{
		_tprintf(_T("error ! (code %d). Please contact SoftActivate support"), err);
	}
	_tprintf(_T("done.\n"));

	generator = KeyGenerator_Create();
	KeyGenerator_SetKeyTemplate(generator, tmpl);
	KeyGenerator_SetIntKeyData(generator, _T("ProductId"), productId);
	KeyGenerator_SetDateKeyData(generator, _T("ExpirationDate"), 2014, 2, 30);
	
	KeyGenerator_SetStringValidationData(generator, _T("Name"), _T("John Doe"));
	KeyHelper_GetCurrentHardwareId(&hwid);
	KeyGenerator_SetStringValidationData(generator, _T("HardwareId"), hwid);

	KeyGenerator_GenerateKey(generator, &licenseKey);

	validator = KeyValidator_Create();
	KeyValidator_SetKeyTemplate(validator, tmpl);
	KeyValidator_SetStringValidationData(validator, _T("Name"), _T("John Doe"));
	KeyValidator_SetStringValidationData(validator, _T("HardwareId"), hwid);
	KeyValidator_SetKey(validator, licenseKey);

	if (KeyValidator_IsKeyValid(validator) == STATUS_SUCCESS)
	{
		_tprintf(_T("The license key %s is valid.\n"), licenseKey);

		// Now that you know the key is valid, retrieve the key stored data
		len = sizeof(productId);
		KeyValidator_QueryKeyData(validator, _T("ProductId"), &productId, (unsigned int *)&len);
		_tprintf(_T("The ProductId field has a value of 0x%02X\n"), productId);

		KeyValidator_QueryDateKeyData(validator, _T("ExpirationDate"), &year, &month, &day);
		_tprintf(_T("The expiration date is %02d/%02d/%04d\n"), month, day, year);

	} else
		_tprintf(_T("The license key %s is NOT valid !"), licenseKey);

	_tprintf(_T("Press any key..."));
	getc(stdin);

	KeyValidator_Destroy(validator);
	KeyTemplate_Destroy(tmpl);

	return 0;
}

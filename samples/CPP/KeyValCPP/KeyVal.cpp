﻿//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

// This sample demonstrates product key validation

#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <exception>

#include "licensing.h"

using namespace SoftActivate::Licensing;

#define MAX_XML_TEMPLATE 16384
char xmlTemplate[ MAX_XML_TEMPLATE ];

LPCTSTR GenerateSampleLicenseKey(KeyTemplate * tmpl)
{
	tmpl->SetPrivateKey(_T("AQnD07PnEw7CRi8="));

	static TCHAR sampleLicenseKey[1000];

	KeyGenerator * keyGenerator = KeyGenerator::Create();
	keyGenerator->SetKeyTemplate(tmpl);

	// the data below is embedded into the license key
	keyGenerator->SetKeyData(_T("ProductId"), 777);
	keyGenerator->SetKeyData(_T("EnabledProductFeatures"), 0x3);

	// the data below is NOT embedded into the license key, but it is required at validation time.
	// if this data does not match at validation time, the key validation will fail
	keyGenerator->SetValidationData(_T("RegistrationName"), _T("John Doe"));

	LPCTSTR key = keyGenerator->GenerateKey();

	_tcscpy_s(sampleLicenseKey, 1000, key);

	return sampleLicenseKey;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Create and setup a license key template
	const char_t * licenseKey;

	KeyGenerator * keyGenerator = NULL;
	KeyValidator * keyValidator = NULL;
	KeyTemplate * keyTemplate = NULL;

	do {
		try {
			// Entering the purchased developer key is the first step in using the SDK for license key generation and private/public key pair generation
			// Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
			// where you will only validate license keys. This call is only needed for license key generation and public/private key pair creation
			// You will only call this method in your key generator application, and you will set it in the configuration file of the licensing service

			// The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
			// Replace the demo key with your purchased key.
			SDKRegistration::SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD")); //VCMDJ-L6PU6-ULWGV-5EPSP-24TNP

			keyTemplate = KeyTemplate::Create();

			// below are some license key format samples. Uncomment the one that suits your application

			keyTemplate->SetCharactersPerGroup(5);
			keyTemplate->SetNumberOfGroups(6);
			keyTemplate->SetGroupSeparator(_T("-"));
			keyTemplate->SetEncoding(ENCODING_BASE32X); // 5 bits per character
			keyTemplate->SetSignatureSize(128); // We chose a data size of 22 bits (16 for product id, 6 for product features, see below), so we have 128 left for signature: 128 = ((5 * 6) * 5) - 22

			//
			// These are some examples of alternative license key formats that you can use
			//

			//keyTemplate->SetCharactersPerGroup(5);
			//keyTemplate->SetNumberOfGroups(6);
			//keyTemplate->SetGroupSeparator(_T("-"));
			//keyTemplate->SetEncoding(ENCODING_BASE32X); // 5 bits per character
			//keyTemplate->SetSignatureSize(128); // We chose a data size of 22 bits, so we have 128 left for signature: 128 = ((5 * 6) * 5) - 22

			//keyTemplate->SetCharactersPerGroup(6);
			//keyTemplate->SetNumberOfGroups(6);
			//keyTemplate->SetGroupSeparator(_T("-"));
			//keyTemplate->SetEncoding(ENCODING_BASE32X);
			//keyTemplate->SetSignatureSize(158);

			// the public key certificate (which is a public key and some additional information) is used for license key validation
			// this certificate is included in your actual software that reaches the customers
			keyTemplate->SetPublicKeyCertificate(_T("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA="));

			keyTemplate->SetDataSize(22);
			keyTemplate->AddDataField(_T("ProductId"), // field name
										FIELD_TYPE_INTEGER, // field type (integer, string or raw)
										16, // field data size in bits 
										0   // (optional) field start position in the sequence of bits forming key data
										);
			keyTemplate->AddDataField(_T("EnabledProductFeatures"), FIELD_TYPE_INTEGER, 6, 16);

			// Validation fields are optional. They are fields that are not stored in the license key, but are used for validation
			// This is very handy for tying the key to the hardware id or user name, thus preventing illegal distribution of a particular key on warez sites
			keyTemplate->SetValidationDataSize(100 * 8);
			keyTemplate->AddValidationField(_T("RegistrationName"), FIELD_TYPE_STRING, 100 * 8, 0);

			// generate a sample license key
			licenseKey = GenerateSampleLicenseKey(keyTemplate);

			// now validate it
			keyValidator = KeyValidator::Create();
			keyValidator->SetKeyTemplate(keyTemplate);

			keyValidator->SetValidationData(_T("RegistrationName"), _T("John Doe"));

			keyValidator->SetKey(licenseKey);

			if (keyValidator->IsKeyValid())
			{
				_tprintf(_T("The license key %s for user 'John Doe' is valid.\n"), licenseKey);

				// Now that you know the key is valid, retrieve the key stored data
				try {
					int productId = 0;
					int len = sizeof(productId);
					keyValidator->QueryKeyData(_T("ProductId"), &productId, &len);
					_tprintf(_T("The ProductId field has a value of 0x%04X\n"), productId);
				} catch(...)
				{

				}

			} else
				_tprintf(_T("The license key %s is NOT valid !"), licenseKey);
		}
		catch (Exception * ex)
		{
			_tprintf(_T("License Key Exception: %d (%s)"), ex->GetCode(), ex->GetExceptionMessage());
			ex->Destroy();
			return -1;
		}
		catch (...)
		{
			_tprintf(_T("General exception caught !\n"));
			return -1;
		}
	} while (0);

	if (keyGenerator) KeyGenerator::Destroy(keyGenerator);
	if (keyValidator) KeyValidator::Destroy(keyValidator);
	if (keyTemplate) KeyTemplate::Destroy(keyTemplate);

	_tprintf(_T("Press any key..."));
	getc(stdin);

	return 0;
}

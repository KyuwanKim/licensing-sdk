// HardwareIdCPP.cpp : Defines the entry point for the console application.
//

#include <tchar.h>
#include <stdio.h>
#include "licensing.h"

int _tmain(int argc, _TCHAR* argv[])
{
	try {
		const _TCHAR * hwid = KeyHelper::GetCurrentHardwareId();
		_tprintf(_T("This computer's hardware id is %s\n"), hwid);

		if (KeyHelper::MatchCurrentHardwareId(hwid))
		{
			_tprintf(_T("The hardware id '%s' matches this computer\n"), hwid);
		} else
		{
			_tprintf(_T("The hardware id '%s' does not match this computer !\n"), hwid); 
		}
	} catch (Exception * ex)
	{
		_tprintf(_T("Exception caught !"));
		ex->Destroy();
	}

	getc(stdin);

	return 0;
}

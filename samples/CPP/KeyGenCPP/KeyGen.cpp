//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

// This sample demonstrates product key generation and validation using the C++ API interface

#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include "licensing.h"

#define PRODUCT_ID 777

#define FEATURE_1 0x1
#define FEATURE_2 0x2
#define FEATURE_3 0x4
#define FEATURE_4 0x8
#define FEATURE_5 0x10
#define FEATURE_6 0x20
#define FEATURE_7 0x40
#define FEATURE_8 0x80

int _tmain(int argc, _TCHAR* argv[])
{
	int result = 0;

	try 
	{
		// Entering the purchased developer key is the first step in using the SDK for license key generation and private/public key pair generation
		// Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
		// where you will only validate license keys. This call is only needed for license key generation and public/private key pair creation
		// You will only call this method in your key generator application, and you will set it in the configuration file of the licensing service

		// The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
		// Replace the demo key with your purchased key.
		SDKRegistration::SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD")); // VCMDJ-L6PU6-ULWGV-5EPSP-24TNP

		// this object is used in both the generation and validation process 
		// to specify the license key parameters
		KeyTemplate  tmpl;

		tmpl.SetSigningServiceUrl(_T("http://192.168.1.114/"));

		// encoding can either be ENCODING_BASE32X or ENCODING_BASE64X. 
		// keys encoded with BASE32X contain only uppercase letters and numbers, omitting confusing characters like 0, i, 1, etc.
		// keys encoded with BASE64X are a bit shorter but they are case sensitive and contain punctuation
		tmpl.SetEncoding(ENCODING_BASE32X);

		// number of character groups of the license key. The key XXXX-XXXX-XXXX has 3 character groups
		tmpl.SetNumberOfGroups(6);

		// number of characters in each group. The key XXXX-XXXX-XXXX has 4 characters per group
		tmpl.SetCharactersPerGroup(5);

		// set the signature size. Usually a large portion of the license key is occupied by the signature.
		// the higher the signature size, the more secure the key is. Valid values are between 108 and 322
		tmpl.SetSignatureSize(120); // we want a data size of 16 bits, and the key size is 150 bits (6 * 5 * 5). So we have room for a signature size of 134 bits

		// This specifies the character used to separate the groups
		tmpl.SetGroupSeparator(_T("-"));

		// Each generated key can be preceded by a header line (eg. "--BEGIN KEY--") and followed by a footer line (eg. "--END KEY--")
		// This can be useful for long keys stored in text files. 
		tmpl.SetHeader(_T(""));
		tmpl.SetFooter(_T(""));

		// each license key can carry user-defined data spanning several bits. The data section can be split in data fields.
		tmpl.SetDataSize(30);

		// include an 8-bit field in the license keys, specifying the product id (up to 256 different id's possible in this sample)
		tmpl.AddDataField(_T("ProductId"), 
			              FIELD_TYPE_INTEGER, 
						  8, // field has a size of 8 bits
						  0  // field starts at position 0 in the data bits
						 );
		
		// include an 8-bit field in the license keys, specifying the enabled features (up to 8 features can be marked as enabled/disabled in this sample)
		tmpl.AddDataField(_T("EnabledFeatures"), 
						  FIELD_TYPE_INTEGER, 
						  8, // fiels has a size of 8 bits
						  8  // field starts at position 8 in the data bits (bits 0-7 are used by the product id)
						 );

		// include a 14-bit field representing the product expiration date (between 1/1/2010 and 12/31/2041). 5 bits for day, 4 bits for month, 5 bits for year.
		tmpl.AddDataField(_T("ExpirationDate"), 
						  FIELD_TYPE_DATE14, 
						  14, // fiels has a size of 14 bits
						  16  // field starts at position 16 in the data bits (bits 0-15 are used by the product id and features bit mask)
						 );

        // Below we use a sample private/public key pair for license key generation and validation purposes, respectively. 
        // In production, you need your own private/public key pair because you need to keep your private key secret
        // You can generate a new key pair using the Licensing Tool (included with the SDK),
        // or programatically with KeyTemplate.GenerateSigningKeyPair() and then retrieve the generated pair with KeyTemplate.GetPrivateKey() 
        // and KeyTemplate.GetPublicKeyCertificate()

        // the private key is used for key generation. NEVER include this in the software that reaches your customers, only in your key generators !
        tmpl.SetPrivateKey(_T("AQnD07PnEw7CRi8="));

        // the public key certificate (which is a public key and some additional information) is used for license key validation
        // this certificate is included in your actual software that reaches the customers
        tmpl.SetPublicKeyCertificate(_T("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA="));

		// this object is used to generate license keys
		KeyGenerator keyGen;

		// specify the template for the license key generator. Note that both the private and public keys need to be set in order to successfully generate keys.
		keyGen.SetKeyTemplate(tmpl);

		// specify a product id, for example in order to distinguish between keys for different products 
		// from the same company
		keyGen.SetKeyData(_T("ProductId"), PRODUCT_ID);

		// mark some of the features as enabled
		keyGen.SetKeyData(_T("EnabledFeatures"), FEATURE_1 | FEATURE_5 | FEATURE_7);

		keyGen.SetKeyData(_T("ExpirationDate"), 2014, 12, 17);

		// note that the returned string is a pointer to a static string and must not be deallocated
		const char_t * licenseKey = keyGen.GenerateKey();

		_tprintf(_T("The generated key is: %s\n"), licenseKey);

		//
		// Everything from now on is optional. 
		// Verify the generated license key (it should always be valid)
		//

		// this object is used to validate license keys
		KeyValidator keyVal(&tmpl);

		// validate the generated key. This is just for demonstration only, all the generated keys are valid.
		keyVal.SetKey(licenseKey);

		if (keyVal.IsKeyValid())
		{
			_tprintf(_T("The generated key is: %s\n"), licenseKey);
			
			int year, month, day;
			keyVal.QueryDateKeyData(_T("ExpirationDate"), &year, &month, &day);

			_tprintf(_T("Expiration date is %02d/%02d/%04d\n"), month, day, year);

			int enabledFeatures = keyVal.QueryIntKeyData(_T("EnabledFeatures"));

			for (int i = 0; i < 8; i++)
				// test the bit corresponding to feature i
				if ((enabledFeatures & (1 << i)) != 0)
					_tprintf(_T("Feature %d is enabled\n"), i + 1);
				else
					_tprintf(_T("Feature %d is disabled\n"), i + 1);
		}
		else
		{
			_tprintf(_T("Error creating/validating license key !\n"));
		}
	}
	catch(Exception * ex)
	{
		_tprintf(_T("Exception caught: %d (%s) !\n"), ex->GetCode(), ex->GetExceptionMessage());
		ex->Destroy();
		result = -1;
	}
	catch (...)
	{
		_tprintf(_T("Unknown exception !\n"));
		result = -1;
	}

	_tprintf(_T("\nPress any key..."));
	getc(stdin);
	return 0;
}

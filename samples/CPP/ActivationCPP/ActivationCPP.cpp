// ActivationCPP.cpp : Defines the entry point for the console application.
//

#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <string>
#include <atlbase.h>

#include "licensing.h"

// these product id's are included with the server's database for sample purposes. Replace with your own.
#define SAMPLE_PRODUCT_ID_SIMPLE_KEY                    12345
#define SAMPLE_PRODUCT_ID_WITH_CUSTOMER_NAME_LINKED_KEY 777

#define FEATURE_BASE	0x10
#define FEATURE_PRO		0x20
#define FEATURE_LITE	0x40
#define FEATURE_4 0x8
#define FEATURE_5 0x10
#define FEATURE_6 0x20
#define FEATURE_7 0x40
#define FEATURE_8 0x80

using namespace std;
using namespace SoftActivate::Licensing;

/*
int _tmain(int argc, _TCHAR* argv[])
{
	const char_t * xml;
	const char_t * productKey;
	char_t * hardwareId = _T("");
	char_t * activationKey = _T("");

	try {
		// Entering the purchased developer key is the first step in using the SDK. 
		// The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
		// You if you purchased the SDK, replace this key with your purchased key.
		SDKRegistration::SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD"));

		KeyTemplate tmpl;

		tmpl.SetCharactersPerGroup(5);
		tmpl.SetNumberOfGroups(5);
		tmpl.SetSignatureSize(109);
		tmpl.SetPublicKeyCertificate(_T("AI0bDQELdoxzyMNu7n46whOSAEon0PzzErF7AiD1r/HhLy4U1wA="));
		//tmpl.SetPublicKeyCertificate(_T("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA="));

		// The next paragraph of lines is used to generate a random product key, for demonstration purposes
		// In real world scenarios, the product key is read from product settings so the lines below are not needed
		tmpl.SetDataSize(16);
		tmpl.AddDataField(_T("ProductId"), FIELD_TYPE_INTEGER, 16, 0);
		
		tmpl.SetPrivateKey(_T("AQnD07PnEw7CRi8="));	

		// remove the following 2 lines if your license keys are not linked to customer names
		tmpl.SetValidationDataSize(100 * 8);
		tmpl.AddValidationField(_T("CustomerName"), FIELD_TYPE_STRING, 100 * 8, 0);
		
		
		xml = tmpl.SaveXml();

		KeyGenerator generator(&tmpl);
		generator.SetKeyData(_T("ProductId"), 777);

		// remove the following line if your license keys are not linked to customer names
		generator.SetValidationData(_T("CustomerName"), _T("kkw"));
		productKey = generator.GenerateKey();

		//productKey = _T("BEB9G-F4NP7-YFR4P-LRZTN-5SRLV");

		KeyValidator validator(&tmpl);
		validator.SetKey(productKey);

		// remove the following line if your license keys are not linked to customer names
		validator.SetValidationData(_T("CustomerName"), _T("kkw"));

		if (!validator.IsKeyValid())
		{
			throw std::exception("invalid license key");
		}

		int validationDataLen = 100;
		unsigned char validationData[100];

		validator.QueryValidationData(NULL, validationData, &validationDataLen);

		// the activation process begins here. 
        // NOTE: the activation VALIDATION (not the initial activation) is done completely offline and does not access the activation server !
		LicensingClient licensingClient;

		// SetLicenseKeyValidationData needs to be called before SetActivationKeyTemplate !
		licensingClient.SetLicenseKeyValidationData(validationData, validationDataLen);
		
		licensingClient.SetLicenseKeyTemplate(tmpl);


		productKey = _T("BEB9G-F4NP7-YFR4P-LRZTN-5SRLV");
		//activationKey 등록 할때는 없어야 함 아래 항목 NULL
		hardwareId = _T("DJV46-2GEFC-EBHBB-JU332-LSNAA");
		activationKey = _T("DVEPQ-5PGBT-GM7ZQ-Q6YMR-AB5QU");

		//등록후에는 productKey, hardwareId, activationKey 3가지 항목을 채워 넣어야함


		licensingClient.SetLicensingServiceUrl(_T("http://192.168.1.114/"));
		licensingClient.SetLicenseKey(productKey);
		licensingClient.SetHardwareId(hardwareId);
		licensingClient.SetActivationKey(activationKey);
		licensingClient.SetProductId(777);



		// new in version 3.x : connects to a timeserver and retrieves the time, instead of retrieving the (possibly altered) local time 
		licensingClient.SetTimeValidationMethod(PREFER_INTERNET_TIME); // alternative: USE_INTERNET_TIME or USE_LOCAL_TIME

		if (!licensingClient.IsLicenseValid())
		{
			try {
				_tprintf(_T("Product needs to be activated.\nRequesting license activation from server..."));

				// SoftActivate Licensing SDK includes an activation server which you can install on your own servers or even on shared hosting environments.
				// Below a sample working server URL is provided. You can replace this with your own installed service URL
				//licensingClient.SetLicensingServiceUrl(_T("http://www.softactivate.com/SampleLicensingService/"));
				licensingClient.SetLicensingServiceUrl(_T("http://192.168.1.114/"));
				
				// the license activation consists in obtaining an "activation key" from the activation server, linked to this particular computer's hardware id. 
				licensingClient.AcquireLicense();

				_tprintf(_T("done."));
		    } 
			catch (Exception * ex)
			{
				_tprintf(_T("Exception caught ! Message: \"%s\". Is the activation server started and properly configured (see documentation) ?"), ex->GetExceptionMessage());
				ex->Destroy();
			}
			catch (...)
			{
				_tprintf(_T("Unknown exception during activation attempt !"));
			}

			if (!licensingClient.IsLicenseValid())
			{
				int activationStatus = licensingClient.GetLicenseActivationStatus();
				char_t * activationStatusStr;

				switch (activationStatus)
				{
				case STATUS_INVALID_ACTIVATION_KEY:
					activationStatusStr = _T("activation key is invalid");
					break;

				case STATUS_INVALID_HARDWARE_ID:
					activationStatusStr = _T("hardware id does not match this computer");
					break;

				case STATUS_LICENSE_EXPIRED:
					int year, month, day;
					// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
					// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
					licensingClient.GetLicenseExpirationDate(&year, &month, &day);
					activationStatusStr = _T("license expired");
					break;

				default:
					activationStatusStr = _T("unknonwn");
					break;
				}

				_tprintf(_T("\nProduct could NOT be activated. Reason: %s"), activationStatusStr);
			} else
			{
				_tprintf(_T("\nActivation Successful. The returned activation key is '%s' and the computer's hardware id is '%s'"), licensingClient.GetActivationKey(), licensingClient.GetHardwareId());

				int year, month, day;

				// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
				// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
				licensingClient.GetLicenseExpirationDate(&year, &month, &day);

				if (year > 0)
					_tprintf(_T("\nLicense Expiration Date: %d/%d/%d"), month, day, year);
				else
					_tprintf(_T("\nLicense Expiration Date: Never"));

				// you should now save the hardware id and the activation key along with the product (license) key. 
				// Note that these keys do not have to be hidden or obfuscated in any way, because they are useless on another computer (IsLicenseActivated() will return false).
			}
		} else
		{
			_tprintf(_T("Product is activated"));
		}
	}
	catch (Exception * ex)
	{
		_tprintf(_T("Exception caught ! Message: \"%s\""), ex->GetExceptionMessage());
		ex->Destroy();
	}
	catch(std::exception & ex)
	{
		_tprintf(_T("Exception caught ! Message: \"%s\"."), ex.what());
	}

	getc(stdin);

	return 0;
}
*/





void SetKeyTemplateInfo(KeyTemplate& tmpl)
{
	int filedCnt = 1;
	int filedSize = 50 * 8; // bit
	const char_t * xml;
	//tmpl.LoadXml("E:2DShopPro-BOX.xml");
	
// 	tmpl.SetCharactersPerGroup(5);
// 	tmpl.SetNumberOfGroups(5);
// 	tmpl.SetSignatureSize(109);
// 	tmpl.SetPublicKeyCertificate(_T("AI0bDQELdoxzyMNu7n46whOSAEon0PzzErF7AiD1r/HhLy4U1wA="));
// 	//tmpl.SetPublicKeyCertificate(_T("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA="));
// 
// 	// The next paragraph of lines is used to generate a random product key, for demonstration purposes
// 	// In real world scenarios, the product key is read from product settings so the lines below are not needed
// 	tmpl.SetDataSize(16);
// 	tmpl.AddDataField(_T("ProductId"), FIELD_TYPE_INTEGER, 16, 0);
// 
// 	tmpl.SetPrivateKey(_T("AQnD07PnEw7CRi8="));	
// 
// 	// remove the following 2 lines if your license keys are not linked to customer names
// 	tmpl.SetValidationDataSize(filedSize * filedCnt);
// 	tmpl.AddValidationField(_T("CustomerName")	, FIELD_TYPE_STRING, filedSize, filedSize * 0);

	tmpl.SetCharactersPerGroup(5);
	tmpl.SetNumberOfGroups(5);
	tmpl.SetSignatureSize(93);
	tmpl.SetPublicKeyCertificate(_T("AFYeCgAIbgftKA0zwLeSAPzQkk8kyk3wWOoY0e6PwefLrQA="));
	//tmpl.SetPublicKeyCertificate(_T("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA="));

	// The next paragraph of lines is used to generate a random product key, for demonstration purposes
	// In real world scenarios, the product key is read from product settings so the lines below are not needed
	tmpl.SetDataSize(32);
	tmpl.AddDataField(_T("ProductId"), FIELD_TYPE_INTEGER, 16, 0);
	tmpl.AddDataField(_T("Version"), FIELD_TYPE_INTEGER, 16, 16);

	tmpl.SetPrivateKey(_T("AAczINgKDL8F"));	

	// remove the following 2 lines if your license keys are not linked to customer names
	tmpl.SetValidationDataSize(filedSize * filedCnt);
	tmpl.AddValidationField(_T("CustomerName")	, FIELD_TYPE_STRING, filedSize, filedSize * 0);


	// xml 값은 라이센스 서버에 등록된 값과 동일해야함
	//xml = tmpl.SaveXml();

	return;
}

void SetLicenseKeyInfo(KeyGenerator &generator)
{
	const char_t * productKey;
	generator.SetKeyData(_T("ProductId"), 777);
	generator.SetKeyData(_T("Version"), FEATURE_BASE | FEATURE_PRO);
	
	// remove the following line if your license keys are not linked to customer names
	generator.SetValidationData(_T("CustomerName"), _T("Wanny"));

	return;
}

//#define ServerURL "http://192.168.0.2/"
#define ServerURL "http://localhost:60001/"


int _tmain(int argc, _TCHAR* argv[])
{
	const char_t * xml;
	const char_t * productKey;
	char_t * hardwareId = _T("");
	char_t * activationKey = _T("");

	try {

		int filedCnt = 3;
		int filedSize = 50 * 8; // bit
		
		SDKRegistration::SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD"));

		KeyTemplate tmpl;
		SetKeyTemplateInfo(tmpl);

		KeyGenerator generator(&tmpl);
		SetLicenseKeyInfo(generator);

		productKey = generator.GenerateKey();
		//productKey = _T("BEBZA-KC3BF-FPG24-LSHDK-Aas");
		KeyValidator validator(&tmpl);
		validator.SetKey(productKey);
		validator.SetValidationData(_T("CustomerName"), _T("Wanny"));

		if (!validator.IsKeyValid())
		{
			throw std::exception("invalid license key");
		}

		unsigned char tmpBuf[50];
		int len = 50;

		validator.QueryValidationData(_T("CustomerName"), tmpBuf, &len);
		
		int id = validator.QueryIntKeyData(_T("ProductId"));
		int ver = validator.QueryIntKeyData(_T("Version"));

		bool check = ver & FEATURE_PRO;

		// 인터넷 연결 안될경우는 여기서 끝~!!
		KeyHelper::GetCurrentHardwareId();
		KeyHelper::DetectClockManipulation(2014,6,3);
		


		int validationDataLen = 200;
		unsigned char validationData[200];
		validator.QueryValidationData(NULL, validationData, &validationDataLen);

		
		LicensingClient licensingClient;
		licensingClient.SetLicenseKeyValidationData(validationData, validationDataLen);
		licensingClient.SetLicenseKeyTemplate(tmpl);


		/*productKey = _T("BEBSKM-ZXHV94-9XRMWC-2VMDM3-KB6CSY");
		//activationKey 등록 할때는 없어야 함 아래 항목 NULL
		hardwareId = _T("DJV46-2GEFC-EBHBB-JU332-LSNAA");
		activationKey = _T("DVTSBS-FV8NNS-K6WHLQ-9MTLGE-T3VJUV");*/
		
// 		productKey = _T("BEBSE5-S5VFLR-LMYC36-XTJDJM-JN243D");
// 		//activationKey 등록 할때는 없어야 함 아래 항목 NULL
// 		hardwareId = _T("DJV46-2GEFC-EBHBB-JU332-LSNAA");
// 		activationKey = _T("DVTSAA-CTU6L6-7HKXQQ-KKEVY2-QSUFKH");

		productKey = _T("BEBZA-KC3BF-FPG24-LSHDK-XWQDV");
		//activationKey 등록 할때는 없어야 함 아래 항목 NULL
		hardwareId = _T("DJV46-2GEFC-EBHBB-JU332-LSNAA");
		activationKey = _T("DVCHL-L3VRV-Y5LUS-68E5F-25UEW");

		//등록후에는 productKey, hardwareId, activationKey 3가지 항목을 채워 넣어야함
		licensingClient.GetHardwareId();

		licensingClient.SetLicensingServiceUrl(_T(ServerURL));
		licensingClient.SetLicenseKey(productKey);
		licensingClient.SetHardwareId(hardwareId);
		licensingClient.SetActivationKey(activationKey);
		licensingClient.SetProductId(777);


		// new in version 3.x : connects to a timeserver and retrieves the time, instead of retrieving the (possibly altered) local time 
		licensingClient.SetTimeValidationMethod(PREFER_INTERNET_TIME); // alternative: USE_INTERNET_TIME or USE_LOCAL_TIME

		if (!licensingClient.IsLicenseValid())
		{
			try {
				_tprintf(_T("Product needs to be activated.\nRequesting license activation from server..."));

				// SoftActivate Licensing SDK includes an activation server which you can install on your own servers or even on shared hosting environments.
				// Below a sample working server URL is provided. You can replace this with your own installed service URL
				//licensingClient.SetLicensingServiceUrl(_T("http://www.softactivate.com/SampleLicensingService/"));
				licensingClient.SetLicensingServiceUrl(_T(ServerURL));

				// the license activation consists in obtaining an "activation key" from the activation server, linked to this particular computer's hardware id. 
				licensingClient.AcquireLicense();

				_tprintf(_T("done."));
			} 
			catch (Exception * ex)
			{
				_tprintf(_T("Exception caught ! Message: \"%s\". Is the activation server started and properly configured (see documentation) ?"), ex->GetExceptionMessage());
				ex->Destroy();
			}
			catch (...)
			{
				_tprintf(_T("Unknown exception during activation attempt !"));
			}

			if (!licensingClient.IsLicenseValid())
			{
				int activationStatus = licensingClient.GetLicenseActivationStatus();
				char_t * activationStatusStr;

				switch (activationStatus)
				{
				case STATUS_INVALID_ACTIVATION_KEY:
					activationStatusStr = _T("activation key is invalid");
					break;

				case STATUS_INVALID_HARDWARE_ID:
					activationStatusStr = _T("hardware id does not match this computer");
					break;

				case STATUS_LICENSE_EXPIRED:
					int year, month, day;
					// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
					// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
					licensingClient.GetLicenseExpirationDate(&year, &month, &day);
					activationStatusStr = _T("license expired");
					break;

				default:
					activationStatusStr = _T("unknonwn");
					break;
				}

				_tprintf(_T("\nProduct could NOT be activated. Reason: %s"), activationStatusStr);
			} else
			{
				_tprintf(_T("\nActivation Successful. The returned activation key is '%s' and the computer's hardware id is '%s'"), licensingClient.GetActivationKey(), licensingClient.GetHardwareId());

				int year, month, day;

				// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
				// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
				licensingClient.GetLicenseExpirationDate(&year, &month, &day);

				if (year > 0)
					_tprintf(_T("\nLicense Expiration Date: %d/%d/%d"), month, day, year);
				else
					_tprintf(_T("\nLicense Expiration Date: Never"));

				// you should now save the hardware id and the activation key along with the product (license) key. 
				// Note that these keys do not have to be hidden or obfuscated in any way, because they are useless on another computer (IsLicenseActivated() will return false).
			}
		} else
		{
			_tprintf(_T("Product is activated"));

			int year, month, day;

			// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
			// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
			licensingClient.GetLicenseExpirationDate(&year, &month, &day);

			if (year > 0)
				_tprintf(_T("\nLicense Expiration Date: %d/%d/%d"), month, day, year);
			else
				_tprintf(_T("\nLicense Expiration Date: Never"));
		}
	}
	catch (Exception * ex)
	{
		_tprintf(_T("Exception caught ! Message: \"%s\""), ex->GetExceptionMessage());
		ex->Destroy();
	}
	catch(std::exception & ex)
	{
		_tprintf(_T("Exception caught ! Message: \"%s\"."), ex.what());
	}

	getc(stdin);

	return 0;
}

//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

// This sample demonstrates the XML import/export feature of the LicenseKeyTemplate class

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "licensing.h"

int _tmain(int argc, _TCHAR* argv[])
{
	const char * xmlTemplate;
	FILE * templateFile;

	try {
		// Entering the purchased developer key is the first step in using the SDK. 
		// The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
		// Replace the demo key with your purchased key.
		SDKRegistration::SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD"));

		KeyTemplate keyTemplate;

		// Set license key appearance as XXXXXX-XXXXXX-XXXXXX-XXXXXX-XXXXXX-XXXXXX
		keyTemplate.SetNumberOfGroups(6);
		keyTemplate.SetCharactersPerGroup(6);
		keyTemplate.SetGroupSeparator(_T("-"));
		keyTemplate.SetEncoding(ENCODING_BASE32X);

		// Set desired license key security
		keyTemplate.SetSignatureSize(158);

		// Generate a private/public key pair for the template
		keyTemplate.GenerateSigningKeyPair();

		// Add a 22-bit data field - This is 180 (the number of bits of a base32-encoded 6x6 character key) minus 158 (signature size)
		// You can customize this to your needs, adding other fields
		keyTemplate.SetDataSize(22);
		keyTemplate.AddDataField(_T("ProductId"), FIELD_TYPE_INTEGER, 16, 0);
		keyTemplate.AddDataField(_T("EnabledProductFeatures"), FIELD_TYPE_INTEGER, 6, 16);

		// In this sample, we link the key to a registration name and hardware id.
		// A specific license key will be generated for a specific computer and registration name, 
		// thus it will be useless on other computers other than the one for which it was generated

		keyTemplate.SetValidationDataSize(32*8);

		// Add a registration name validation field of a maximum of 32 characters
		keyTemplate.AddValidationField(_T("RegistrationName"), FIELD_TYPE_STRING, 32 * 8, 0);

		// Save the template into an UTF-8 encoded XML string, also storing the private key
		xmlTemplate = keyTemplate.SaveXml(true);

		// Save the XML template for use in generating license keys
		if ( ( templateFile = _tfopen(_T("template_gen.xml"), _T("wt") ) ) == NULL )
			return -1;

		fputs(xmlTemplate, templateFile);
		fclose(templateFile);

		// Save the template into an UTF-8 encoded XML string for key validarion purposes. The private key is NOT saved.
		xmlTemplate = keyTemplate.SaveXml(false);

		// Save the XML template for use in validating license keys
		if ( ( templateFile = _tfopen(_T("template_val.xml"), _T("wt") ) ) == NULL )
			return -1;

		fputs(xmlTemplate, templateFile);
		fclose(templateFile);

		_tprintf(_T("The files template_gen.xml and template_val.xml (for key generation and validation) were successfully generated. Press any key..."));

	} catch (Exception * ex)
	{
		_tprintf(_T("License Key Exception: %d (%s)"), ex->GetCode(), ex->GetExceptionMessage());
		ex->Destroy();
		return -1;
	}

	getc(stdin);

	return 0;
}

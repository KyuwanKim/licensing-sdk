// CustomHardwareIdCPP.cpp : Using your own hardware ids for license activation
//

#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <string>
#include <atlbase.h>

#include "licensing.h"

// these product id's are included with the server's database for sample purposes. Replace with your own.
#define SAMPLE_PRODUCT_ID_SIMPLE_KEY                    12345
#define SAMPLE_PRODUCT_ID_WITH_CUSTOMER_NAME_LINKED_KEY 54321

using namespace std;
using namespace SoftActivate::Licensing;

// derive a class from LicensingClient in order to provide our own custom hardware id generation/matching algorithms
class CustomHardwareIdLicensingClient : public LicensingClient
{
public:
	virtual const TCHAR * GetCurrentHardwareId()
	{
		// here you have the opportunity to return a (static) string which is created from the information taken from the current computer's hardware components
		// for example, you can return the MAC address of the first network adapter, or a concatenation of information taken from several hardware components
		return _T("MyCustomHardwareIdString");
	}

	virtual bool MatchCurrentHardwareId(const TCHAR * hwid)
	{
		// here you have the opportunity to compare a provided hardware id (usually the initial hardware id used for activation) to the current computer hardware id 
		// and decide of they match or not. The simplest comparison is a direct string comparison, however this is suboptimal for most cases. 
		// Licensing SDK's original routines do much more than just a direct string comparison of the provided hardware id 
		// in order to decide if it comes from the current computer or not (because certain components may have changed in the mean time, altering the current hardware id). 
		// It is recommended to not override these methods unless you know what you are doing.
		return _tcscmp(hwid, _T("MyCustomHardwareIdString")) == 0;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	const char_t * productKey;
	char_t * hardwareId = _T("");
	char_t * activationKey = _T("");

	try {
		// Entering the purchased developer key is the first step in using the SDK. 
		// The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
		// You if you purchased the SDK, replace this key with your purchased key.
		SDKRegistration::SetLicenseKey(_T("DXBUT-DLKR4-KHV6E-N6H5J-25VDD"));

		KeyTemplate tmpl;

		tmpl.SetCharactersPerGroup(5);
		tmpl.SetNumberOfGroups(5);
		tmpl.SetSignatureSize(109);
		tmpl.SetPublicKeyCertificate(_T("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA="));

		// The next paragraph of lines is used to generate a random product key, for demonstration purposes
		// In real world scenarios, the product key is read from product settings so the lines below are not needed
		tmpl.SetDataSize(16);
		tmpl.AddDataField(_T("ProductId"), FIELD_TYPE_INTEGER, 16, 0);
		tmpl.SetPrivateKey(_T("AQnD07PnEw7CRi8="));

		// remove the following 2 lines if your license keys are not linked to customer names
		tmpl.SetValidationDataSize(100 * 8);
		tmpl.AddValidationField(_T("CustomerName"), FIELD_TYPE_STRING, 100 * 8, 0);
		
		KeyGenerator generator(&tmpl);
		generator.SetKeyData(_T("ProductId"), SAMPLE_PRODUCT_ID_WITH_CUSTOMER_NAME_LINKED_KEY);

		// remove the following line if your license keys are not linked to customer names
		generator.SetValidationData(_T("CustomerName"), _T("John Smith"));
		productKey = generator.GenerateKey();

		KeyValidator validator(&tmpl);
		validator.SetKey(productKey);

		// remove the following line if your license keys are not linked to customer names
		validator.SetValidationData(_T("CustomerName"), _T("John Smith"));

		if (!validator.IsKeyValid())
		{
			throw std::exception("invalid license key");
		}

		int validationDataLen = 100;
		unsigned char validationData[100];

		validator.QueryValidationData(NULL, validationData, &validationDataLen);

		// the activation process begins here. 
        // NOTE: the activation VALIDATION (not the initial activation) is done completely offline and does not access the activation server !
		CustomHardwareIdLicensingClient licensingClient;

		// SetLicenseKeyValidationData needs to be called before SetActivationKeyTemplate !
		licensingClient.SetLicenseKeyValidationData(validationData, validationDataLen);
		
		licensingClient.SetLicenseKeyTemplate(tmpl);

		licensingClient.SetLicenseKey(productKey);
		licensingClient.SetHardwareId(hardwareId);
		licensingClient.SetActivationKey(activationKey);
		licensingClient.SetProductId(SAMPLE_PRODUCT_ID_WITH_CUSTOMER_NAME_LINKED_KEY);

		if (!licensingClient.IsLicenseValid())
		{
			try {
				_tprintf(_T("Product needs to be activated.\nRequesting license activation from server..."));

				// SoftActivate Licensing SDK includes an activation server which you can install on your own servers or even on shared hosting environments.
				// Below a sample working server URL is provided. You can replace this with your own installed service URL
				licensingClient.SetLicensingServiceUrl(_T("http://192.168.1.114/"));
				
				// the license activation consists in obtaining an "activation key" from the activation server, linked to this particular computer's hardware id. 
				licensingClient.AcquireLicense();

				_tprintf(_T("done."));
		    } 
			catch (Exception * ex)
			{
				_tprintf(_T("Exception caught ! Message: \"%s\". Is the activation server started and properly configured (see documentation) ?"), ex->GetExceptionMessage());
				ex->Destroy();
			}
			catch (...)
			{
				_tprintf(_T("Unknown exception during activation attempt !"));
			}

			if (!licensingClient.IsLicenseValid())
			{
				int activationStatus = licensingClient.GetLicenseActivationStatus();
				char_t * activationStatusStr;

				switch (activationStatus)
				{
				case STATUS_INVALID_ACTIVATION_KEY:
					activationStatusStr = _T("activation key is invalid");
					break;

				case STATUS_INVALID_HARDWARE_ID:
					activationStatusStr = _T("hardware id does not match this computer");
					break;

				case STATUS_LICENSE_EXPIRED:
					int year, month, day;
					// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
					// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
					licensingClient.GetLicenseExpirationDate(&year, &month, &day);
					activationStatusStr = _T("license expired");
					break;

				default:
					activationStatusStr = _T("unknonwn");
					break;
				}

				_tprintf(_T("\nProduct could NOT be activated. Reason: %s"), activationStatusStr);
			} else
			{
				_tprintf(_T("\nActivation Successful. The returned activation key is '%s' and the computer's hardware id is '%s'"), licensingClient.GetActivationKey(), licensingClient.GetHardwareId());

				int year, month, day;

				// the license expiration date returned by GetLicenseExpirationDate() is only valid if IsLicenseValid() returns > 0, 
				// or if IsLicenseValid() returns 0 and GetLicenseActivationStatus() returns STATUS_LICENSE_EXPIRED
				licensingClient.GetLicenseExpirationDate(&year, &month, &day);

				if (year > 0)
					_tprintf(_T("\nLicense Expiration Date: %d/%d/%d"), month, day, year);
				else
					_tprintf(_T("\nLicense Expiration Date: Never"));

				// you should now save the hardware id and the activation key along with the product (license) key. 
				// Note that these keys do not have to be hidden or obfuscated in any way, because they are useless on another computer (IsLicenseActivated() will return false).
			}
		} else
		{
			_tprintf(_T("Product is activated"));
		}
	}
	catch (Exception * ex)
	{
		_tprintf(_T("Exception caught ! Message: \"%s\""), ex->GetExceptionMessage());
		ex->Destroy();
	}
	catch(std::exception & ex)
	{
		_tprintf(_T("Exception caught ! Message: \"%s\"."), ex.what());
	}

	getc(stdin);

	return 0;
}

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ActivationForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.progressBar = New System.Windows.Forms.ProgressBar()
        Me.actionLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'progressBar
        '
        Me.progressBar.Location = New System.Drawing.Point(7, 25)
        Me.progressBar.MarqueeAnimationSpeed = 20
        Me.progressBar.Name = "progressBar"
        Me.progressBar.Size = New System.Drawing.Size(254, 23)
        Me.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progressBar.TabIndex = 0
        '
        'actionLabel
        '
        Me.actionLabel.AutoSize = True
        Me.actionLabel.Location = New System.Drawing.Point(7, 5)
        Me.actionLabel.Name = "actionLabel"
        Me.actionLabel.Size = New System.Drawing.Size(82, 13)
        Me.actionLabel.TabIndex = 1
        Me.actionLabel.Text = "xxxxxxxxxxxxxxx"
        '
        'ProgressForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(270, 76)
        Me.ControlBox = False
        Me.Controls.Add(Me.actionLabel)
        Me.Controls.Add(Me.progressBar)
        Me.Name = "ProgressForm"
        Me.Text = "Please Wait"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents progressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents actionLabel As System.Windows.Forms.Label
End Class

﻿Imports System.Threading
Imports SoftActivate.Licensing

Class LicensingForm
    Private _licenseKey As String

    Private Sub LicensingForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnActivate.DialogResult = Windows.Forms.DialogResult.OK
        btnCancel.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Public ReadOnly Property LicenseKey As String
        Get
            Return _licenseKey
        End Get
    End Property

    Private Sub btnGenerateLicenseKey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateLicenseKey.Click
        Dim tmpl As LicenseTemplate = New LicenseTemplate(5, 5, "AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=", "AQnD07PnEw7CRi8=", 109, 16, "ProductId")

        Dim keyGen As KeyGenerator = New KeyGenerator(tmpl)
        keyGen.SetKeyData("ProductId", &H1234)

        _licenseKey = keyGen.GenerateKey()
        txtLicenseKey.Text = _licenseKey
    End Sub

    Private Sub btnActivate_Click(sender As System.Object, e As System.EventArgs) Handles btnActivate.Click
        Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub
End Class

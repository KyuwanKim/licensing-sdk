﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRegistrationStatus = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtLicenseKey = New System.Windows.Forms.TextBox()
        Me.txtActivationKey = New System.Windows.Forms.TextBox()
        Me.txtActivationHardwareId = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnRegisterProduct = New System.Windows.Forms.Button()
        Me.btnDeleteRegistrationData = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnStartTrial = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtClockManipulationStatus = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Registration Status"
        '
        'txtRegistrationStatus
        '
        Me.txtRegistrationStatus.Location = New System.Drawing.Point(132, 18)
        Me.txtRegistrationStatus.Name = "txtRegistrationStatus"
        Me.txtRegistrationStatus.ReadOnly = True
        Me.txtRegistrationStatus.Size = New System.Drawing.Size(321, 20)
        Me.txtRegistrationStatus.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(79, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "License Key:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(69, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Activation Key:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(29, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Activation Hardware Id:"
        '
        'txtLicenseKey
        '
        Me.txtLicenseKey.Location = New System.Drawing.Point(153, 19)
        Me.txtLicenseKey.Name = "txtLicenseKey"
        Me.txtLicenseKey.ReadOnly = True
        Me.txtLicenseKey.Size = New System.Drawing.Size(234, 20)
        Me.txtLicenseKey.TabIndex = 5
        '
        'txtActivationKey
        '
        Me.txtActivationKey.Location = New System.Drawing.Point(153, 56)
        Me.txtActivationKey.Name = "txtActivationKey"
        Me.txtActivationKey.ReadOnly = True
        Me.txtActivationKey.Size = New System.Drawing.Size(234, 20)
        Me.txtActivationKey.TabIndex = 6
        '
        'txtActivationHardwareId
        '
        Me.txtActivationHardwareId.Location = New System.Drawing.Point(153, 93)
        Me.txtActivationHardwareId.Name = "txtActivationHardwareId"
        Me.txtActivationHardwareId.ReadOnly = True
        Me.txtActivationHardwareId.Size = New System.Drawing.Size(234, 20)
        Me.txtActivationHardwareId.TabIndex = 7
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtClockManipulationStatus)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtActivationHardwareId)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtLicenseKey)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtActivationKey)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 51)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(432, 161)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Registration Data"
        '
        'btnRegisterProduct
        '
        Me.btnRegisterProduct.Location = New System.Drawing.Point(174, 230)
        Me.btnRegisterProduct.Name = "btnRegisterProduct"
        Me.btnRegisterProduct.Size = New System.Drawing.Size(126, 23)
        Me.btnRegisterProduct.TabIndex = 9
        Me.btnRegisterProduct.Text = "Register Product"
        Me.btnRegisterProduct.UseVisualStyleBackColor = True
        '
        'btnDeleteRegistrationData
        '
        Me.btnDeleteRegistrationData.Location = New System.Drawing.Point(327, 230)
        Me.btnDeleteRegistrationData.Name = "btnDeleteRegistrationData"
        Me.btnDeleteRegistrationData.Size = New System.Drawing.Size(126, 23)
        Me.btnDeleteRegistrationData.TabIndex = 10
        Me.btnDeleteRegistrationData.Text = "&Delete Registration Data"
        Me.btnDeleteRegistrationData.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(327, 266)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(126, 23)
        Me.btnExit.TabIndex = 11
        Me.btnExit.Text = "&Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Enabled = False
        Me.Label5.Location = New System.Drawing.Point(-1, 286)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(184, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Sample contributed by Ansel Reynard"
        '
        'btnStartTrial
        '
        Me.btnStartTrial.Location = New System.Drawing.Point(21, 230)
        Me.btnStartTrial.Name = "btnStartTrial"
        Me.btnStartTrial.Size = New System.Drawing.Size(126, 23)
        Me.btnStartTrial.TabIndex = 13
        Me.btnStartTrial.Text = "Start 30-Day Trial"
        Me.btnStartTrial.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(47, 132)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Clock Manipulation:"
        '
        'txtClockManipulationStatus
        '
        Me.txtClockManipulationStatus.Location = New System.Drawing.Point(153, 130)
        Me.txtClockManipulationStatus.Name = "txtClockManipulationStatus"
        Me.txtClockManipulationStatus.ReadOnly = True
        Me.txtClockManipulationStatus.Size = New System.Drawing.Size(234, 20)
        Me.txtClockManipulationStatus.TabIndex = 15
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(475, 300)
        Me.Controls.Add(Me.btnStartTrial)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnDeleteRegistrationData)
        Me.Controls.Add(Me.btnRegisterProduct)
        Me.Controls.Add(Me.txtRegistrationStatus)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "MainForm"
        Me.Text = "Sample Product"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtRegistrationStatus As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtLicenseKey As System.Windows.Forms.TextBox
    Friend WithEvents txtActivationKey As System.Windows.Forms.TextBox
    Friend WithEvents txtActivationHardwareId As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRegisterProduct As System.Windows.Forms.Button
    Friend WithEvents btnDeleteRegistrationData As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnStartTrial As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtClockManipulationStatus As System.Windows.Forms.TextBox
End Class

﻿Imports SoftActivate.Licensing

Public Class ActivationForm
    Public Sub New(ByVal actionText As String, ByVal licenseKey As String)
        InitializeComponent()
        actionLabel.Text = actionText
        Me.licenseKey = licenseKey
    End Sub

    Public Sub ActivateLicense()
        _isActivated = False
        licensingClient = New LicensingClient("http://www.softActivate.com/SampleLicensingService/", CreateLicenseKeyTemplate(), licenseKey, Nothing, 12345)
        licensingClient.BeginAcquireLicense()

        ShowDialog()
    End Sub

    Private Sub OnLicensingComplete() Handles licensingClient.OnLicensingComplete
        _activationKey = licensingClient.ActivationKey
        _hardwareId = licensingClient.HardwareId
        _licenseExpirationDate = licensingClient.LicenseExpirationDate
        _isActivated = True
        Invoke(New LicensingCompleteEventHandler(AddressOf OnLicensingCompleteUI))
    End Sub

    Private Sub OnLicensingCompleteUI()
        Close()
    End Sub

    Private Sub OnLicensingException(ex As Exception) Handles licensingClient.OnLicensingException
        _isActivated = False
        Invoke(New LicensingExceptionEventHandler(AddressOf OnLicensingExceptionUI), ex)
    End Sub

    Private Sub OnLicensingExceptionUI(ex As Exception)
        MessageBox.Show("Error: " + ex.Message)
        Close()
    End Sub

    ReadOnly Property ActivationKey As String
        Get
            Return _activationKey
        End Get
    End Property

    ReadOnly Property HardwareId As String
        Get
            Return _hardwareId
        End Get
    End Property

    ReadOnly Property LicenseExpirationDate As String
        Get
            Return _licenseExpirationDate
        End Get
    End Property

    ReadOnly Property IsActivated As Boolean
        Get
            Return _isActivated
        End Get
    End Property

    Private Function CreateLicenseKeyTemplate() As LicenseTemplate
        Return New LicenseTemplate(5, 5, "AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=", Nothing, 109, 16, "ProductId")
    End Function

    Private WithEvents licensingClient As LicensingClient
    Private licenseKey As String
    Private _activationKey As String
    Private _hardwareId As String
    Private _licenseExpirationDate As DateTime
    Private _isActivated As Boolean
End Class

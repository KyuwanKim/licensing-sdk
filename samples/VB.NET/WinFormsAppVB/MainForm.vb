﻿Imports System.Threading
Imports SoftActivate.Licensing

Class MainForm

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        settings = New AppSettings()
        settings.Load()

        BeginLicenseValidation()
    End Sub

    Private Sub BeginLicenseValidation()
        SyncLock Me
            _licenseValidationThread = New Thread(New ParameterizedThreadStart(AddressOf LicenseValidationThread))
        End SyncLock

        _licenseValidationThread.Start()
    End Sub

    Private Delegate Sub SetTextBoxValueDelegate(textBox As TextBox, value As String)

    Sub SetTextBoxValue(textBox As TextBox, value As String)
        textBox.Text = value
    End Sub

    Private Sub LicenseValidationThread(param As Object)

        Dim registrationStatus As String = "NOT REGISTERED"
        Dim clockManipulationStatus As String = "NOT DETECTED"
        Dim isLicenseValid As Boolean = False
        Dim licenseExpirationDate As System.DateTime = System.DateTime.MinValue

        REM if a license key is not saved in the product settings, ask the user to enter the license key
        If (Not (String.IsNullOrEmpty(settings.GetProperty("ActivationKey")) Or String.IsNullOrEmpty(settings.GetProperty("ActivationHardwareId")) Or String.IsNullOrEmpty(settings.GetProperty("LicenseKey")))) Then
            Dim licensingClient As LicensingClient = New LicensingClient(CreateLicenseKeyTemplate(), settings.GetProperty("LicenseKey"), Nothing, settings.GetProperty("ActivationHardwareId"), settings.GetProperty("ActivationKey"))

            If licensingClient.IsLicenseValid() Then
                isLicenseValid = True
                licenseExpirationDate = licensingClient.LicenseExpirationDate

                If settings.GetProperty("LicenseKey") = trialLicenseKey Then
                    registrationStatus = "TRIAL" + " (" + (1 + (licenseExpirationDate - DateTime.UtcNow.ToLocalTime()).Days).ToString() + " days remaining)"
                Else
                    registrationStatus = "REGISTERED"
                End If
            Else
                registrationStatus = "INVALID LICENSE "

                Select Case licensingClient.LicenseStatus
                    Case LICENSE_STATUS.Expired
                        registrationStatus = registrationStatus + "(expired on " + licensingClient.LicenseExpirationDate.Month + "/" + licensingClient.LicenseExpirationDate.Day + "/" + licensingClient.LicenseExpirationDate.Year + ")"
                    Case LICENSE_STATUS.InvalidActivationKey
                        registrationStatus = registrationStatus + "(invalid activation key)"
                    Case LICENSE_STATUS.InvalidHardwareId
                        registrationStatus = registrationStatus + "(invalid hardware id)"
                    Case Else
                        registrationStatus = registrationStatus + "(unknown reason)"
                End Select
            End If
        End If

        Invoke(New SetTextBoxValueDelegate(AddressOf SetTextBoxValue), New Object() {txtRegistrationStatus, registrationStatus})
        Invoke(New SetTextBoxValueDelegate(AddressOf SetTextBoxValue), New Object() {txtLicenseKey, settings.GetProperty("LicenseKey")})
        Invoke(New SetTextBoxValueDelegate(AddressOf SetTextBoxValue), New Object() {txtActivationHardwareId, settings.GetProperty("ActivationHardwareId")})
        Invoke(New SetTextBoxValueDelegate(AddressOf SetTextBoxValue), New Object() {txtActivationKey, settings.GetProperty("ActivationKey")})

        If isLicenseValid Then
            If KeyHelper.DetectClockManipulation(licenseExpirationDate) Then
                clockManipulationStatus = "CLOCK MANIPULATION DETECTED !"
            End If
        End If

        Invoke(New SetTextBoxValueDelegate(AddressOf SetTextBoxValue), New Object() {txtClockManipulationStatus, clockManipulationStatus})

        SyncLock Me
            _licenseValidationThread = Nothing
        End SyncLock
    End Sub

    Private Sub MainForm_Closing(sender As Object, args As FormClosingEventArgs) Handles MyBase.FormClosing
        settings.Save()
    End Sub

    Private Sub btnRegisterProduct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegisterProduct.Click
        Dim licensingForm As LicensingForm = New LicensingForm()

        If licensingForm.ShowDialog() = DialogResult.OK Then
            REM display the license dialog
            Dim activationForm As ActivationForm = New ActivationForm("Activating product...", licensingForm.LicenseKey)
            activationForm.ActivateLicense()

            If activationForm.IsActivated Then
                settings.SetProperty("LicenseKey", licensingForm.LicenseKey)
                settings.SetProperty("ActivationKey", activationForm.ActivationKey)
                settings.SetProperty("ActivationHardwareId", activationForm.HardwareId)

                BeginLicenseValidation()
            End If
        End If
    End Sub

    Private Sub btnStartTrial_Click(sender As System.Object, e As System.EventArgs) Handles btnStartTrial.Click
        Dim activationForm As ActivationForm = New ActivationForm("Acquiring trial license...", trialLicenseKey)
        activationForm.ActivateLicense()

        If activationForm.IsActivated Then
            settings.SetProperty("LicenseKey", trialLicenseKey)
            settings.SetProperty("ActivationKey", activationForm.ActivationKey)
            settings.SetProperty("ActivationHardwareId", activationForm.HardwareId)

            BeginLicenseValidation()
        End If
    End Sub

    Private Sub btnDeleteRegistrationData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteRegistrationData.Click
        settings.SetProperty("LicenseKey", "")
        settings.SetProperty("ActivationKey", "")
        settings.SetProperty("ActivationHardwareId", "")

        settings.Save()

        txtLicenseKey.Clear()
        txtActivationKey.Clear()
        txtActivationHardwareId.Clear()
        txtRegistrationStatus.Text = "NOT REGISTERED"
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        MyBase.OnClosing(e)

        Dim tmpLicenseValidationThread As Thread = Nothing

        SyncLock Me
            tmpLicenseValidationThread = _licenseValidationThread
        End SyncLock

        If Not (tmpLicenseValidationThread Is Nothing) Then
            MessageBox.Show("Application is busy. Please wait until the worker thread finishes.")
            e.Cancel = True
            Return
        End If

        settings.Save()
    End Sub

    Private settings As AppSettings

    Private progressForm As ActivationForm

    Private Function CreateLicenseKeyTemplate() As LicenseTemplate
        Return New LicenseTemplate(5, 5, "AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=", Nothing, 109, 16, "ProductId")
    End Function

    Private trialLicenseKey As String = "X62DG-94SDT-A4TBZ-949CK-KMZB5"
    Private _licenseValidationThread As Thread
End Class

﻿using System; 
using System.Collections.Generic;
using System.Text;
using SoftActivate.Licensing;

namespace ActivationCS
{
    class Program
    {
        const int SAMPLE_PRODUCT_ID_NO_VALIDATION_FIELDS = 12345;
        const int SAMPLE_PRODUCT_ID_WITH_VALIDATION_FIELDS = 777;

        static void Main(string[] args)
        {
            try
            {
                // Entering the purchased developer key is the first step in using the SDK for license key generation and private/public key pair generation
                // Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
                // where you will only validate license keys. This call is only needed for license key generation and public/private key pair creation
                // You will only call this method in your key generator application, and you will set it in the configuration file of the licensing service

                // The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
                // You if you purchased the SDK, replace this key with your purchased key.

                // Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
                SDKRegistration.SetLicenseKey("DXBUT-DLKR4-KHV6E-N6H5J-25VDD");

                string productKey;

                // this should be read from the settings, but for the scope of this sample, we set it with the correct hardware id for this computer
                string hardwareId = "";

                // the activation key should be read from product settings, and saved back after a successful activation
                string activationKey = "";

                // this template must correspond to the activation server template
                LicenseTemplate template = new LicenseTemplate();
                template.CharactersPerGroup = 5;
                template.NumberOfGroups = 5;
                template.SignatureSize = 109;
                template.SetPublicKeyCertificate("AI0bDQELdoxzyMNu7n46whOSAH+azyCfw4Kx2FDADOO/01SjFgE=");

                // The next paragraph of lines is used to generate a sample product key, for demonstration purposes
                // In real world scenarios, the product key is read from product settings so the lines below are not needed
                template.SetPrivateKey("AQnD07PnEw7CRi8=");
                template.DataSize = 16;
                template.AddDataField("KeyData", LicenseKeyFieldType.Integer, 16, 0);
                
                // optionally, link the license key to a customer name. Note: this does not include the customer name into the license key !
                template.AddValidationField("CustomerName", LicenseKeyFieldType.String, 100 * 8, 0);

                KeyGenerator generator = new KeyGenerator(template);
                
                // set data field values (these are included in the license key)
                generator.SetKeyData("KeyData", 0x1234);
                 
                // set validation field values (this are not included in the license key, thus they are also required when you validate the key)
                generator.SetValidationData("CustomerName", "John Doe");
                
                // Generate the license key
                productKey = generator.GenerateKey();

                byte[] licenseKeyValidationData = null;

                // The next paragraph should only be used if there are validation fields in the license key template.
                // We create a KeyValidator in order to obtain the license key validation data which needs to be passed to the LicensingClient instance
                // If we have no validation fields, we will pass null to the LicensingClient
                KeyValidator validator = new KeyValidator(template, productKey);
                
                // note that validation fields need to be set at validation, since they are not included in the license key
                validator.SetValidationData("CustomerName", "John Doe");
                
                if (validator.IsKeyValid())
                {
                    // by calling the QueryValidationData without a field name, we get the entire validation data
                    licenseKeyValidationData = validator.QueryValidationData(null);
                }

                // SoftActivate Licensing SDK includes an activation server which you can install on your own servers 
                // or in shared hosting environments.
                // Below a sample working server URL is provided. In production you will need to replace this with your own installed service URL

                // We first assume that the license is already activated, so we use a constructor that validates the license locally 
                // (does not connect to the activation server).
                LicensingClient licensingClient = new LicensingClient(template,
                                                                      productKey, /* product license read from product's settings */
                                                                      licenseKeyValidationData,
                                                                      hardwareId,
                                                                      activationKey
                                                                     );

                // this causes the client to connect to a timeserver to check the time, instead of using the computer's time
                // if leaving this unset or setting to false, you risk checking against a computer time that was turned back
                // in order to fool the licensing system
                licensingClient.TimeValidationMethod = TIME_VALIDATION_METHOD.PreferInternetTime;

                if (!licensingClient.IsLicenseValid())
                {
                    Console.WriteLine("Product needs to be activated");
                    Console.Write("Requesting license activation from server...");

                    int productIdOnLicensingServer = (licenseKeyValidationData == null) ? SAMPLE_PRODUCT_ID_NO_VALIDATION_FIELDS :
                                                                                          SAMPLE_PRODUCT_ID_WITH_VALIDATION_FIELDS;

                    // Now that we know that the license is not valid, we try to activate the license. 
                    // (Most developers choose to just display an error box to the customer and let him press an "Activate" button instead of attempting to activate silently)
                    // We use the constructor in which we set the activation server and product id
                    //licensingClient = new LicensingClient("http://www.softactivate.com/SampleLicensingService/",
                    licensingClient = new LicensingClient("http://192.168.1.114/",
                                                          template,
                                                          productKey,
                                                          licenseKeyValidationData, 
                                                          productIdOnLicensingServer);

                    // this causes the client to connect to a timeserver to check the time, instead of using the computer's time
                    // if leaving this unset or setting to false, you risk checking against a computer time that was turned back
                    // in order to fool the licensing system
                    licensingClient.TimeValidationMethod = TIME_VALIDATION_METHOD.PreferInternetTime;

                    try
                    {
                        licensingClient.AcquireLicense();
                        Console.WriteLine("done.");
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Exception caught during activation (is the server started ?) ! " + ex.Message);
                    }

                    if (!licensingClient.IsLicenseValid())
                    {
                        string activationStatusStr;

                        switch (licensingClient.LicenseStatus)
                        {
                            case LICENSE_STATUS.InvalidActivationKey:
                                activationStatusStr = "invalid activation key";
                                break;

                            case LICENSE_STATUS.InvalidHardwareId:
                                activationStatusStr = "invalid hardware id";
                                break;

                            case LICENSE_STATUS.Expired:
                                {
                                    // the license expiration date returned by LicenseExpirationDate property is only valid if IsLicenseValid() returns true, 
                                    // or if IsLicenseValid() returns false and LicenseStatus returns LicenseStatus.Expired
                                    DateTime expDate = licensingClient.LicenseExpirationDate;
                                    activationStatusStr = "license expired (expiration date: " + expDate.Month + "/" + expDate.Day + "/" + expDate.Year + ")";
                                }
                                break;

                            default:
                                activationStatusStr = "unknown";
                                break;
                        }

                        Console.WriteLine("Activation validation failed. Reason: " + activationStatusStr);
                    }
                    else
                    {
                        Console.WriteLine("Activation was successful. ");
                        // After successful activation, save the activation key and the hardware id, along with the license key.
                        // You need to use this hardware id next time you run the application, and not the current computer hardware id.
                        // (the hardware id matching of the hardware id used for activation with the current hardware id is done 
                        // automatically when you call IsLicenseActivated)
                        // These do not have to be hidden or obfuscated. 
                        Console.WriteLine("License Key: " + productKey);
                        Console.WriteLine("Activation Key: " + licensingClient.ActivationKey);
                        Console.WriteLine("Hardware Id: " + licensingClient.HardwareId);

                        // the license expiration date returned by LicenseExpirationDate property is only valid if IsLicenseValid() returns true, 
                        // or if IsLicenseValid() returns false and LicenseStatus returns LicenseStatus.Expired
                        DateTime expDate = licensingClient.LicenseExpirationDate;

                        if (expDate == DateTime.MinValue)
                            Console.WriteLine("License Expiration Date: Never");
                        else
                            Console.WriteLine("License Expiration Date: " + expDate.Month + "/" + expDate.Day + "/" + expDate.Year);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write("Exception caught ! " + ex.Message);
            }

            Console.WriteLine();
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using SoftActivate.Licensing;

namespace HardwareIdCS
{
    class Program
    {
        static void Main(string[] args)
        {
            string hardwareId;

            try
            {
                hardwareId = KeyHelper.GetCurrentHardwareId();
                Console.WriteLine("This computer's hardware ID is: " + hardwareId);

                if (KeyHelper.MatchCurrentHardwareId(hardwareId))
                {
                    Console.WriteLine("The hardware id '" + hardwareId + "' matches this computer");
                }
                else
                {
                    Console.WriteLine("The hardware id'" + hardwareId + "' does not match this computer !");
                }
            } catch (Exception ex)
            {
                Console.WriteLine("Could not obtain a hardware id for this computer. Please contact SoftActivate support");
                if (!string.IsNullOrEmpty(ex.Message))
                    Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}

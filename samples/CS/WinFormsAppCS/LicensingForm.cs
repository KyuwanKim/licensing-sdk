﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SoftActivate.Licensing;
using System.Threading;

namespace WinFormsAppCS
{
    public partial class LicensingForm : Form
    {
        public LicensingForm()
        {
            InitializeComponent();

            btnActivate.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        public string LicenseKey
        {
            get
            {
                return txtLicenseKey.Text;
            }
        }

        private void btnGenerateLicenseKey_Click(object sender, EventArgs e)
        {

            /*
            int filedCnt = 3;
            int filedSize = 50 * 8; // bit

            LicenseTemplate template = new LicenseTemplate(5, 5, MainForm.publicKeyCertificate, "AQnD07PnEw7CRi8=", 109, 16, "ProductId");
            template.ValidationDataSize = filedSize * filedCnt;
            template.AddValidationField("ExpirationDate", LicenseKeyFieldType.String, filedSize, filedSize * 0);
            template.AddValidationField("CustomerName", LicenseKeyFieldType.String, filedSize, filedSize * 1);
            template.AddValidationField("ActivationID", LicenseKeyFieldType.String, filedSize, filedSize * 2);

            KeyGenerator generator = new KeyGenerator(template);
            generator.SetKeyData("ProductId", 777);
            txtLicenseKey.Text = generator.GenerateKey();
            string xml = template.SaveXml(true);
            */

            
            //int filedCnt = 1;
            //int filedSize = 100 * 8; // bit

            //LicenseTemplate template = new LicenseTemplate(5, 5, MainForm.publicKeyCertificate, "AQnD07PnEw7CRi8=", 109, 30, "ProductId");

            LicenseTemplate template = new LicenseTemplate();

            template.NumberOfGroups = 5;
            template.CharactersPerGroup = 5;
            template.Encoding = LicenseKeyEncoding.Base32X;
            template.GroupSeparator = "-";
            template.DataSize = 22; // the license keys will hold 30 bits of data
            template.SignatureSize = 103; // 120 bits = ((characters per group * number of groups) * bits per character) - data size ; 120 = ((6 * 5) * 5) - 30
            template.ValidationDataSize = 0;

            template.SetPrivateKey("AQnD07PnEw7CRi8=");
            template.SetPublicKeyCertificate("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=");


            template.AddDataField("ProductId",
                              LicenseKeyFieldType.Integer,
                              8,   // the field is has a size of 8 bits (it can hold 2^8 = 256 different product id's)
                              0);  // field starts at position 0 in the license key data

            template.AddDataField("ExpirationDate",            // field name
                              LicenseKeyFieldType.Date14, // field is regarded as a 14-bit date (year must be between 2010 and 2041)
                              14, // field is 14 bits long (4 bits for month, 5 bits for day, 5 bits for year)
                              8  // field starts at position 16 in license key data bits (bits 0-7 and 8-15 are occupied by the product id and feature bits)
                              );

            //template.ValidationDataSize = filedSize * filedCnt;
            //template.AddValidationField("CustomerName", LicenseKeyFieldType.String, filedSize, filedSize * 0);


            KeyGenerator generator = new KeyGenerator(template);
            generator.SetKeyData("ProductId", 777);
            generator.SetKeyData("ExpirationDate", DateTime.Today.AddYears(1));
            //generator.SetValidationData("CustomerName", "Wanny");

            txtLicenseKey.Text = generator.GenerateKey();

            string xml = template.SaveXml(true);
            

        }
    }
}

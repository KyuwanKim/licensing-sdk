﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SoftActivate.Licensing;
using System.Threading;

namespace WinFormsAppCS
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            settings = new AppSettings();
            settings.Load();

            string licenseKey = settings.GetProperty("LicenseKey"),
                   activationHardwareId = settings.GetProperty("ActivationHardwareId"),
                   activationKey = settings.GetProperty("ActivationKey");

            // if the license data is found in the settings file, validate it. 
            // 

            if (!(String.IsNullOrEmpty(activationKey)
                  || String.IsNullOrEmpty(activationHardwareId)
                  || String.IsNullOrEmpty(licenseKey)))
            {
                // check the license validity on a different thread, it can take a second or so, we do not want to block the application startup while doing it
                ValidateLicenseCaller vl = new ValidateLicenseCaller(ValidateLicense);
                IAsyncResult vlResult = vl.BeginInvoke(OnValidateLicenseComplete, null);
            }
        }

        public delegate void ValidateLicenseCaller();

        void ValidateLicense()
        {
            string registrationStatus = "NOT REGISTERED";
            string clockManipulationStatus = "NOT DETECTED";
            bool validLicense = false;
            DateTime licenseExpirationDate = default(DateTime);

            if (!(String.IsNullOrEmpty(settings.GetProperty("ActivationKey"))
                  || String.IsNullOrEmpty(settings.GetProperty("ActivationHardwareId"))
                  || String.IsNullOrEmpty(settings.GetProperty("LicenseKey"))))
            {
                LicensingClient licensingClient = new LicensingClient(
                                                             CreateLicenseKeyTemplate(),
                                                             settings.GetProperty("LicenseKey"),
                                                             null,
                                                             settings.GetProperty("ActivationHardwareId"),
                                                             settings.GetProperty("ActivationKey"));

                if (licensingClient.IsLicenseValid())
                {
                    validLicense = true;
                    licenseExpirationDate = licensingClient.LicenseExpirationDate;
                    registrationStatus = (settings.GetProperty("LicenseKey") == trialLicenseKey) ? "TRIAL" + " (" + (1 + (licenseExpirationDate - DateTime.UtcNow.ToLocalTime()).Days) + " days remaining)" : "REGISTERED";
                }
                else
                {
                    registrationStatus = "INVALID LICENSE ";
                    switch (licensingClient.LicenseStatus)
                    {
                        case LICENSE_STATUS.Expired:
                            registrationStatus += "(expired on " + licensingClient.LicenseExpirationDate.Month + "/" 
                                                                 + licensingClient.LicenseExpirationDate.Day + "/" 
                                                                 + licensingClient.LicenseExpirationDate.Year + ")";
                            break;

                        case LICENSE_STATUS.InvalidActivationKey:
                            registrationStatus += "(invalid activation key)";
                            break;

                        case LICENSE_STATUS.InvalidHardwareId:
                            registrationStatus += "(invalid hardware id)";
                            break;

                        default:
                            registrationStatus += "(unknown reason)";
                            break;
                    }
                }
            }

            // since we are on a different thread from the UI thread, 
            // we cannot directly update the textboxes from here, we need to call a method on the UI thread to do this
            Invoke(new SetTextBoxValueDelegate(SetTextBoxValue), new object[] { txtRegistrationStatus, registrationStatus });
            Invoke(new SetTextBoxValueDelegate(SetTextBoxValue), new object[] { txtLicenseKey, settings.GetProperty("LicenseKey") });
            Invoke(new SetTextBoxValueDelegate(SetTextBoxValue), new object[] { txtActivationHardwareId, settings.GetProperty("ActivationHardwareId") });
            Invoke(new SetTextBoxValueDelegate(SetTextBoxValue), new object[] { txtActivationKey, settings.GetProperty("ActivationKey") });

            if (validLicense)
            {
                if (KeyHelper.DetectClockManipulation(licenseExpirationDate))
                    clockManipulationStatus = "CLOCK MANIPULATION DETECTED !";
            }
            
            Invoke(new SetTextBoxValueDelegate(SetTextBoxValue), new object[] { txtClockManipulationStatus, clockManipulationStatus });

            lock (this)
            {
                licenseValidationThread = null;
            }
        }

        void OnValidateLicenseComplete(IAsyncResult ar)
        {

        }

        public delegate void SetTextBoxValueDelegate(TextBox textBox, string value);

        void SetTextBoxValue(TextBox textBox, string value)
        {
            textBox.Text = value;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRegisterProduct_Click(object sender, EventArgs e)
        {
            LicensingForm licensingForm = new LicensingForm();

            if (licensingForm.ShowDialog() == DialogResult.OK)
            {
                // display the license dialog
                ActivationForm activationForm = new ActivationForm("Activating product...", licensingForm.LicenseKey);
                activationForm.ActivateLicense();

                if (activationForm.IsActivated)
                {
                    settings.SetProperty("LicenseKey", licensingForm.LicenseKey);
                    settings.SetProperty("ActivationKey", activationForm.ActivationKey);
                    settings.SetProperty("ActivationHardwareId", activationForm.HardwareId);

                    // check the license validity on a different thread, it can take a second or so, we do not want to block the application startup while doing it
                    new ValidateLicenseCaller(ValidateLicense).BeginInvoke(OnValidateLicenseComplete, null);
                }
            }
        }

        private void btnStartTrial_Click(object sender, EventArgs e)
        {
            ActivationForm activationForm = new ActivationForm("Acquiring trial license...", trialLicenseKey);
            activationForm.ActivateLicense();

            if (activationForm.IsActivated)
            {
                settings.SetProperty("LicenseKey", trialLicenseKey);
                settings.SetProperty("ActivationKey", activationForm.ActivationKey);
                settings.SetProperty("ActivationHardwareId", activationForm.HardwareId);

                new ValidateLicenseCaller(ValidateLicense).BeginInvoke(OnValidateLicenseComplete, null);
            }
        }

        private void btnDeleteRegistrationData_Click(object sender, EventArgs e)
        {
            settings.SetProperty("LicenseKey", "");
            settings.SetProperty("ActivationKey", "");
            settings.SetProperty("ActivationHardwareId", "");

            settings.Save();

            txtLicenseKey.Clear();
            txtActivationKey.Clear();
            txtActivationHardwareId.Clear();
            txtRegistrationStatus.Text = "NOT REGISTERED";
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            Thread tmpLicenseValidationThread = null;

            lock (this)
            {
                tmpLicenseValidationThread = licenseValidationThread;
            }

            if (tmpLicenseValidationThread != null)
            {
                MessageBox.Show("The application is busy. Please wait for the worker thread to finish");
                e.Cancel = true;
                return;
            }

            settings.Save();
        }

        private LicenseTemplate CreateLicenseKeyTemplate()
        {
            return new LicenseTemplate(5, // number of groups of characters in the license key  
                                               5, // number of character in each group
                                               publicKeyCertificate, // the public key certificate used to verify the license key signature
                                               PrivateKey,
                                               109,  // signature size of the license key is 109 bits
                                               16,   // data size embedded in the license key is 16 bits
                                               "ProductId" // this is the name of the data field embedded in the license key
                                               );
        }

        AppSettings settings;
        const int productId = 12345;
        const string trialLicenseKey = "X62DG-94SDT-A4TBZ-949CK-KMZB5";
        public static string publicKeyCertificate = "AI0bDQELdoxzyMNu7n46whOSAEon0PzzErF7AiD1r/HhLy4U1wA=";
        public static string PrivateKey = "AQnD07PnEw7CRi8=";
        Thread licenseValidationThread;
    }
}

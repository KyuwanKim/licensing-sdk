﻿namespace WinFormsAppCS
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new System.Windows.Forms.Button();
            this.btnDeleteRegistrationData = new System.Windows.Forms.Button();
            this.btnRegisterProduct = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLicenseKey = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtActivationKey = new System.Windows.Forms.TextBox();
            this.txtActivationHardwareId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRegistrationStatus = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnStartTrial = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClockManipulationStatus = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(327, 266);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(126, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnDeleteRegistrationData
            // 
            this.btnDeleteRegistrationData.Location = new System.Drawing.Point(327, 230);
            this.btnDeleteRegistrationData.Name = "btnDeleteRegistrationData";
            this.btnDeleteRegistrationData.Size = new System.Drawing.Size(126, 23);
            this.btnDeleteRegistrationData.TabIndex = 1;
            this.btnDeleteRegistrationData.Text = "&Delete Registration Data";
            this.btnDeleteRegistrationData.UseVisualStyleBackColor = true;
            this.btnDeleteRegistrationData.Click += new System.EventHandler(this.btnDeleteRegistrationData_Click);
            // 
            // btnRegisterProduct
            // 
            this.btnRegisterProduct.Location = new System.Drawing.Point(174, 230);
            this.btnRegisterProduct.Name = "btnRegisterProduct";
            this.btnRegisterProduct.Size = new System.Drawing.Size(126, 23);
            this.btnRegisterProduct.TabIndex = 2;
            this.btnRegisterProduct.Text = "Register Product";
            this.btnRegisterProduct.UseVisualStyleBackColor = true;
            this.btnRegisterProduct.Click += new System.EventHandler(this.btnRegisterProduct_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "License Key:";
            // 
            // txtLicenseKey
            // 
            this.txtLicenseKey.Location = new System.Drawing.Point(153, 19);
            this.txtLicenseKey.Name = "txtLicenseKey";
            this.txtLicenseKey.ReadOnly = true;
            this.txtLicenseKey.Size = new System.Drawing.Size(234, 20);
            this.txtLicenseKey.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Activation Key:";
            // 
            // txtActivationKey
            // 
            this.txtActivationKey.Location = new System.Drawing.Point(153, 56);
            this.txtActivationKey.Name = "txtActivationKey";
            this.txtActivationKey.ReadOnly = true;
            this.txtActivationKey.Size = new System.Drawing.Size(234, 20);
            this.txtActivationKey.TabIndex = 6;
            // 
            // txtActivationHardwareId
            // 
            this.txtActivationHardwareId.Location = new System.Drawing.Point(153, 93);
            this.txtActivationHardwareId.Name = "txtActivationHardwareId";
            this.txtActivationHardwareId.ReadOnly = true;
            this.txtActivationHardwareId.Size = new System.Drawing.Size(234, 20);
            this.txtActivationHardwareId.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Activation Hardware Id:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtClockManipulationStatus);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtLicenseKey);
            this.groupBox1.Controls.Add(this.txtActivationKey);
            this.groupBox1.Controls.Add(this.txtActivationHardwareId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(21, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 161);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registration Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Registration Status:";
            // 
            // txtRegistrationStatus
            // 
            this.txtRegistrationStatus.Location = new System.Drawing.Point(132, 18);
            this.txtRegistrationStatus.Name = "txtRegistrationStatus";
            this.txtRegistrationStatus.ReadOnly = true;
            this.txtRegistrationStatus.Size = new System.Drawing.Size(321, 20);
            this.txtRegistrationStatus.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(-1, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(184, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Sample contributed by Ansel Reynard";
            // 
            // btnStartTrial
            // 
            this.btnStartTrial.Location = new System.Drawing.Point(21, 230);
            this.btnStartTrial.Name = "btnStartTrial";
            this.btnStartTrial.Size = new System.Drawing.Size(126, 23);
            this.btnStartTrial.TabIndex = 13;
            this.btnStartTrial.Text = "Start 30-Day Trial";
            this.btnStartTrial.UseVisualStyleBackColor = true;
            this.btnStartTrial.Click += new System.EventHandler(this.btnStartTrial_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Clock Manipulation:";
            // 
            // txtClockManipulationStatus
            // 
            this.txtClockManipulationStatus.Location = new System.Drawing.Point(153, 130);
            this.txtClockManipulationStatus.Name = "txtClockManipulationStatus";
            this.txtClockManipulationStatus.ReadOnly = true;
            this.txtClockManipulationStatus.Size = new System.Drawing.Size(234, 20);
            this.txtClockManipulationStatus.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 300);
            this.Controls.Add(this.btnStartTrial);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtRegistrationStatus);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRegisterProduct);
            this.Controls.Add(this.btnDeleteRegistrationData);
            this.Controls.Add(this.btnExit);
            this.Name = "MainForm";
            this.Text = "Sample Product";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnDeleteRegistrationData;
        private System.Windows.Forms.Button btnRegisterProduct;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLicenseKey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtActivationKey;
        private System.Windows.Forms.TextBox txtActivationHardwareId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRegistrationStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnStartTrial;
        private System.Windows.Forms.TextBox txtClockManipulationStatus;
        private System.Windows.Forms.Label label6;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SoftActivate.Licensing;

namespace WinFormsAppCS
{
    public partial class ActivationForm : Form
    {
        public ActivationForm(string actionText, string licenseKey)
        {
            InitializeComponent();

            actionLabel.Text = actionText;
            this.licenseKey = licenseKey;
        }

        public void ActivateLicense()
        {
            /*
            isActivated = false;

            //licensingClient = new LicensingClient("http://www.softactivate.com/SampleLicensingService/",
            licensingClient = new LicensingClient("http://192.168.1.114/",
                                                  CreateLicenseKeyTemplate(),  // no license key provided means we are requesting a trial version
                                                  licenseKey,
                                                  null,
                                                  777
                                                 );

            licensingClient.OnLicensingException += new LicensingExceptionEventHandler(OnLicensingException);
            licensingClient.OnLicensingComplete += new LicensingCompleteEventHandler(OnLicensingComplete);

            // contact the licensing server and ask for a trial license for this computer's hardware id
            // if a trial license was already granted by the server for this particular computer and product id, the server will deny the request
            licensingClient.BeginAcquireLicense();

            ShowDialog();
            */

            
            isActivated = false;

            LicenseTemplate templ = CreateLicenseKeyTemplate();

//             KeyValidator KeyVali = new KeyValidator(templ);
//             KeyVali.SetValidationData("CustomerName", "Wanny");


            //licensingClient = new LicensingClient("http://www.softactivate.com/SampleLicensingService/",
            licensingClient = new LicensingClient("http://192.168.1.114/",
                                                  templ,  // no license key provided means we are requesting a trial version
                                                  licenseKey,
                                                  null,
                                                  //KeyVali.QueryValidationData(null),
                                                  777 
                                                 );


            licensingClient.OnLicensingException += new LicensingExceptionEventHandler(OnLicensingException);
            licensingClient.OnLicensingComplete += new LicensingCompleteEventHandler(OnLicensingComplete);

            // contact the licensing server and ask for a trial license for this computer's hardware id
            // if a trial license was already granted by the server for this particular computer and product id, the server will deny the request
            licensingClient.BeginAcquireLicense();

            ShowDialog();
            
        }

        public string ActivationKey
        {
            get
            {
                return activationKey;
            }
        }

        public string HardwareId
        {
            get
            {
                return hardwareId;
            }
        }

        public DateTime LicenseExpirationDate
        {
            get
            {
                return licenseExpirationDate;
            }
        }

        public bool IsActivated
        {
            get
            {
                return isActivated;
            }
        }

        public bool ClockManipulationDetected
        {
            get
            {
                return clockManipulationDetected;
            }
        }

        private void OnLicensingComplete()
        {
            isActivated = true;
            activationKey = licensingClient.ActivationKey;
            hardwareId = licensingClient.HardwareId;
            licenseExpirationDate = licensingClient.LicenseExpirationDate;
            clockManipulationDetected = KeyHelper.DetectClockManipulation(licenseExpirationDate);

            // This thread is a worker thread. We need to invoke the method on the UI thread.
            Invoke(new LicensingCompleteEventHandler(OnLicensingCompleteUI));
        }

        private void OnLicensingCompleteUI()
        {
            Close();
        }

        private void OnLicensingException(Exception ex)
        {
            isActivated = false;

            // This thread is a worker thread. We need to invoke the method on the UI thread.
            Invoke(new LicensingExceptionEventHandler(OnLicensingExceptionUI), ex);
        }

        private void OnLicensingExceptionUI(Exception ex)
        {
            MessageBox.Show("Error: " + ex.Message);
            Close();
        }

        private LicenseTemplate CreateLicenseKeyTemplate()
        {
            int filedCnt = 1;
            int filedSize = 100 * 8; // bit
            LicenseTemplate template = new LicenseTemplate();

            template.NumberOfGroups = 5;
            template.CharactersPerGroup = 5;
            template.Encoding = LicenseKeyEncoding.Base32X;
            template.GroupSeparator = "-";
            template.DataSize = 22; // the license keys will hold 30 bits of data
            template.SignatureSize = 103; // 120 bits = ((characters per group * number of groups) * bits per character) - data size ; 120 = ((6 * 5) * 5) - 30
            template.ValidationDataSize = 0;

            template.SetPrivateKey("AQnD07PnEw7CRi8=");
            template.SetPublicKeyCertificate("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=");

            template.AddDataField("ProductId",
                              LicenseKeyFieldType.Integer,
                              8,   // the field is has a size of 8 bits (it can hold 2^8 = 256 different product id's)
                              0);  // field starts at position 0 in the license key data

            template.AddDataField("ExpirationDate",            // field name
                              LicenseKeyFieldType.Date14, // field is regarded as a 14-bit date (year must be between 2010 and 2041)
                              14, // field is 14 bits long (4 bits for month, 5 bits for day, 5 bits for year)
                              8  // field starts at position 16 in license key data bits (bits 0-7 and 8-15 are occupied by the product id and feature bits)
                              );

            //template.ValidationDataSize = filedSize * filedCnt;
            //template.AddValidationField("CustomerName", LicenseKeyFieldType.String, filedSize, filedSize * 0);

            return template;
            /*
            return new LicenseTemplate(5, // number of groups of characters in the license key  
                                    5, // number of character in each group
                                    MainForm.publicKeyCertificate, // the public key used to verify the license key signature
                                    null, // private key is not needed for license key validation ! 
                                    109,  // signature size of the license key is 109 bits
                                    16,   // data size embedded in the license key is 16 bits
                                    "ProductId" // this is the name of the data field embedded in the license key
                                    );
             * */
        }

        LicensingClient licensingClient;
        string licenseKey;
        string hardwareId;
        string activationKey;
        bool isActivated;
        DateTime licenseExpirationDate;
        bool clockManipulationDetected;
    }
}

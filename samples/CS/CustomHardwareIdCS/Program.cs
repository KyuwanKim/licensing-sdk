﻿// This samples demonstrates the use of custom hardware id strings with the SoftActivate Licensing SDK

using System; 
using System.Collections.Generic;
using System.Text;
using SoftActivate.Licensing;

namespace CustomHardwareIdCS
{
    class Program
    {
        const int SAMPLE_PRODUCT_ID_NO_VALIDATION_FIELDS = 12345;
        const int SAMPLE_PRODUCT_ID_WITH_VALIDATION_FIELDS = 54321;

        class CustomHardwareIdLicensingClient : LicensingClient
        {
            // this constructor is used during the activation process
            public CustomHardwareIdLicensingClient(string licensingServiceUrl, LicenseTemplate keyTemplate, string licenseKey, byte[] licenseKeyValidationData, int productId)
                : base(licensingServiceUrl, keyTemplate, licenseKey, licenseKeyValidationData, productId)
            {

            }

            // this constructor is used during the validation process
            public CustomHardwareIdLicensingClient(LicenseTemplate keyTemplate, string licenseKey, byte[] licenseKeyValidationData, string hardwareId, string activationKey)
                : base(keyTemplate, licenseKey, licenseKeyValidationData, hardwareId, activationKey)
            {

            }

            public override string GetCurrentHardwareId()
            {
                // here you have the opportunity to return a (static) string which is created from the information taken from the current computer's hardware components
                // for example, you can return the MAC address of the first network adapter, or a concatenation of information taken from many hardware components
                return "My Custom Hardware Id String";
            }

            public override bool MatchCurrentHardwareId(string hardwareId)
            {
                // here you have the opportunity to compare a provided hardware id (usually the initial hardware id used for activation) to the current computer hardware id 
                // and decide of they match or not. The simplest comparison is a direct string comparison, however this is suboptimal for most cases. 
                // Licensing SDK's original routines do much more than just a direct string comparison of the provided hardware id 
                // in order to decide if it comes from the current computer or not, it is recommended to not override these methods unless you know what you are doing
                return (hardwareId == "My Custom Hardware Id String");
            }
        }

        static void Main(string[] args)
        {
            try
            {
                // Entering the purchased developer key is the first step in using the SDK for license key generation and private/public key pair generation
                // Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
                // where you will only validate license keys. This call is only needed for license key generation and public/private key pair creation
                // You will only call this method in your key generator application, and you will set it in the configuration file of the licensing service

                // The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
                // You if you purchased the SDK, replace this key with your purchased key.

                // Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
                SDKRegistration.SetLicenseKey("DXBUT-DLKR4-KHV6E-N6H5J-25VDD");

                string productKey;

                // this should be read from the settings, but for the scope of this sample, we set it with the correct hardware id for this computer
                string hardwareId = "";

                // the activation key should be read from product settings, and saved back after a successful activation
                string activationKey = "";

                // this template must correspond to the activation server template
                LicenseTemplate template = new LicenseTemplate();
                template.CharactersPerGroup = 5;
                template.NumberOfGroups = 5;
                template.SignatureSize = 109;
                template.SetPublicKeyCertificate("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=");

                // The next paragraph of lines is used to generate a sample product key, for demonstration purposes
                // In real world scenarios, the product key is read from product settings so the lines below are not needed
                template.SetPrivateKey("AQnD07PnEw7CRi8=");
                template.DataSize = 16;
                template.AddDataField("KeyData", LicenseKeyFieldType.Integer, 16, 0);
                
                // optionally, link the license key to a customer name. Note: this does not include the customer name into the license key !
                template.AddValidationField("CustomerName", LicenseKeyFieldType.String, 100 * 8, 0);

                KeyGenerator generator = new KeyGenerator(template);
                
                // set data field values (these are included in the license key)
                generator.SetKeyData("KeyData", 12345);
                 
                // set validation field values (this are not included in the license key, thus they are also required when you validate the key)
                generator.SetValidationData("CustomerName", "John Doe");
                
                // Generate the license key
                productKey = generator.GenerateKey();

                byte[] licenseKeyValidationData = null;

                // The next paragraph should only be used if there are validation fields in the license key template.
                // We create a KeyValidator in order to obtain the license key validation data which needs to be passed to the LicensingClient instance
                // If we have no validation fields, we will pass null to the LicensingClient
                KeyValidator validator = new KeyValidator(template, productKey);
                
                // note that validation fields need to be set at validation, since they are not included in the license key
                validator.SetValidationData("CustomerName", "John Doe");
                
                if (validator.IsKeyValid())
                {
                    // by calling the QueryValidationData without a field name, we get the entire validation data
                    licenseKeyValidationData = validator.QueryValidationData(null);
                }

                // SoftActivate Licensing SDK includes an activation server which you can install on your own servers 
                // or in shared hosting environments.
                // Below a sample working server URL is provided. In production you will need to replace this with your own installed service URL

                // We first assume that the license is already activated, so we use a constructor that validates the license locally 
                // (does not connect to the activation server).
                CustomHardwareIdLicensingClient licensingClient = new CustomHardwareIdLicensingClient(template,
                                                                      productKey, /* product license read from product's settings */
                                                                      licenseKeyValidationData,
                                                                      hardwareId,
                                                                      activationKey
                                                                     );

                if (!licensingClient.IsLicenseValid())
                {
                    Console.WriteLine("Product needs to be activated");
                    Console.Write("Requesting license activation from server...");

                    int productIdOnLicensingServer = (licenseKeyValidationData == null) ? SAMPLE_PRODUCT_ID_NO_VALIDATION_FIELDS :
                                                                                          SAMPLE_PRODUCT_ID_WITH_VALIDATION_FIELDS;

                    // Now that we know that the license is not valid, we try to activate the license. 
                    // (Most developers choose to just display an error box to the customer and let him press an "Activate" button instead of attempting to activate silently)
                    // We use the constructor in which we set the activation server and product id
                    licensingClient = new CustomHardwareIdLicensingClient("http://192.168.1.114/",
                                                          template,
                                                          productKey,
                                                          licenseKeyValidationData, 
                                                          productIdOnLicensingServer);

                    try
                    {
                        licensingClient.AcquireLicense();
                        Console.WriteLine("done.");
                    }
                    catch (Exception ex)
                    {
                        Console.Write("Exception caught during activation (is the server started ?) ! " + ex.Message);
                    }

                    if (!licensingClient.IsLicenseValid())
                    {
                        string activationStatusStr;

                        switch (licensingClient.LicenseStatus)
                        {
                            case LICENSE_STATUS.InvalidActivationKey:
                                activationStatusStr = "invalid activation key";
                                break;

                            case LICENSE_STATUS.InvalidHardwareId:
                                activationStatusStr = "invalid hardware id";
                                break;

                            case LICENSE_STATUS.Expired:
                                {
                                    // the license expiration date returned by LicenseExpirationDate property is only valid if IsLicenseValid() returns true, 
                                    // or if IsLicenseValid() returns false and LicenseStatus returns LicenseStatus.Expired
                                    DateTime expDate = licensingClient.LicenseExpirationDate;
                                    activationStatusStr = "license expired (expiration date: " + expDate.Month + "/" + expDate.Day + "/" + expDate.Year + ")";
                                }
                                break;

                            default:
                                activationStatusStr = "unknown";
                                break;
                        }

                        Console.WriteLine("Activation validation failed. Reason: " + activationStatusStr);
                    }
                    else
                    {
                        Console.WriteLine("Activation was successful. ");
                        // After successful activation, save the activation key and the hardware id, along with the license key.
                        // You need to use this hardware id next time you run the application, and not the current computer hardware id.
                        // (the hardware id matching of the hardware id used for activation with the current hardware id is done 
                        // automatically when you call IsLicenseActivated)
                        // These do not have to be hidden or obfuscated. 
                        Console.WriteLine("License Key: " + productKey);
                        Console.WriteLine("Activation Key: " + licensingClient.ActivationKey);
                        Console.WriteLine("Hardware Id: " + licensingClient.HardwareId);

                        // the license expiration date returned by LicenseExpirationDate property is only valid if IsLicenseValid() returns true, 
                        // or if IsLicenseValid() returns false and LicenseStatus returns LicenseStatus.Expired
                        DateTime expDate = licensingClient.LicenseExpirationDate;

                        if (expDate == DateTime.MinValue)
                            Console.WriteLine("License Expiration Date: Never");
                        else
                            Console.WriteLine("License Expiration Date: " + expDate.Month + "/" + expDate.Day + "/" + expDate.Year);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write("Exception caught ! " + ex.Message);
            }

            Console.WriteLine("\nPress any key...");
            Console.ReadKey();
        }
    }
}

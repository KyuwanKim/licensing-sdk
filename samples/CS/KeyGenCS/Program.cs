//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

// This sample demonstrates product key generation and validation

using System;
using System.Collections.Generic;
using System.Text;
using SoftActivate.Licensing;

namespace KeyGen
{
    class Program
    {
        static void Main(string[] args)
        {
            // Entering the purchased developer key is the first step in using the SDK for license key generation and private/public key pair generation
            // Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
            // where you will only validate license keys. This call is only needed for license key generation and public/private key pair creation
            // You will only call this method in your key generator application, and you will set it in the configuration file of the licensing service

            // The key below is a demo key which only allows certain public/private key pairs to be generated and used for license key generation/validation purposes.
            // You if you purchased the SDK, replace this key with your purchased key.

            // Note that you do NOT need (and neither is it recommended for obvious reasons) to set the purchased developer license key in your actual end-user application
            SDKRegistration.SetLicenseKey("DXBUT-DLKR4-KHV6E-N6H5J-25VDD"); //VCMDJ-L6PU6-ULWGV-5EPSP-24TNP

            // the license key template specifies how the license key will look like, what data will it contain and how secure the license key will be
	        LicenseTemplate  tmpl = new LicenseTemplate();

            // we want our license keys to look like this: XXXXX-XXXXX-XXXXX-XXXXX-XXXXX-XXXXX (6 groups of 5 characters)

            tmpl.NumberOfGroups = 6;
            tmpl.CharactersPerGroup = 5;
            tmpl.Encoding = LicenseKeyEncoding.Base32X;
            tmpl.GroupSeparator = "-";
            tmpl.DataSize = 30; // the license keys will hold 30 bits of data
            tmpl.SignatureSize = 120; // 120 bits = ((characters per group * number of groups) * bits per character) - data size ; 120 = ((6 * 5) * 5) - 30
            
            // specify that we want to link the license keys to additional data (that will not be included in the generated keys, 
            // but it is required for both key generation and key validation. For example, the customer email or name
            tmpl.ValidationDataSize = 100 * 8;

            // Below we use a sample private/public key pair for license key generation and validation purposes, respectively. 
            // In production, you need your own private/public key pair because you need to keep your private key secret
            // You can generate a new key pair using the Licensing Tool (included with the SDK),
            // or programatically with LicenseTemplate.GenerateSigningKeyPair() and then retrieve the generated pair with LicenseTemplate.GetPrivateKey() 
            // and LicenseTemplate.GetPublicKeyCertificate()

            // the private key is used for key generation. NEVER include this in the software that reaches your customers, only in your key generators !
            tmpl.SetPrivateKey("AQnD07PnEw7CRi8=");

            // the public key certificate (which is a public key and some additional information) is used for license key validation
            // this certificate is included in your actual software that reaches the customers
            tmpl.SetPublicKeyCertificate("AGEbDQELdoxzyMNu7n46whOSAEr234LdyefOU9b6P391Wz3wdgA=");

            // Now define the data portion of the license key. Actual field names are not included in the license keys, only the field values are included.

            // Add an 8-bit field (256 possible values) specifying the product id, 
            // to distinguish between different products from the same company, 
            // or between different editions of the same product (Professional, Standard, Demo, Trial, etc.)
            tmpl.AddDataField("ProductId", 
                              LicenseKeyFieldType.Integer, 
                              8,   // the field is has a size of 8 bits (it can hold 2^8 = 256 different product id's)
                              0);  // field starts at position 0 in the license key data

            // Add an 8-bit field specifying which features are enabled 
            // (in this example there are up to 8 different features that we can enable/disable)
            tmpl.AddDataField("EnabledFeatures", LicenseKeyFieldType.Integer, 
                              8,  // this field is 8 bits in size
                              8); // this field starts at position 8 in license key data bits (bits 0-7 are occupied by the product id)

            // Add a 14-bit field specifying the product expiration date, between 1/1/2010 and 12/31/2041
            tmpl.AddDataField("ExpirationDate",            // field name
                              LicenseKeyFieldType.Date14, // field is regarded as a 14-bit date (year must be between 2010 and 2041)
                              14, // field is 14 bits long (4 bits for month, 5 bits for day, 5 bits for year)
                              16  // field starts at position 16 in license key data bits (bits 0-7 and 8-15 are occupied by the product id and feature bits)
                              );

            tmpl.AddValidationField("UserName", LicenseKeyFieldType.String, 100 * 8, 0);
            
            string xml = tmpl.SaveXml(true);

            // create a key generator based on the template above
            KeyGenerator keyGen = new KeyGenerator(tmpl);

            // set the product id
            keyGen.SetKeyData("ProductId", PRODUCT_ID); 

            // set some features as enabled
            keyGen.SetKeyData("EnabledFeatures", FEATURE_1 | FEATURE_5 | FEATURE_7);

            // add an expiration date for this license key, one year from now
            keyGen.SetKeyData("ExpirationDate", DateTime.Today.AddYears(1));

            keyGen.SetValidationData("UserName", "John Doe");

            // now generate the license key
            string licenseKey = keyGen.GenerateKey();

            // validate the generated key. This sequence of code is also used in the actual product to validate the entered license key
            KeyValidator keyVal = new KeyValidator(tmpl);
            keyVal.SetKey(licenseKey);

            keyVal.SetValidationData("UserName", "John Doe"); // the key will not be valid if you set a different user name than the one you have set at key generation

            if (keyVal.IsKeyValid())
            {
                Console.WriteLine("The generated license key is: " + licenseKey);

                // now read the expiration date from the license key
                DateTime expirationDate = keyVal.QueryDateKeyData("ExpirationDate");

                Console.WriteLine("The expiration date is " + expirationDate.ToString("MM/dd/yyyy"));

                int enabledFeatures = keyVal.QueryIntKeyData("EnabledFeatures");

                for (int i = 0; i < 8; i++)
                    // test the bit corresponding to feature i
                    if ((enabledFeatures & (1 << i)) != 0)
                        Console.WriteLine("Feature " + (i + 1).ToString() + " is enabled");
                    else
                        Console.WriteLine("Feature " + (i + 1).ToString() + " is disabled");
            }
            else
            {
                Console.WriteLine("Error creating/validating license key !\n");
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        // pack a DateTime into a 14-bit integer (year must be between 2010-2041)
        static int PackDate(DateTime date)
        {
            return ((date.Month - 1) << 10) | ((date.Day - 1) << 5) | (date.Year - 2010);
        }

        // unpack a 14-bit integer into a DateTime
        static DateTime UnpackDate(int packedDate)
        {
            return new DateTime(2010 + (packedDate & 0x1F), 1 + (packedDate >> 10), 1 + ((packedDate >> 5) & 0x1F));
        }

        const int PRODUCT_ID = 0x12;

        const int FEATURE_1 = 0x1;
        const int FEATURE_2 = 0x2;
        const int FEATURE_3 = 0x4;
        const int FEATURE_4 = 0x8;
        const int FEATURE_5 = 0x10;
        const int FEATURE_6 = 0x20;
        const int FEATURE_7 = 0x40;
        const int FEATURE_8 = 0x80;
    }
}

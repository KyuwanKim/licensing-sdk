﻿using System;
using System.Collections.Generic;
using System.Text;
using SoftActivate.Licensing;

namespace EasyProtectionCS
{
    class Program
    {
        static void Main(string[] args)
        {
            // let the license manager handle registration/trial UI for you
            if (!(new UILicenseManager(licenseTemplate)).Run())
            {
                // either the trial is expired or product is not registered, exit your product
                return;
            }

            // you may continue running your product

            Console.WriteLine("Program is either in trial mode or registered and is allowed to execute. Press any key to exit...");
            
            Console.ReadKey();
        }

        // generate this template with SoftActivate Licensing Tool and replace it here (replace " with "")
        // IMPORTANT: Make sure you remove the <SignaturePrivateKey>...</SignaturePrivateKey> from your template !
        static string licenseTemplate =
          @"<?xml version=""1.0"" encoding=""utf-8""?>
            <LicenseTemplate version=""3"">
              <Properties>
                  <General>
                    <ProductName>Sample Product</ProductName>
                    <CompanyName>Sample Company</CompanyName>
                    <ProductSupportUrl>http://www.google.com/</ProductSupportUrl>
                    <BuyNowUrl>http://www.google.com/</BuyNowUrl>
                  </General>
                  <Trial>
                    <TrialLicenseKey>X62DG-94SDT-A4TBZ-949CK-KMZB5</TrialLicenseKey>
                  </Trial>
              </Properties>
              <LicenseKey encoding=""BASE32X"" characterGroups=""5"" charactersPerGroup=""5"" groupSeparator=""-"" header="""" footer="""">
                <Data>
                  <DataFields>
                    <Field name=""ProductId"" type=""Integer"" size=""16"" offset=""0"" />
                  </DataFields>
                  <ValidationFields>
                    <!-- <Field name=""RegistrationName"" type=""String"" size=""800"" offset=""0"" /> -->
                  </ValidationFields>
                </Data>
                <Signature size=""109"">
                  <SignaturePublicKey>AI0bDQELdoxzyMNu7n46whOSAEon0PzzErF7AiD1r/HhLy4U1wA=</SignaturePublicKey>
                  <SignaturePrivateKey>AQnD07PnEw7CRi8=</SignaturePrivateKey>
                  <SigningServiceUrl>http://192.168.1.114/</SigningServiceUrl>
                  <SigningServiceTemplateId>12345</SigningServiceTemplateId>
                </Signature>
              </LicenseKey>
            </LicenseTemplate>";
    }
}

//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

#include "precomp.h"
#include "licensing.h"
#include "template.h"
#include "base32.h"
#include "base64.h"
#include "sha1.h"
#include "except.h"
#include "BitStream.h"
#include "uniconv.h"
#include "generator.h"

template<typename _XCHAR>
KeyGeneratorT<_XCHAR> * KeyGeneratorT<_XCHAR>::Create()
{
	return new KeyGeneratorT<_XCHAR>();
}

template<typename _XCHAR>
void KeyGeneratorT<_XCHAR>::Destroy(KeyGeneratorT<_XCHAR> * obj)
{
	delete obj;
}

template <typename _XCHAR>
KeyGeneratorT<_XCHAR>::KeyGeneratorT():
m_Impl( *new KeyGeneratorImpl())
{

}

template<typename _XCHAR>
KeyGeneratorT<_XCHAR>::KeyGeneratorT(const KeyTemplateT<_XCHAR> * templ):
m_Impl( *new KeyGeneratorImpl(&templ->m_Impl) )
{

}

template<typename _XCHAR>
KeyGeneratorT<_XCHAR>::~KeyGeneratorT()
{
	delete & m_Impl;
}

template<>
void KeyGeneratorT<char>::SetKeyData(const char * fieldName, const void * buf, int len)
{
	USES_CONVERSION;
	m_Impl.KeyGeneratorImpl::SetKeyData(A2T(fieldName), buf, len);
}

template<>
void KeyGeneratorT<wchar_t>::SetKeyData(const wchar_t * fieldName, const void * buf, int len)
{
	USES_CONVERSION;
	m_Impl.KeyGeneratorImpl::SetKeyData(W2T(fieldName), buf, len);
}

template<>
void KeyGeneratorT<char>::SetKeyData(const char * fieldName, int data)
{
	USES_CONVERSION;
	m_Impl.SetKeyData(A2T(fieldName), data);
}

template<>
void KeyGeneratorT<wchar_t>::SetKeyData(const wchar_t * fieldName, int data)
{
	USES_CONVERSION;
	m_Impl.SetKeyData(W2T(fieldName), data);
}

template<>
void KeyGeneratorT<char>::SetKeyData(const char * fieldName, const char * data)
{
	USES_CONVERSION;

	m_Impl.SetKeyData(A2T(fieldName), A2T(data));
}

template<>
void KeyGeneratorT<wchar_t>::SetKeyData(const wchar_t * fieldName, const wchar_t * data)
{
	USES_CONVERSION;

	m_Impl.SetKeyData(W2T(fieldName), W2T(data));
}

template<>
void KeyGeneratorT<char>::SetKeyData(const char * fieldName, int year, int month, int day)
{
	USES_CONVERSION;
	m_Impl.SetKeyData(A2T(fieldName), year, month, day);
}

template<>
void KeyGeneratorT<wchar_t>::SetKeyData(const wchar_t * fieldName, int year, int month, int day)
{
	USES_CONVERSION;
	m_Impl.SetKeyData(W2T(fieldName), year, month, day);
}

template<>
void KeyGeneratorT<char>::SetValidationData(const char * fieldName, const void * buf, int len)
{
	USES_CONVERSION;
	m_Impl.SetValidationData(A2T(fieldName), buf, len);
}

template<>
void KeyGeneratorT<wchar_t>::SetValidationData(const wchar_t * fieldName, const void * buf, int len)
{
	USES_CONVERSION;
	m_Impl.SetValidationData(W2T(fieldName), buf, len);
}

template<>
void KeyGeneratorT<char>::SetValidationData(const char * fieldName, int data)
{
	USES_CONVERSION;
	m_Impl.SetValidationData(A2T(fieldName), data);
}

template<>
void KeyGeneratorT<wchar_t>::SetValidationData(const wchar_t * fieldName, int data)
{
	USES_CONVERSION;
	m_Impl.SetValidationData(W2T(fieldName), data);
}

template<>
void KeyGeneratorT<char>::SetValidationData(const char * fieldName, const char * data)
{
	USES_CONVERSION;
	m_Impl.SetValidationData(A2T(fieldName), A2T(data));
}

template<>
void KeyGeneratorT<wchar_t>::SetValidationData(const wchar_t * fieldName, const wchar_t * data)
{
	USES_CONVERSION;
	m_Impl.SetValidationData(W2T(fieldName), W2T(data));
}

template<typename _XCHAR>
void KeyGeneratorT<_XCHAR>::SetKeyTemplate(const KeyTemplateT<_XCHAR> & templ)
{
	m_Impl.SetKeyTemplate(&templ.m_Impl);
}

template<typename _XCHAR>
void KeyGeneratorT<_XCHAR>::SetKeyTemplate(const KeyTemplateT<_XCHAR> * templ)
{
	m_Impl.SetKeyTemplate(&templ->m_Impl);
}

template<>
const char * KeyGeneratorT<char>::GenerateKey()
{
#ifdef _UNICODE
	USES_CONVERSION;
	static std::string key;
	key = W2A(m_Impl.GenerateKey());
	return key.c_str();
#else
	return m_Impl.GenerateKey();
#endif
}

template<>
const wchar_t * KeyGeneratorT<wchar_t>::GenerateKey()
{
#ifdef _UNICODE
	return m_Impl.GenerateKey();
#else
	USES_CONVERSION;
	static std::wstring key;
	key = A2W(m_Impl.GenerateKey());
	return key.c_str();
#endif
}

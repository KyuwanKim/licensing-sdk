#include "precomp.h"
#include "uniconv.h"
#include "licensingclient.h"

template<>
LicensingClientT<char>::LicensingClientT() :
m_Impl(*(new LicensingClientImpl(this, false)))
{

}

template<>
LicensingClientT<wchar_t>::LicensingClientT():
m_Impl(*(new LicensingClientImpl(this, true)))
{

}

template<typename _XCHAR>
LicensingClientT<_XCHAR>::~LicensingClientT()
{
	delete & m_Impl;
}

template<typename _XCHAR>
void LicensingClientT<_XCHAR>::SetLicenseKeyTemplate(const KeyTemplateT<_XCHAR> & tmpl)
{
	m_Impl.SetLicenseKeyTemplate(tmpl.m_Impl);
}

template<>
void LicensingClientT<char>::SetLicenseKey(const char * key)
{
	USES_CONVERSION;
	m_Impl.SetLicenseKey(A2T(key));
}

template<>
void LicensingClientT<wchar_t>::SetLicenseKey(const wchar_t * key)
{
	USES_CONVERSION;
	m_Impl.SetLicenseKey(W2T(key));
}

template<>
void LicensingClientT<char>::SetActivationKey(const char * key)
{
	USES_CONVERSION;
	m_Impl.SetActivationKey(A2T(key));
}

template<>
void LicensingClientT<wchar_t>::SetActivationKey(const wchar_t * key)
{
	USES_CONVERSION;
	m_Impl.SetActivationKey(W2T(key));
}

template<>
const char * LicensingClientT<char>::GetActivationKey()
{
#ifdef _UNICODE
	USES_CONVERSION;
	static std::string key;
	key = W2A(m_Impl.GetActivationKey());
	return key.c_str();
#else
	return m_Impl.GetActivationKey();
#endif
}

template<>
const wchar_t * LicensingClientT<wchar_t>::GetActivationKey()
{
#ifdef _UNICODE
	return m_Impl.GetActivationKey();
#else
	USES_CONVERSION;
	static std::wstring key;
	key = A2W(m_Impl.GetActivationKey());
	return key.c_str();
#endif
}

template<>
void LicensingClientT<char>::SetHardwareId(const char * hwid)
{
	USES_CONVERSION;
	m_Impl.SetHardwareId(A2T(hwid));
}

template<>
void LicensingClientT<wchar_t>::SetHardwareId(const wchar_t * hwid)
{
	USES_CONVERSION;
	m_Impl.SetHardwareId(W2T(hwid));
}

template<>
const char * LicensingClientT<char>::GetHardwareId()
{
#ifdef _UNICODE
	USES_CONVERSION;
	static std::string hwid;
	hwid = W2A(m_Impl.GetHardwareId());
	return hwid.c_str();
#else
	return m_Impl.GetHardwareId();
#endif
}

template<>
const wchar_t * LicensingClientT<wchar_t>::GetHardwareId()
{
#ifdef _UNICODE
	return m_Impl.GetHardwareId();
#else
	USES_CONVERSION;
	static std::wstring hwid;
	hwid = A2W(m_Impl.GetHardwareId());
	return hwid.c_str();
#endif
}

template<>
void LicensingClientT<char>::SetLicensingServiceUrl(const char * url)
{
	USES_CONVERSION;
	m_Impl.SetLicensingServiceUrl(A2T(url));
}

template<>
void LicensingClientT<wchar_t>::SetLicensingServiceUrl(const wchar_t * url)
{
	USES_CONVERSION;
	m_Impl.SetLicensingServiceUrl(W2T(url));
}

template<typename _XCHAR>
void LicensingClientT<_XCHAR>::SetProductId(int productId)
{
	m_Impl.SetProductId(productId);
}

template<typename _XCHAR>
void LicensingClientT<_XCHAR>::AcquireLicense()
{
	m_Impl.AcquireLicense();
}

template<typename _XCHAR>
bool LicensingClientT<_XCHAR>::IsLicenseValid()
{
	return m_Impl.IsLicenseValid();
}

template<typename _XCHAR>
void LicensingClientT<_XCHAR>::SetLicenseKeyValidationData(void * data, int len)
{
	return m_Impl.SetLicenseKeyValidationData(data, len);
}

template<typename _XCHAR>
int LicensingClientT<_XCHAR>::GetLicenseActivationStatus()
{
	return m_Impl.GetLicenseActivationStatus();
}

template<typename _XCHAR>
void LicensingClientT<_XCHAR>::GetLicenseExpirationDate(int * year, int * month, int * day)
{
	m_Impl.GetLicenseExpirationDate(year, month, day);
}

template<>
const wchar_t * LicensingClientT<wchar_t>::GetCurrentHardwareId()
{
#ifdef _UNICODE
	return m_Impl.GetCurrentHardwareId();
#else
	USES_CONVERSION;
	static std::wstring currenthwid;
	currenthwid = A2W(m_Impl.GetCurrentHardwareId());
	return currenthwid.c_str();
#endif
}

template<>
const char * LicensingClientT<char>::GetCurrentHardwareId()
{
#ifdef _UNICODE
	USES_CONVERSION;
	static std::string currenthwid;
	currenthwid = W2A(m_Impl.GetCurrentHardwareId());
	return currenthwid.c_str();
#else
	return m_Impl.GetCurrentHardwareId();
#endif
}

template<>
bool LicensingClientT<char>::MatchCurrentHardwareId(const char * hwid)
{
	USES_CONVERSION;
	return m_Impl.MatchCurrentHardwareId(A2T(hwid));
}

template<>
bool LicensingClientT<wchar_t>::MatchCurrentHardwareId(const wchar_t * hwid)
{
	USES_CONVERSION;
	return m_Impl.MatchCurrentHardwareId(W2T(hwid));
}

template<typename _XCHAR>
void LicensingClientT<_XCHAR>::SetTimeValidationMethod(int method)
{
	return m_Impl.SetTimeValidationMethod(method);
}

//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

#include "precomp.h"
#include "licensing.h"
#include "base32.h"
#include "base64.h"
#include "sha1.h"
#include "except.h"
#include "BitStream.h"
#include "uniconv.h"
#include "template.h"
#include "generator.h"
#include "validator.h"

void * KeyGenerator_Create()
{
	return new(std::nothrow) KeyGeneratorImpl();
}

void KeyGenerator_Destroy(void * generator)
{
	delete static_cast<KeyGeneratorImpl *>(generator);
}

int KeyGenerator_SetKeyTemplate(void * generator, const void * tmpl)
{
	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyTemplate(static_cast<KeyTemplateImpl *>(const_cast<void *>(tmpl)));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetKeyDataA(void * generator, const char * fieldName, const void * data, int len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(A2T(fieldName), data, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetKeyDataW(void * generator, const wchar_t * fieldName, const void * data, int len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(W2T(fieldName), data, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetIntKeyDataA(void * generator, const char * fieldName, int data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(A2T(fieldName), data);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetIntKeyDataW(void * generator, const wchar_t * fieldName, int data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(W2T(fieldName), data);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetDateKeyDataA(void * generator, const char * fieldName, int year, int month, int day)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(A2T(fieldName), year, month, day);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetDateKeyDataW(void * generator, const wchar_t * fieldName, int year, int month, int day)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(W2T(fieldName), year, month, day);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetStringKeyDataA(void * generator, const char * fieldName, const char * data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(A2T(fieldName), A2T(data));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetStringKeyDataW(void * generator, const wchar_t * fieldName, const wchar_t * data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetKeyData(W2T(fieldName), W2T(data));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetValidationDataA(void * generator, const char * fieldName, const void * buf, int len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetValidationData(A2T(fieldName), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetValidationDataW(void * generator, const wchar_t * fieldName, const void * buf, int len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetValidationData(W2T(fieldName), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetIntValidationDataA(void * generator, const char * fieldName, int data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetValidationData(A2T(fieldName), data);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetIntValidationDataW(void * generator, const wchar_t * fieldName, int data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetValidationData(W2T(fieldName), data);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetStringValidationDataA(void * generator, const char * fieldName, const char * data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetValidationData(A2T(fieldName), A2T(data));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_SetStringValidationDataW(void * generator, const wchar_t * fieldName, const wchar_t * data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyGeneratorImpl *>(generator)->SetValidationData(W2T(fieldName), W2T(data));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_GenerateKeyA(void * generator, const char **key)
{
	try {
#ifdef _UNICODE
		*key = NULL;
#else
		*key = static_cast<KeyGeneratorImpl *>(generator)->GenerateKey();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyGenerator_GenerateKeyW(void * generator, const wchar_t **key)
{
	try {
#ifdef _UNICODE
		*key = static_cast<KeyGeneratorImpl *>(generator)->GenerateKey();
#else
		*key = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

void * KeyValidator_Create()
{
	return new(std::nothrow) KeyValidatorImpl();
}

void KeyValidator_Destroy(void * validator)
{
	delete static_cast<KeyValidatorImpl *>(validator);
}

int KeyValidator_SetKeyTemplate(void * validator, const void * tmpl)
{
	try {
		static_cast<KeyValidatorImpl *>(validator)->SetKeyTemplate(static_cast<KeyTemplateImpl *>(const_cast<void *>(tmpl)));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetKeyA(void * validator, const char * key)
{
	USES_CONVERSION;
	try {
		static_cast<KeyValidatorImpl *>(validator)->SetKey(A2T(key));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetKeyW(void * validator, const wchar_t * key)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->SetKey(W2T(key));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetValidationDataA(void * validator, const char * fieldName, const void * buf, int len)
{
	USES_CONVERSION;
	try {
		static_cast<KeyValidatorImpl *>(validator)->SetValidationData(A2T(fieldName), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetValidationDataW(void * validator, const wchar_t * fieldName, const void * buf, int len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->SetValidationData(W2T(fieldName), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetIntValidationDataA(void * validator, const char * fieldName, int data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->SetValidationData(A2T(fieldName), data); 
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetIntValidationDataW(void * validator, const wchar_t * fieldName, int data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->SetValidationData(W2T(fieldName), data); 
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetStringValidationDataA(void * validator, const char * fieldName, const char * data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->SetValidationData(A2T(fieldName), A2T(data));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_SetStringValidationDataW(void * validator, const wchar_t * fieldName, const wchar_t * data)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->SetValidationData(W2T(fieldName), W2T(data));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_IsKeyValid(void * validator)
{
	int status = STATUS_SUCCESS;

	try {
		if (!static_cast<KeyValidatorImpl *>(validator)->IsKeyValid())
			status = STATUS_INVALID_LICENSE_KEY;
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return status;
}

int KeyValidator_QueryKeyDataA(void * validator, const char * dataField, void * buf, int * len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->QueryKeyData(A2T(dataField), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryKeyDataW(void * validator, const wchar_t * dataField, void * buf, int * len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->QueryKeyData(W2T(dataField), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryIntKeyDataA(void * validator, const char * dataField, int * data)
{
	USES_CONVERSION;

	try {
		*data = static_cast<KeyValidatorImpl *>(validator)->QueryIntKeyData(A2T(dataField));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryIntKeyDataW(void * validator, const wchar_t * dataField, int * data)
{
	USES_CONVERSION;

	try {
		*data = static_cast<KeyValidatorImpl *>(validator)->QueryIntKeyData(W2T(dataField));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryDateKeyDataA(void * validator, const char * dataField, int * year, int * month, int * day)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->QueryDateKeyData(A2T(dataField), year, month, day);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryDateKeyDataW(void * validator, const wchar_t * dataField, int * year, int * month, int * day)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->QueryDateKeyData(W2T(dataField), year, month, day);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryValidationDataA(void * validator, const char * dataField, void * buf, int * len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->QueryValidationData(A2T(dataField), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyValidator_QueryValidationDataW(void * validator, const wchar_t * dataField, void * buf, int * len)
{
	USES_CONVERSION;

	try {
		static_cast<KeyValidatorImpl *>(validator)->QueryValidationData(W2T(dataField), buf, len);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

void * KeyTemplate_Create()
{
	return new(std::nothrow) KeyTemplateImpl();
}

void KeyTemplate_Destroy(void * tmpl)
{
	delete static_cast<KeyTemplateImpl *>(tmpl);
}

int KeyTemplate_SetVersion(void * tmpl, int version)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetVersion(version);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetVersion(void * tmpl, int * version)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->GetVersion();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetNumberOfGroups(void * tmpl, int numGroups)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetNumberOfGroups(numGroups);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetNumberOfGroups(void * tmpl, int * numGroups)
{
	try {
		*numGroups = static_cast<KeyTemplateImpl *>(tmpl)->GetNumberOfGroups();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetCharactersPerGroup(void * tmpl, int charsPerGroup)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetCharactersPerGroup(charsPerGroup);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetCharactersPerGroup(void * tmpl, int * charsPerGroup)
{
	try {
		*charsPerGroup = static_cast<KeyTemplateImpl *>(tmpl)->GetCharactersPerGroup();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetGroupSeparatorA(void * tmpl, const char * groupSep)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetGroupSeparator(A2T(groupSep));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetGroupSeparatorW(void * tmpl, const wchar_t * groupSep)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetGroupSeparator(W2T(groupSep));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetGroupSeparatorA(void * tmpl, const char **groupSep)
{
	try {
#ifdef _UNICODE
		*groupSep = NULL;
#else
		*groupSep = static_cast<KeyTemplateImpl *>(tmpl)->GetGroupSeparator();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetGroupSeparatorW(void * tmpl, const wchar_t **groupSep)
{
	try {
#ifdef _UNICODE
		*groupSep = static_cast<KeyTemplateImpl *>(tmpl)->GetGroupSeparator();
#else
		*groupSep = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetEncoding(void * tmpl, int encoding)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetEncoding(encoding);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetEncoding(void * tmpl, int * encoding)
{
	try {
		*encoding = static_cast<KeyTemplateImpl *>(tmpl)->GetEncoding();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetHeaderA(void * tmpl, const char * header)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetHeader(A2T(header));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetHeaderW(void * tmpl, const wchar_t * header)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetHeader(W2T(header));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetHeaderA(void * tmpl, const char ** header)
{
	try {
#ifdef _UNICODE
		*header = NULL;
#else
		*header = static_cast<KeyTemplateImpl *>(tmpl)->GetHeader();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetHeaderW(void * tmpl, const wchar_t ** header)
{
	try {
#ifdef _UNICODE
		*header = static_cast<KeyTemplateImpl *>(tmpl)->GetHeader();
#else
		*header = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetFooterA(void * tmpl, const char * footer)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetFooter(A2T(footer));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetFooterW(void * tmpl, const wchar_t * footer)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetFooter(W2T(footer));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetFooterA(void * tmpl, const char **footer)
{
	try {
#ifdef _UNICODE
		*footer = NULL;
#else
		*footer = static_cast<KeyTemplateImpl *>(tmpl)->GetFooter();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetFooterW(void * tmpl, const wchar_t **footer)
{
	try {
#ifdef _UNICODE
		*footer = static_cast<KeyTemplateImpl *>(tmpl)->GetFooter();
#else
		*footer = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetDataSize(void * tmpl, int sizeInBits)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetDataSize(sizeInBits);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetDataSize(void * tmpl, int * sizeInBits)
{
	try {
		*sizeInBits = static_cast<KeyTemplateImpl *>(tmpl)->GetDataSize();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_AddDataFieldA(void * tmpl, const char * fieldName, int fieldType, int fieldBitSize, int offset)
{
	USES_CONVERSION;
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->AddDataField(A2T(fieldName), fieldType, fieldBitSize, offset);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_AddDataFieldW(void * tmpl, const wchar_t * fieldName, int fieldType, int fieldBitSize, int fieldOffset)
{
	USES_CONVERSION;
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->AddDataField(W2T(fieldName), fieldType, fieldBitSize, fieldOffset);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_EnumDataFieldsA(void * tmpl, void **enumHandle, const char **fieldName, int * fieldType, int * fieldSize, int * fieldOffset)
{
	int status = STATUS_SUCCESS;

	try {
#ifdef _UNICODE
		status = STATUS_GENERIC_ERROR;
#else
		if (!static_cast<KeyTemplateImpl *>(tmpl)->EnumDataFields(enumHandle, fieldName, fieldType, fieldSize, fieldOffset))
			status = STATUS_GENERIC_ERROR;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return status;
}

int KeyTemplate_EnumDataFieldsW(void * tmpl, void **enumHandle, const wchar_t **fieldName, int * fieldType, int * fieldSize, int * fieldOffset)
{
	int status = STATUS_SUCCESS;

	try {
#ifdef _UNICODE
		if (!static_cast<KeyTemplateImpl *>(tmpl)->EnumDataFields(enumHandle, fieldName, fieldType, fieldSize, fieldOffset))
			status = STATUS_GENERIC_ERROR;
#else
		status = STATUS_GENERIC_ERROR;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return status;
}

int KeyTemplate_SetValidationDataSize(void * tmpl, int sizeInBits)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetValidationDataSize(sizeInBits);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetValidationDataSize(void * tmpl, int * sizeInBits)
{
	try {
		*sizeInBits = static_cast<KeyTemplateImpl *>(tmpl)->GetValidationDataSize();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_AddValidationFieldA(void * tmpl, const char * fieldName, int fieldType, int fieldBitSize, int offset)
{
	USES_CONVERSION;
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->AddValidationField(A2T(fieldName), fieldType, fieldBitSize, offset);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_AddValidationFieldW(void * tmpl, const wchar_t * fieldName, int fieldType, int fieldBitSize, int offset)
{
	USES_CONVERSION;
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->AddValidationField(W2T(fieldName), fieldType, fieldBitSize, offset);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_EnumValidationFieldsA(void * tmpl, void **enumHandle, const char **fieldName, int * fieldType, int * fieldSize, int * fieldOffset)
{
	int status = STATUS_SUCCESS;

	try {
#ifdef _UNICODE
		status = STATUS_GENERIC_ERROR;
#else
		if (!static_cast<KeyTemplateImpl *>(tmpl)->EnumValidationFields(enumHandle, fieldName, fieldType, fieldSize, fieldOffset))
			status = STATUS_GENERIC_ERROR;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return status;
}

int KeyTemplate_EnumValidationFieldsW(void * tmpl, void **enumHandle, const wchar_t **fieldName, int * fieldType, int * fieldSize, int * fieldOffset)
{
	int status = STATUS_SUCCESS;

	try {
#ifdef _UNICODE
		if (!static_cast<KeyTemplateImpl *>(tmpl)->EnumValidationFields(enumHandle, fieldName, fieldType, fieldSize, fieldOffset))
			status = STATUS_GENERIC_ERROR;
#else
		status = STATUS_GENERIC_ERROR;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return status;
}

int KeyTemplate_SetSignatureSize(void * tmpl, int signatureSize)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetSignatureSize(signatureSize);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetSignatureSize(void * tmpl, int * signatureSize)
{
	try {
		*signatureSize = static_cast<KeyTemplateImpl *>(tmpl)->GetSignatureSize();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_LoadXml(void * tmpl, const char * xmlTemplate)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->LoadXml(xmlTemplate);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SaveXml(void * tmpl, int savePrivateKey, const char **xmlTemplate)
{
	try {
		*xmlTemplate = static_cast<KeyTemplateImpl *>(tmpl)->SaveXml((savePrivateKey) ? true : false);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetSigningServiceUrlA(void * tmpl, const char * url)
{
	USES_CONVERSION;
	
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetSigningServiceUrl(A2T(url));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetSigningServiceUrlW(void * tmpl, const wchar_t * url)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetSigningServiceUrl(W2T(url));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetSigningServiceUrlA(void * tmpl, const char **url)
{
	try {
#ifdef _UNICODE
		*url = NULL;
#else
		*url = static_cast<KeyTemplateImpl *>(tmpl)->GetSigningServiceUrl();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetSigningServiceUrlW(void * tmpl, const wchar_t **url)
{
	try {
#ifdef _UNICODE
		*url = static_cast<KeyTemplateImpl *>(tmpl)->GetSigningServiceUrl();
#else
		*url = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetSigningServiceTemplateId(void * tmpl, int templateId)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetSigningServiceTemplateId(templateId);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetSigningServiceTemplateId(void * tmpl, int * templateId)
{
	try {
		*templateId = static_cast<KeyTemplateImpl *>(tmpl)->GetSigningServiceTemplateId();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyHelper_GetCurrentHardwareIdW(const wchar_t **hwid)
{
	try {
		*hwid = KeyHelperT<wchar_t>::GetCurrentHardwareId();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyHelper_GetCurrentHardwareIdA(const char **hwid)
{
	try {
		*hwid = KeyHelperT<char>::GetCurrentHardwareId();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}



int KeyHelper_MatchCurrentHardwareIdW(const wchar_t *hwid)
{
	try {
		if (!KeyHelperT<wchar_t>::MatchCurrentHardwareId(hwid))
			return STATUS_INVALID_LICENSE_KEY;
	}
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyHelper_MatchCurrentHardwareIdA(const char *hwid)
{
	try {
		if (!KeyHelperT<char>::MatchCurrentHardwareId(hwid))
			return STATUS_INVALID_LICENSE_KEY;
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int SDKRegistration_SetLicenseKeyA(const char * key)
{
	try {
		SDKRegistrationT<char>::SetLicenseKey(key);
	}
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int SDKRegistration_SetLicenseKeyW(const wchar_t * key)
{
	try {
		SDKRegistrationT<wchar_t>::SetLicenseKey(key);
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetPublicKeyCertificateA(void * tmpl, const char **base64Certificate)
{
	try {
#ifdef _UNICODE
		*base64Certificate = NULL;
#else
		*base64Certificate = static_cast<KeyTemplateImpl *>(tmpl)->GetPublicKeyCertificate();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetPublicKeyCertificateW(void * tmpl, const wchar_t **base64Certificate)
{
	try {
#ifdef _UNICODE
		*base64Certificate = static_cast<KeyTemplateImpl *>(tmpl)->GetPublicKeyCertificate();
#else
		*base64Certificate = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetPublicKeyCertificateA(void * tmpl, const char * base64Certificate)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetPublicKeyCertificate(A2T(base64Certificate));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetPublicKeyCertificateW(void * tmpl, const wchar_t * base64Certificate)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetPublicKeyCertificate(W2T(base64Certificate));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetPrivateKeyA(void * tmpl, const char **base64Key)
{
	try {
#ifdef _UNICODE
		*base64Key = NULL;
#else
		*base64Key = static_cast<KeyTemplateImpl *>(tmpl)->GetPrivateKey();
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GetPrivateKeyW(void * tmpl, const wchar_t **base64Key)
{
	try {
#ifdef _UNICODE
		*base64Key = static_cast<KeyTemplateImpl *>(tmpl)->GetPrivateKey();
#else
		*base64Key = NULL;
#endif
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetPrivateKeyA(void * tmpl, const char * base64Key)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetPrivateKey(A2T(base64Key));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_SetPrivateKeyW(void * tmpl, const wchar_t * base64Key)
{
	USES_CONVERSION;

	try {
		static_cast<KeyTemplateImpl *>(tmpl)->SetPrivateKey(W2T(base64Key));
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

int KeyTemplate_GenerateSigningKeyPair(void * tmpl)
{
	try {
		static_cast<KeyTemplateImpl *>(tmpl)->GenerateSigningKeyPair();
	} 
	catch (Exception * ex)
	{
		int status = ex->GetCode();
		ex->Destroy();
		return status;
	}
	catch (...)
	{
		return STATUS_GENERIC_ERROR;
	}

	return STATUS_SUCCESS;
}

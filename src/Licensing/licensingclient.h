#pragma once

#include <time.h>

#include "except.h"
#include "template.h"
#include "generator.h"
#include "validator.h"
#include "ntpclient.h"

class LicensingClientImpl
{
public:
	LicensingClientImpl(void * licensingClient, bool usesWideChar)
	{
		licenseKeyValidationData = NULL;
		licenseKeyValidationDataLen = 0;
		productId = -1;
		parentLicensingClient = licensingClient;
		parentUsesWideChar = usesWideChar;
		timeValidationMethod = PREFER_INTERNET_TIME;
	}

	~LicensingClientImpl()
	{
		if (licenseKeyValidationData)
			free(licenseKeyValidationData);
	}

	void SetLicenseKeyTemplate(const KeyTemplateImpl & tmpl)
	{
		activationKeyTemplate.SetCharactersPerGroup(tmpl.GetCharactersPerGroup());
		activationKeyTemplate.SetNumberOfGroups(tmpl.GetNumberOfGroups());
		activationKeyTemplate.SetGroupSeparator(tmpl.GetGroupSeparator());
		//activationKeyTemplate.SetDataSize(t.GetDataSize());
		activationKeyTemplate.SetSignatureSize(tmpl.GetSignatureSize());
		activationKeyTemplate.SetEncoding(tmpl.GetEncoding());
		activationKeyTemplate.SetPublicKeyCertificate(tmpl.GetPublicKeyCertificate());
	}

	void SetLicenseKey(const char_t * key)
	{
		licenseKey = key;
	}

	void SetActivationKey(const char_t * key)
	{
		activationKey = key;
	}

	const char_t * GetActivationKey()
	{
		return activationKey.c_str();
	}

	void SetHardwareId(const char_t * hwid)
	{
		hardwareId = hwid;
	}

	const char_t * GetHardwareId()
	{
		return hardwareId.c_str();
	}

	void SetProductId(int id)
	{
		productId = id;
	}

	int GetProductId()
	{
		return productId;
	}

	void SetLicenseKeyValidationData(void * data, unsigned len)
	{
		if (licenseKeyValidationData)
			free(licenseKeyValidationData);

		if ((licenseKeyValidationData = malloc(len)) == NULL)
			throw new KeyExceptionImpl(STATUS_GENERIC_ERROR, _T("insufficient memory"));

		memcpy(licenseKeyValidationData, data, len);

		licenseKeyValidationDataLen = len;
	}

	bool IsLicenseValid()
	{
		try 
		{
			if (!MatchCurrentHardwareIdCallback(hardwareId.c_str()))
			{
				activationStatus = STATUS_INVALID_HARDWARE_ID;
				return false;
			}

			int keyLen = activationKeyTemplate.GetNumberOfGroups() * activationKeyTemplate.GetCharactersPerGroup() 
						 + (activationKeyTemplate.GetNumberOfGroups() - 1) * _tcslen(activationKeyTemplate.GetGroupSeparator());

			int hwidLen = hardwareId.length();

			activationKeyTemplate.SetDataSize(activationKeyTemplate.GetCharactersPerGroup() * activationKeyTemplate.GetNumberOfGroups() * activationKeyTemplate.GetEncoding() - activationKeyTemplate.GetSignatureSize());
			activationKeyTemplate.SetValidationDataSize((keyLen + hwidLen + licenseKeyValidationDataLen) * 8);

			activationKeyTemplate.AddDataField(_T("ExpirationDate"), FIELD_TYPE_RAW, 16, 0);
			activationKeyTemplate.AddValidationField(_T("LicenseKey"), FIELD_TYPE_STRING, keyLen * 8, 0);
			activationKeyTemplate.AddValidationField(_T("HardwareId"), FIELD_TYPE_STRING, hwidLen * 8, keyLen * 8);

			if (licenseKeyValidationData != NULL)
			{
				activationKeyTemplate.AddValidationField(_T("LicenseKeyValidationData"), FIELD_TYPE_RAW, licenseKeyValidationDataLen * 8, (keyLen + hwidLen) * 8);
			}

			KeyValidatorImpl validator(&activationKeyTemplate);

			validator.SetValidationData(_T("LicenseKey"), licenseKey.c_str());
			validator.SetValidationData(_T("HardwareId"), hardwareId.c_str());
		
			if (licenseKeyValidationData)
				validator.SetValidationData(_T("LicenseKeyValidationData"), licenseKeyValidationData, licenseKeyValidationDataLen);
		
			try {
				validator.SetKey(activationKey.c_str());
			}
			catch(Exception * ex)
			{
				ex->Destroy();
				activationStatus = STATUS_INVALID_ACTIVATION_KEY;
				return false;
			}

			if (!validator.IsKeyValid())
			{
				activationStatus = STATUS_INVALID_ACTIVATION_KEY;
				return false;
			}

			unsigned char rawKeyData[2];
			int rawKeyDataBits = 16;

			validator.QueryKeyData(_T("ExpirationDate"), rawKeyData, &rawKeyDataBits);

			unsigned short keyData = (unsigned short)(((unsigned short)rawKeyData[0]) << 8 | rawKeyData[1]);

			memset(&licenseExpirationTime, 0, sizeof(licenseExpirationTime));

			if (keyData != 0)
			{
				licenseExpirationTime.tm_year = (2000 + (keyData >> 9)) - 1900;
				licenseExpirationTime.tm_mon = ((keyData & 0x01FF) >> 5) - 1;
				licenseExpirationTime.tm_mday = keyData & 0x001F;

				time_t currentTime;

				if (timeValidationMethod != USE_LOCAL_TIME)
				{
					currentTime = NTPClient::GetCurrentTimeUTC();

					if (currentTime == 0) // if couldn't obtain internet time
					{
						if (timeValidationMethod == USE_INTERNET_TIME) // internet time was mandatory
						{
							activationStatus = STATUS_NET_ERROR;
							return false;
						}

						// fallback to local time
						currentTime = time(NULL);
					}
				} else
					currentTime = time(NULL);

				if (difftime(mktime(&licenseExpirationTime), currentTime) < 0)
				{
					activationStatus = STATUS_LICENSE_EXPIRED;
					return false;
				}
			}

			activationStatus = STATUS_SUCCESS;
			
			return true;
		}
		catch (Exception * ex)
		{
			ex->Destroy();

			activationStatus = STATUS_GENERIC_ERROR;
			return false;
		}
		catch (...)
		{
			activationStatus = STATUS_GENERIC_ERROR;
			return false;
		}
	}

	void AcquireLicense()
	{
		if (hardwareId.empty())
			hardwareId = GetCurrentHardwareIdCallback();

		int keyLen = activationKeyTemplate.GetNumberOfGroups() * activationKeyTemplate.GetCharactersPerGroup() 
					 + (activationKeyTemplate.GetNumberOfGroups() - 1) * _tcslen(activationKeyTemplate.GetGroupSeparator());

		int hwidLen = hardwareId.length();

		activationKeyTemplate.SetDataSize(activationKeyTemplate.GetCharactersPerGroup() * activationKeyTemplate.GetNumberOfGroups() * activationKeyTemplate.GetEncoding() - activationKeyTemplate.GetSignatureSize());
		activationKeyTemplate.SetValidationDataSize((keyLen + hwidLen + licenseKeyValidationDataLen) * 8);

		activationKeyTemplate.AddDataField(_T("ExpirationDate"), FIELD_TYPE_RAW, 16, 0);
		activationKeyTemplate.AddValidationField(_T("LicenseKey"), FIELD_TYPE_STRING, keyLen * 8, 0);
		activationKeyTemplate.AddValidationField(_T("HardwareId"), FIELD_TYPE_STRING, hwidLen * 8, keyLen * 8);

		if (licenseKeyValidationData != NULL)
		{
			activationKeyTemplate.AddValidationField(_T("LicenseKeyValidationData"), FIELD_TYPE_RAW, licenseKeyValidationDataLen * 8, (keyLen + hwidLen) * 8);
		}

		activationKeyTemplate.SetSigningServiceUrl(licensingServiceUrl.c_str());

		if (productId != -1)
		{
			TCHAR params[64] = _T("ProductId=");
			_itot(productId, params + sizeof("ProductId"), 10);
			activationKeyTemplate.SetSigningServiceParameters(params);
		}

		KeyGeneratorImpl generator(&activationKeyTemplate);

		generator.SetKeyData(_T("ExpirationDate"), (int)0);
		generator.SetValidationData(_T("LicenseKey"), licenseKey.c_str());
		generator.SetValidationData(_T("HardwareId"), hardwareId.c_str());

		if (licenseKeyValidationData != NULL)
			generator.SetValidationData(_T("LicenseKeyValidationData"), licenseKeyValidationData, licenseKeyValidationDataLen * 8);

		activationKey = generator.GenerateKey();
	}

	void SetLicensingServiceUrl(const char_t * url)
	{
		licensingServiceUrl = url;
	}

	int GetLicenseActivationStatus()
	{
		return activationStatus;
	}

	void GetLicenseExpirationDate(int * year, int * month, int * day)
	{
		if (licenseExpirationTime.tm_year > 0)
		{
			*year = 1900 + licenseExpirationTime.tm_year;
			*month = 1 + licenseExpirationTime.tm_mon;
			*day = licenseExpirationTime.tm_mday;
		} else
		{
			*year = 0;
			*month = 0;
			*day = 0;
		}
	}

	const char_t * GetCurrentHardwareIdCallback()
	{
#ifdef _UNICODE
		if (parentUsesWideChar) 
			return ((LicensingClientT<wchar_t> *)parentLicensingClient)->GetCurrentHardwareId();
		else
		{
			USES_CONVERSION;
			static std::wstring hwid = A2W(((LicensingClientT<char> *)parentLicensingClient)->GetCurrentHardwareId());
			return hwid.c_str();
		}
#else
		if (parentUsesWideChar) 
		{
			USES_CONVERSION;
			static std::string hwid = W2A(((LicensingClientT<wchar_t> *)parentLicensingClient)->GetCurrentHardwareId());
			return hwid.c_str();
		}
		else
		{
			return ((LicensingClientT<char> *)parentLicensingClient)->GetCurrentHardwareId();
		}
#endif
	}

	const char_t * GetCurrentHardwareId()
	{
		return KeyHelper::GetCurrentHardwareId();
	}

	bool MatchCurrentHardwareIdCallback(const char_t * hwid)
	{
#ifdef _UNICODE
		if (parentUsesWideChar) 
			return ((LicensingClientT<wchar_t> *)parentLicensingClient)->MatchCurrentHardwareId(hwid);
		else
		{
			USES_CONVERSION;
			return ((LicensingClientT<char> *)parentLicensingClient)->MatchCurrentHardwareId(W2A(hwid));
		}
#else
		if (parentUsesWideChar) 
		{
			USES_CONVERSION;
			return ((LicensingClientT<wchar_t> *)parentLicensingClient)->MatchCurrentHardwareId(A2W(hwid));
		}
		else
			return ((LicensingClientT<char> *)parentLicensingClient)->MatchCurrentHardwareId(hwid);
#endif
	}

	bool MatchCurrentHardwareId(const char_t * hwid)
	{
		return KeyHelper::MatchCurrentHardwareId(hwid);
	}

	void SetTimeValidationMethod(int method)
	{
		timeValidationMethod = method;
	}

private:
	struct tm licenseExpirationTime;
	string_t licenseKey;
	string_t activationKey;
	string_t hardwareId;
	string_t licensingServiceUrl;
	KeyTemplateImpl activationKeyTemplate;
	void * licenseKeyValidationData;
	unsigned licenseKeyValidationDataLen;
	int activationStatus;
	int productId;
	void * parentLicensingClient;
	bool parentUsesWideChar;
	int timeValidationMethod;
};

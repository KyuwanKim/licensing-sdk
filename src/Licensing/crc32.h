#pragma once

class Crc32
{
public:
	static unsigned long Compute(const unsigned char * buffer, int count);

private:
	static const unsigned long crcTable[256];
};

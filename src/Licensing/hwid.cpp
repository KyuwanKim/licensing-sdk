//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

#include "precomp.h"

#include <list>
#include <string>

#include "wmihelper.h"
#include "bitStream2.h"
#include "base32.h"
#include "crc32.h"

#include "hwid.h"

string_t HardwareId::hardwareId;

unsigned short HardwareId::HashString(const char * str)
{
	unsigned long crc = Crc32::Compute((const unsigned char *)str, strlen(str));

	return (unsigned short)(crc >> 16);
}

unsigned short HardwareId::HashInt(int val)
{
	unsigned char buf[4] = {(unsigned char)(val >> 24), (unsigned char)(val >> 16), (unsigned char)(val >> 8), val & 0xFF};
	unsigned long crc = Crc32::Compute(buf, 4);

	return (unsigned short)(crc >> 16);
}

const char_t * HardwareId::GetCurrentHardwareId()
{
	WmiHelper wmi;
	BitStream2 bits(125);
	list<string> propList;
	unsigned char zero = 0;
	unsigned short hash1, hash2;

	bits.Write(&zero, 3);

	propList.clear();
	wmi.GetPropertyList("SELECT Capacity FROM Win32_PhysicalMemory", "Capacity", &propList);

	unsigned long long memSize = 0;

	for (list<string>::iterator iter = propList.begin(); iter != propList.end(); iter++)
	{
		memSize += _atoi64(iter->c_str());
	}

	memSize = (memSize + 1023) / (1024 * 1024);

	hash1 = ((int)propList.size() > 0) ? HashInt(memSize) : HashInt(0);

	bits.WriteUInt16(hash1);

	propList.clear();
	wmi.GetPropertyList("SELECT ProcessorId FROM Win32_Processor", "ProcessorId", &propList, 2);

	hash1 = (propList.size() > 0) ? HashString(propList.front().c_str()) : HashInt(0);
	hash2 = (propList.size() > 1) ? HashString(propList.back().c_str()) : hash1+256;

	bits.WriteUInt16(hash1);
	bits.WriteUInt16(hash2);

	propList.clear();
	wmi.GetPropertyList("SELECT MACAddress FROM Win32_NetworkAdapter WHERE (AdapterType = \"Ethernet 802.3\") AND (MACAddress IS NOT NULL) AND PNPDeviceId LIKE \"%PCI%\"", "MACAddress", &propList, 1);

	hash1 = (propList.size() > 0) ? HashString(propList.front().c_str()) : HashInt(0);
	hash2 = (propList.size() > 1) ? HashString(propList.back().c_str()) : hash1+256;

	bits.WriteUInt16(hash1);
	bits.WriteUInt16(hash2);

	propList.clear();
	//wmi.GetPropertyList("SELECT SerialNumber FROM Win32_PhysicalMedia WHERE SerialNumber IS NOT NULL", "SerialNumber", &propList, 2);
	//wmi.GetPropertyList("SELECT SerialNumber FROM Win32_DiskDrive WHERE SerialNumber IS NOT NULL", "SerialNumber", &propList, 1);
	wmi.GetPropertyList("SELECT SerialNumber FROM Win32_DiskDrive WHERE Index = 0", "SerialNumber", &propList, 1);
	if (propList.size() > 0)
	{
		string s = propList.front();
		s.erase(0, s.find_first_not_of(" \t\n"));
		s.erase(s.find_last_not_of(" \t\n") + 1);

		hash1 = HashString(s.c_str());
	} else
		hash1 = HashInt(0);

	if (propList.size() > 1)
	{
		string s = propList.back();
		s.erase(0, s.find_first_not_of(" \t\n"));
		s.erase(s.find_last_not_of(" \t\n") + 1);

		hash2 = HashString(s.c_str());
	} else
		hash2 = hash1+256;

	bits.WriteUInt16(hash1);
	bits.WriteUInt16(hash2);

	unsigned char * rawHwid = bits.GetBuffer();

	BASE32 base32;

	char * encHwid = base32.encode(rawHwid, 16);

	USES_CONVERSION;

	hardwareId = A2T(encHwid);

	free(encHwid);

	hardwareId.erase(hardwareId.length() - 1);

	unsigned i;
	unsigned insertPos;
	// separate character groups
	for (i = 0, insertPos = 5; i < 4; i++)
	{
		hardwareId.insert(insertPos, _T("-"));
		insertPos += 6;
	}

	return hardwareId.c_str();
}

bool HardwareId::MatchCurrentHardwareId(const char_t * hwid)
{
	WmiHelper wmi;
	USES_CONVERSION;

	string hardwareId(T2A((char_t *)hwid));

	for (int i = 0, erasePos = 0; i < 4; i++)
	{
		erasePos += 5;
		hardwareId.erase(erasePos, 1);
	}

	BASE32 base32;
	int bufLen;

	int padLen;
	int len = base32.encode_pad_length(((int)hardwareId.length() * 5 + 7) >> 3, &padLen);

	if (len > (int)hardwareId.length())
		hardwareId.append(len - hardwareId.length(), 'A');

	if (padLen)
		hardwareId.append(padLen,'=');

	unsigned char * buf = base32.decode(hardwareId.c_str(), hardwareId.length(), &bufLen);

	BitStream2 bits;
	bits.Attach(buf);
	unsigned char version;

	bits.Read(&version, 3);

	if ((version & 0xE0) != 0)
		throw new exception("invalid hardware id version");

	unsigned short hash1, hash2;

	bits.ReadUInt16(&hash1);

	list<string> propList;

	unsigned long long memSize = 0;

	wmi.GetPropertyList("SELECT Capacity FROM Win32_PhysicalMemory", "Capacity", &propList);

	for (list<string>::iterator iter = propList.begin(); iter != propList.end(); iter++)
	{
		memSize += _atoi64(iter->c_str());
	}

	memSize = memSize / (1024 * 1024);

	if (hash1 != HashInt((int)memSize))
		return false;

	bits.ReadUInt16(&hash1);
	bits.ReadUInt16(&hash2);

	propList.clear();
	wmi.GetPropertyList("SELECT ProcessorId FROM Win32_Processor", "ProcessorId", &propList, 2);

	if (propList.size() > 0)
		if (HashString(propList.front().c_str()) != hash1 && (HashString(propList.front().c_str()) != hash2))
			return false;

	if (propList.size() > 1)
		if (HashString(propList.back().c_str()) != hash1 && (HashString(propList.back().c_str()) != hash2))
			return false;

	bits.ReadUInt16(&hash1);
	bits.ReadUInt16(&hash2);

	propList.clear();
	wmi.GetPropertyList("SELECT MACAddress FROM Win32_NetworkAdapter WHERE (AdapterType = \"Ethernet 802.3\") AND (MACAddress IS NOT NULL) AND PNPDeviceId LIKE \"%PCI%\"", "MACAddress", &propList, 1);

	if (propList.size() > 0)
		if (HashString(propList.front().c_str()) == hash1 || (HashString(propList.front().c_str()) == hash2))
			return true;

	if (propList.size() > 1)
		if (HashString(propList.back().c_str()) == hash1 || (HashString(propList.back().c_str()) == hash2))
			return true;

	bits.ReadUInt16(&hash1);
	bits.ReadUInt16(&hash2);

	propList.clear();
	//wmi.GetPropertyList("SELECT SerialNumber FROM Win32_DiskDrive WHERE SerialNumber IS NOT NULL and DeviceID LIKE '%DRIVE0'", "SerialNumber", &propList, 1);
	wmi.GetPropertyList("SELECT SerialNumber FROM Win32_DiskDrive WHERE Index = 0", "SerialNumber", &propList, 1);

	if (propList.size() > 0)
		if (HashString(propList.front().c_str()) == hash1 || (HashString(propList.front().c_str()) == hash2))
			return true;

	if (propList.size() > 1)
		if (HashString(propList.back().c_str()) == hash1 || (HashString(propList.back().c_str()) == hash2))
			return true;

	return true;
}



/* OLD 
#include "precomp.h"

#include <list>
#include <string>

#include "wmihelper.h"
#include "bitStream2.h"
#include "base32.h"
#include "crc32.h"

#include "hwid.h"

string_t HardwareId::hardwareId;

unsigned short HardwareId::HashString(const char * str)
{
	unsigned long crc = Crc32::Compute((const unsigned char *)str, strlen(str));

	return (unsigned short)(crc >> 16);
}

unsigned short HardwareId::HashInt(int val)
{
	unsigned char buf[4] = {(unsigned char)(val >> 24), (unsigned char)(val >> 16), (unsigned char)(val >> 8), val & 0xFF};
	unsigned long crc = Crc32::Compute(buf, 4);

	return (unsigned short)(crc >> 16);
}

const char_t * HardwareId::GetCurrentHardwareId()
{
	WmiHelper wmi;
	BitStream2 bits(125);
	list<string> propList;
	unsigned char zero = 0;
	unsigned short hash1, hash2;
	
	bits.Write(&zero, 3);

	propList.clear();
	wmi.GetPropertyList("SELECT Capacity FROM Win32_PhysicalMemory", "Capacity", &propList);

	unsigned long long memSize = 0;

	for (list<string>::iterator iter = propList.begin(); iter != propList.end(); iter++)
	{
		memSize += _atoi64(iter->c_str());
	}

	memSize = (memSize + 1023) / (1024 * 1024);

	hash1 = ((int)propList.size() > 0) ? HashInt(memSize) : HashInt(0);

	bits.WriteUInt16(hash1);

	propList.clear();
	wmi.GetPropertyList("SELECT ProcessorId FROM Win32_Processor", "ProcessorId", &propList, 2);

	hash1 = (propList.size() > 0) ? HashString(propList.front().c_str()) : HashInt(0);
	hash2 = (propList.size() > 1) ? HashString(propList.back().c_str()) : hash1+256;
bits.WriteUInt16(hash1);
	bits.WriteUInt16(hash2);

	propList.clear();
	wmi.GetPropertyList("SELECT MACAddress FROM Win32_NetworkAdapter WHERE (AdapterType = \"Ethernet 802.3\") AND (MACAddress IS NOT NULL) AND PNPDeviceId LIKE \"%PCI%\"", "MACAddress", &propList, 1);

	hash1 = (propList.size() > 0) ? HashString(propList.front().c_str()) : HashInt(0);
	hash2 = (propList.size() > 1) ? HashString(propList.back().c_str()) : hash1+256;

	bits.WriteUInt16(hash1);
	bits.WriteUInt16(hash2);

	propList.clear();
	//wmi.GetPropertyList("SELECT SerialNumber FROM Win32_PhysicalMedia WHERE SerialNumber IS NOT NULL", "SerialNumber", &propList, 1);
	wmi.GetPropertyList("SELECT SerialNumber FROM Win32_DiskDrive WHERE SerialNumber IS NOT NULL", "SerialNumber", &propList, 1);

	if (propList.size() > 0)
	{
		string s = propList.front();
		s.erase(0, s.find_first_not_of(" \t\n"));
		s.erase(s.find_last_not_of(" \t\n") + 1);
		
		hash1 = HashString(s.c_str());
	} else
		hash1 = HashInt(0);

	if (propList.size() > 1)
	{
		string s = propList.back();
		s.erase(0, s.find_first_not_of(" \t\n"));
		s.erase(s.find_last_not_of(" \t\n") + 1);
		
		hash2 = HashString(s.c_str());
	} else
		hash2 = hash1+256;

	bits.WriteUInt16(hash1);
	bits.WriteUInt16(hash2);
	propList.clear();

	unsigned char * rawHwid = bits.GetBuffer();

	BASE32 base32;

	char * encHwid = base32.encode(rawHwid, 16);

	USES_CONVERSION;

	hardwareId = A2T(encHwid);

	free(encHwid);

	hardwareId.erase(hardwareId.length() - 1);

	unsigned i;
	unsigned insertPos;
	// separate character groups
	for (i = 0, insertPos = 5; i < 4; i++)
	{
		hardwareId.insert(insertPos, _T("-"));
		insertPos += 6;
	}

	return hardwareId.c_str();
}

bool HardwareId::MatchCurrentHardwareId(const char_t * hwid)
{
	WmiHelper wmi;
	USES_CONVERSION;

	string hardwareId(T2A((char_t *)hwid));

	for (int i = 0, erasePos = 0; i < 4; i++)
	{
		erasePos += 5;
		hardwareId.erase(erasePos, 1);
	}

	BASE32 base32;
	int bufLen;

	int padLen;
	int len = base32.encode_pad_length(((int)hardwareId.length() * 5 + 7) >> 3, &padLen);

	if (len > (int)hardwareId.length())
		hardwareId.append(len - hardwareId.length(), 'A');

	if (padLen)
		hardwareId.append(padLen,'=');
	
	unsigned char * buf = base32.decode(hardwareId.c_str(), hardwareId.length(), &bufLen);
	
	BitStream2 bits;
	bits.Attach(buf);
	unsigned char version;

	bits.Read(&version, 3);

	if ((version & 0xE0) != 0)
		throw new exception("invalid hardware id version");

    unsigned short hash1, hash2;

    bits.ReadUInt16(&hash1);

	list<string> propList;

    unsigned long long memSize = 0;

    wmi.GetPropertyList("SELECT Capacity FROM Win32_PhysicalMemory", "Capacity", &propList);

    for (list<string>::iterator iter = propList.begin(); iter != propList.end(); iter++)
    {
        memSize += _atoi64(iter->c_str());
    }

    memSize = memSize / (1024 * 1024);

    if (hash1 != HashInt((int)memSize))
        return false;

    bits.ReadUInt16(&hash1);
    bits.ReadUInt16(&hash2);

	propList.clear();
    wmi.GetPropertyList("SELECT ProcessorId FROM Win32_Processor", "ProcessorId", &propList, 2);

    if (propList.size() > 0)
        if (HashString(propList.front().c_str()) != hash1 && (HashString(propList.front().c_str()) != hash2))
            return false;

    if (propList.size() > 1)
        if (HashString(propList.back().c_str()) != hash1 && (HashString(propList.back().c_str()) != hash2))
            return false;

    bits.ReadUInt16(&hash1);
    bits.ReadUInt16(&hash2);

	propList.clear();
    wmi.GetPropertyList("SELECT MACAddress FROM Win32_NetworkAdapter WHERE (AdapterType = \"Ethernet 802.3\") AND (MACAddress IS NOT NULL) AND PNPDeviceId LIKE \"%PCI%\"", "MACAddress", &propList, 1);

    if (propList.size() > 0)
        if (HashString(propList.front().c_str()) == hash1 || (HashString(propList.front().c_str()) == hash2))
            return true;

    if (propList.size() > 1)
        if (HashString(propList.back().c_str()) == hash1 || (HashString(propList.back().c_str()) == hash2))
            return true;

    bits.ReadUInt16(&hash1);
    bits.ReadUInt16(&hash2);

	propList.clear();
    wmi.GetPropertyList("SELECT SerialNumber FROM Win32_DiskDrive WHERE SerialNumber IS NOT NULL", "SerialNumber", &propList, 1);

    if (propList.size() > 0)
        if (HashString(propList.front().c_str()) == hash1 || (HashString(propList.front().c_str()) == hash2))
            return true;

    if (propList.size() > 1)
        if (HashString(propList.back().c_str()) == hash1 || (HashString(propList.back().c_str()) == hash2))
            return true;

    return false;
}
*/
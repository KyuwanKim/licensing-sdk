#include "precomp.h"
#include <windows.h>
#include <wincrypt.h>
#include "bigint.h"
#include "rand.h"

random::random()
{
   	if ( !CryptAcquireContext( (HCRYPTPROV *)&m_hCryptProv, NULL, MS_DEF_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT ) )
		m_hCryptProv = NULL;
}

random::~random()
{
	if ( m_hCryptProv )
		CryptReleaseContext( (HCRYPTPROV)m_hCryptProv, 0L );
}

unsigned random::rand( unsigned max )
{
	unsigned rnd;

	if ( m_hCryptProv )
		CryptGenRandom( (HCRYPTPROV)m_hCryptProv, sizeof( unsigned ), (BYTE *)&rnd );

	return rnd % max;
}

bigint random::rand( bigint & max )
{
	unsigned n = ( max.bits() + ( ( sizeof( unsigned ) << 3 ) - 1 ) ) / ( sizeof( unsigned ) << 3 );
	unsigned * buf = new unsigned[ n ];
	if ( !buf ) return max - 1;

	bigint rnd;

	if ( m_hCryptProv )
		CryptGenRandom( (HCRYPTPROV)m_hCryptProv, n * sizeof( unsigned ), (BYTE *)buf );

	rnd.load( buf, n );

	return rnd % max;
}

#ifndef __PRECOMP_H
#define __PRECOMP_H

#define _CRT_SECURE_NO_DEPRECATE

#include <malloc.h>
#include <stddef.h>
#include <string.h>                     /* memset(), memcpy() */
#include <assert.h>
#include <string>

#ifdef _UNICODE
typedef std::wstring string_t;
#define t_stricmp _wcsicmp
#define t_isalnum iswalnum
#define t_strlen wcslen
#define t_atoi _wtoi
#define t_strncpy wcsncpy
#define T_STR(s) L##s
#define t_snprintf _snwprintf
#else
typedef std::string string_t;
#define t_stricmp _stricmp
#define t_isalnum isalnum
#define t_strlen strlen
#define t_atoi atoi
#define t_strncpy strncpy
#define T_STR(s) s
#define t_snprintf _snprintf
#endif

#define _CRTDBG_MAP_ALLOC 
#include <crtdbg.h>

#define _WIN32_WINNT 0x0501

#include <WinSock2.h>

#ifdef WIN32
#include <windows.h>
#endif

#include <tchar.h>
#include "licensing.h"

#endif

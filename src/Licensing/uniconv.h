#ifndef __UNICONV_H
#define __UNICONV_H

#include <windows.h>
#include <assert.h>

inline LPWSTR WINAPI A2WHelper(LPWSTR lpw, LPCSTR lpa, int nChars, UINT acp) throw()
{
	assert(lpa != NULL);
	assert(lpw != NULL);

	if (lpw == NULL || lpa == NULL)
		return NULL;

	// verify that no illegal character present
	// since lpw was allocated based on the size of lpa
	// don't worry about the number of chars
	lpw[0] = '\0';
	int ret = MultiByteToWideChar(acp, 0, lpa, -1, lpw, nChars);
	if(ret == 0)
	{
		assert(0);
		return NULL;
	}		
	return lpw;
}

inline LPSTR WINAPI W2AHelper(LPSTR lpa, LPCWSTR lpw, int nChars, UINT acp) throw()
{
	assert(lpw != NULL);
	assert(lpa != NULL);

	if (lpa == NULL || lpw == NULL)
		return NULL;

	// verify that no illegal character present
	// since lpa was allocated based on the size of lpw
	// don't worry about the number of chars
	lpa[0] = '\0';
	int ret = WideCharToMultiByte(acp, 0, lpw, -1, lpa, nChars, NULL, NULL);
	if(ret == 0)
	{
		assert(0);
		return NULL;
	}
	return lpa;
}

#ifdef NDEBUG
	#define USES_CONVERSION int _convert; (_convert); UINT _acp = CP_ACP /*CP_THREAD_ACP*/; (_acp); LPCWSTR _lpw; (_lpw); LPCSTR _lpa; (_lpa)
#else
	#define USES_CONVERSION int _convert = 0; (_convert); UINT _acp = CP_ACP /*CP_THREAD_ACP*/; (_acp); LPCWSTR _lpw = NULL; (_lpw); LPCSTR _lpa = NULL; (_lpa)
#endif

#define W2A(lpw) (\
	((_lpw = lpw) == NULL) ? NULL : (\
		_convert = (lstrlenW(_lpw)+1)*2,\
		W2AHelper((LPSTR) alloca(_convert), _lpw, _convert, _acp)))

#define W2A_2(lpw) (\
		(lpw) == NULL) ? NULL : W2AHelper((LPSTR) alloca((lstrlenW(lpw)+1)*2), lpw, (lstrlenW(lpw)+1)*2, CP_ACP))

#define A2W(lpa) (\
	((_lpa = lpa) == NULL) ? NULL : (\
		_convert = (lstrlenA(_lpa)+1),\
		A2WHelper((LPWSTR) alloca(_convert*2), _lpa, _convert, _acp)))

#define UTF82W(lpa) (\
	((_lpa = lpa) == NULL) ? NULL : (\
		_convert = (lstrlenA(_lpa)+1),\
		A2WHelper((LPWSTR) alloca(_convert*2), _lpa, _convert, CP_UTF8)))

#define W2UTF8(lpw) (\
	((_lpw = lpw) == NULL) ? NULL : (\
		_convert = (lstrlenW(_lpw)+1)*4,\
		W2AHelper((LPSTR) alloca(_convert), _lpw, _convert, CP_UTF8)))

#ifdef _UNICODE
	#define A2T A2W
	#define UTF82T UTF82W
	#define T2A W2A
	#define T2UTF8 W2UTF8
	#define W2T(lpt) lpt
	#define W2T_2(lpt) lpt
	#define T2W(lpt) lpt
#else
	#define A2T(lpt) lpt
	#define UTF82T(lpt) lpt /* NOT OK */
	#define T2A(lpt) lpt
	#define T2UTF8(lpt) lpt
	#define W2T W2A
	#define W2T_2 W2A_2
    #define T2W A2W
#endif

#endif

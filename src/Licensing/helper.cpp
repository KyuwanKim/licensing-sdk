//
// Copyright (c) 2011 Catalin Stavaru. All rights reserved.
//

#include "precomp.h"
#include "licensing.h"
#include "uniconv.h"
#include "except.h"
#include "hwid.h"

class KeyHelperImpl {
friend class KeyHelperT<char>;
friend class KeyHelperT<wchar_t>;
private:
	static const char_t * GetCurrentHardwareId()
	{
		return HardwareId::GetCurrentHardwareId();
	}

	static bool MatchCurrentHardwareId(const char_t * hwid)
	{
		return HardwareId::MatchCurrentHardwareId(hwid);
	}

	static bool DetectClockManipulation(int year, int month, int day)
	{
		FILETIME thresholdTime;
		SYSTEMTIME st;
		TCHAR path[MAX_PATH];

		ZeroMemory(&st, sizeof(st));

		st.wYear = year;
		st.wMonth = month;
		st.wDay = day;
		st.wHour = 23;
		st.wMinute = 59;
		st.wSecond = 59;

		if (!SystemTimeToFileTime(&st, &thresholdTime))
			return false;

		if (GetTempPath(MAX_PATH, path) > 0)
		{
			_tcsncat_s<MAX_PATH>(path, _T("*.*"), _TRUNCATE);

			WIN32_FIND_DATA findData;
			
			HANDLE findHandle = FindFirstFile(path, &findData);
			if (findHandle != INVALID_HANDLE_VALUE)
			{
				while (FindNextFile(findHandle, &findData))
				{
					SYSTEMTIME st1, st2;

					FileTimeToSystemTime(&findData.ftLastWriteTime, &st1);
					FileTimeToSystemTime(&thresholdTime, &st2);

					if (CompareFileTime(&findData.ftLastWriteTime, &thresholdTime) > 0)
						return true;
				}

				FindClose(findHandle);
			}
		}

		DWORD bytesRead;
		DWORD bytesNeeded;
		FILETIME timeGenerated;
		unsigned char eventLogRecords[4096];
		
		HANDLE eventLog = OpenEventLog(NULL, _T("System"));
		if (eventLog != NULL)
		{
			while (ReadEventLog(eventLog, EVENTLOG_SEQUENTIAL_READ | EVENTLOG_BACKWARDS_READ, 0L, eventLogRecords, sizeof(eventLogRecords), &bytesRead, &bytesNeeded))
			{
				PBYTE record = eventLogRecords;
				GetTimestamp(((PEVENTLOGRECORD)record)->TimeGenerated, &timeGenerated);

				if (CompareFileTime(&timeGenerated, &thresholdTime) > 0)
				{
					CloseEventLog(eventLog);
					return true;
				}

				record += ((PEVENTLOGRECORD)record)->Length;
			}

			CloseEventLog(eventLog);
		}

		return false;
	}

	static void GetTimestamp(const DWORD t, LPFILETIME ft)
	{
		ULONGLONG ullTimeStamp = 0;
		ULONGLONG SecsTo1970 = 116444736000000000;

		ullTimeStamp = Int32x32To64(t, 10000000) + SecsTo1970;
		ft->dwHighDateTime = (DWORD)((ullTimeStamp >> 32) & 0xFFFFFFFF);
		ft->dwLowDateTime = (DWORD)(ullTimeStamp & 0xFFFFFFFF);
	}
};

template<>
const char * KeyHelperT<char>::GetCurrentHardwareId()
{
#ifdef _UNICODE
	USES_CONVERSION;
	static std::string hwid = W2A(KeyHelperImpl::GetCurrentHardwareId());
	return hwid.c_str();
#else
	return KeyHelperImpl::GetCurrentHardwareId();
#endif
}

template<>
const wchar_t * KeyHelperT<wchar_t>::GetCurrentHardwareId()
{
#ifdef _UNICODE
	return KeyHelperImpl::GetCurrentHardwareId();
#else
	USES_CONVERSION;
	static std::wstring hwid = A2W(KeyHelperImpl::GetCurrentHardwareId());
	return hwid.c_str();
#endif
}

template<>
bool KeyHelperT<char>::MatchCurrentHardwareId(const char * hwid)
{
#ifdef _UNICODE
	USES_CONVERSION;
	return KeyHelperImpl::MatchCurrentHardwareId(A2W(hwid));
#else
	return KeyHelperImpl::MatchCurrentHardwareId(hwid);
#endif
}

template<>
bool KeyHelperT<wchar_t>::MatchCurrentHardwareId(const wchar_t * hwid)
{
#ifdef _UNICODE
	return KeyHelperImpl::MatchCurrentHardwareId(hwid);
#else
	USES_CONVERSION;
	return KeyHelperImpl::MatchCurrentHardwareId(W2A(hwid));
#endif
}

template<>
bool KeyHelperT<char>::DetectClockManipulation(int thresholdYear, int thresholdMonth, int thresholdDay)
{
	return KeyHelperImpl::DetectClockManipulation(thresholdYear, thresholdMonth, thresholdDay);
}

template<>
bool KeyHelperT<wchar_t>::DetectClockManipulation(int thresholdYear, int thresholdMonth, int thresholdDay)
{
	return KeyHelperImpl::DetectClockManipulation(thresholdYear, thresholdMonth, thresholdDay);
}

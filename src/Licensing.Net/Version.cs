namespace SoftActivate.Licensing
{
    public class Version
    {
        public const int Major = 3;
        public const int Minor = 1;
        public const int Build = 15;
        public const int BuildTimestamp = 1399159291;
    }
}

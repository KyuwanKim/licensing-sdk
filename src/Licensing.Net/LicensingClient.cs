﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Web;
using System.Net;
using System.IO;

namespace SoftActivate.Licensing
{
    public enum LICENSE_STATUS
    {
        Valid = 0,
        InvalidHardwareId = 12,
        Expired = 13,
        InvalidActivationKey = 14
    }

    public enum TIME_VALIDATION_METHOD
    {
        PreferInternetTime = 0,
        UseInternetTime,
        UseLocalTime
    }

    public delegate void LicensingCompleteEventHandler();
    public delegate void LicensingExceptionEventHandler(Exception ex);

    public class LicensingClient
    {
        public event LicensingCompleteEventHandler OnLicensingComplete;
        public event LicensingExceptionEventHandler OnLicensingException;

        public LicensingClient(string licensingServiceUrl, LicenseTemplate licenseKeyTemplate, string licenseKey, byte[] licenseKeyValidationData, string hardwareId, int productId)
        {
            activationKeyTemplate = new LicenseTemplate();

            activationKeyTemplate.CharactersPerGroup = licenseKeyTemplate.CharactersPerGroup;
            activationKeyTemplate.NumberOfGroups = licenseKeyTemplate.NumberOfGroups;
            activationKeyTemplate.Encoding = licenseKeyTemplate.Encoding;
            activationKeyTemplate.DataSize = licenseKeyTemplate.DataSize;
            activationKeyTemplate.SignatureSize = licenseKeyTemplate.SignatureSize;
            activationKeyTemplate.SetPublicKeyCertificate(licenseKeyTemplate.GetPublicKeyCertificate());

            this.licenseKey = licenseKey;
            this.licenseKeyValidationData = licenseKeyValidationData;
            this.productId = productId;
            this.hardwareId = hardwareId;

            int keyLen = activationKeyTemplate.NumberOfGroups * activationKeyTemplate.CharactersPerGroup +
                    (activationKeyTemplate.NumberOfGroups - 1) * activationKeyTemplate.GroupSeparator.Length;

            int hwidLen = 29;

            activationKeyTemplate.DataSize = activationKeyTemplate.NumberOfGroups * activationKeyTemplate.CharactersPerGroup * (int)activationKeyTemplate.Encoding - activationKeyTemplate.SignatureSize;
            activationKeyTemplate.AddDataField("ExpirationDate", LicenseKeyFieldType.Integer, 16);

            activationKeyTemplate.ValidationDataSize = (keyLen + hwidLen) * 8 + ((licenseKeyValidationData == null) ? 0 : (licenseKeyValidationData.Length * 8));

            activationKeyTemplate.AddValidationField("LicenseKey", LicenseKeyFieldType.String, keyLen * 8, 0);
            activationKeyTemplate.AddValidationField("HardwareId", LicenseKeyFieldType.String, hwidLen * 8, keyLen * 8);

            if (licenseKeyValidationData != null)
                activationKeyTemplate.AddValidationField("LicenseKeyValidationData", LicenseKeyFieldType.Raw, licenseKeyValidationData.Length * 8, (hwidLen + keyLen) * 8);

            if (!String.IsNullOrEmpty(licensingServiceUrl))
                activationKeyTemplate.SetSigningServiceUrl(licensingServiceUrl);

            if (productId != -1)
            {
                //activationKeyTemplate.SetSigningServiceParameters("ProductId=" + productId.ToString());
                activationKeyTemplate.SigningServiceTemplateId = productId;
            }
        }

        public LicensingClient(string licensingServiceUrl, LicenseTemplate licenseKeyTemplate, string licenseKey, byte[] licenseKeyValidationData, int productId):
            this(licensingServiceUrl, licenseKeyTemplate, licenseKey, licenseKeyValidationData, null, productId)
        {

        }

        public LicensingClient(string licensingServiceUrl, LicenseTemplate licenseKeyTemplate, string licenseKey, byte[] licenseKeyValidationData) :
            this(licensingServiceUrl, licenseKeyTemplate, licenseKey, licenseKeyValidationData, null, -1)
        {

        }

        public LicensingClient(LicenseTemplate licenseKeyTemplate, string licenseKey, byte[] licenseKeyValidationData, string hardwareId, string activationKey) :
            this(null, licenseKeyTemplate, licenseKey, licenseKeyValidationData, null, -1)
        {
            this.hardwareId = hardwareId;
            this.activationKey = activationKey;
        }

        public void BeginAcquireLicense()
        {
            Thread licensingThread = new Thread(new ParameterizedThreadStart(LicensingThread));
            licensingThread.IsBackground = true;
            licensingThread.Start(null);
        }

        public void AcquireLicense()
        {
            KeyGenerator generator = new KeyGenerator(activationKeyTemplate);

            if (!string.IsNullOrEmpty(licenseKey))
            {
                generator.SetValidationData("LicenseKey", licenseKey);
            }

            if (String.IsNullOrEmpty(hardwareId))
                hardwareId = GetCurrentHardwareId();

            generator.SetValidationData("HardwareId", hardwareId);

            if (licenseKeyValidationData != null)
                generator.SetValidationData("LicenseKeyValidationData", licenseKeyValidationData);

            activationKey = generator.GenerateKey();
        }

        private void LicensingThread(object param)
        {
            try
            {
                AcquireLicense();
            }
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    HttpWebResponse httpResponse = ex.Response as HttpWebResponse;

                    if (httpResponse != null)
                    {
                        OnLicensingException(new Exception(ex.Message + " " + httpResponse.StatusDescription));
                        return;
                    }
                }

                OnLicensingException(ex);
                return;
            }
            catch (Exception ex)
            {
                OnLicensingException(ex);
                return;
            }

            OnLicensingComplete();
        }

        public bool IsLicenseValid()
        {
            if (string.IsNullOrEmpty(hardwareId))
            {
                activationStatus = LICENSE_STATUS.InvalidHardwareId;
                return false;
            }

            if (!MatchCurrentHardwareId(hardwareId))
            {
                activationStatus = LICENSE_STATUS.InvalidHardwareId;
                return false;
            }

	        KeyValidator validator = new KeyValidator(activationKeyTemplate);

            try
            {
                validator.SetKey(activationKey);
            }
            catch (Exception)
            {
                // activation key is invalid - need activation
                activationStatus = LICENSE_STATUS.InvalidActivationKey;
                return false;
            }

            if (licenseKey != null)
	            validator.SetValidationData("LicenseKey", licenseKey);

            if (hardwareId != null)
	            validator.SetValidationData("HardwareId", hardwareId);

            if (licenseKeyValidationData != null)
                validator.SetValidationData("LicenseKeyValidationData", licenseKeyValidationData);

            if (!validator.IsKeyValid())
            {
                activationStatus = LICENSE_STATUS.InvalidActivationKey;
                return false;
            }

            byte[] rawKeyData = validator.QueryKeyData("ExpirationDate");
            ushort keyData = (ushort)(((ushort)rawKeyData[0]) << 8 | rawKeyData[1]);

            if (keyData != 0)
            {
                licenseExpirationDate = new DateTime(2000 + (keyData >> 9), (keyData & 0x01FF) >> 5, keyData & 0x001F);

                DateTime now;
                
                if (timeValidationMethod != TIME_VALIDATION_METHOD.UseLocalTime)
                {
                    try
                    {
                        now = NTPClient.GetCurrentTimeUTC(20000);
                        //System.Diagnostics.Trace.WriteLine("[LicensingSDK] The reported Internet time is " + now.ToString("D"));
                    }
                    catch (Exception)
                    {
                        if (timeValidationMethod == TIME_VALIDATION_METHOD.UseInternetTime)
                        {
                            throw;
                            // activationStatus = LICENSE_STATUS.Expired;
                            // return false;
                        }

                        now = DateTime.UtcNow.ToLocalTime();
                    }
                } else
                    now = DateTime.UtcNow.ToLocalTime();

                if (DateTime.Compare(licenseExpirationDate, now) < 0)
                {
                    activationStatus = LICENSE_STATUS.Expired;
                    return false;
                }
            }
            else
                licenseExpirationDate = DateTime.MinValue;

            return true;
        }

        public virtual string GetCurrentHardwareId()
        {
            return KeyHelper.GetCurrentHardwareId();
        }

        public virtual bool MatchCurrentHardwareId(string hardwareId)
        {
            return KeyHelper.MatchCurrentHardwareId(hardwareId);
        }

        public string ActivationKey
        {
            get
            {
                return activationKey;
            }
        }

        public string HardwareId
        {
            get
            {
                return hardwareId;
            }
        }

        public LICENSE_STATUS LicenseStatus
        {
            get
            {
                return activationStatus;
            }
        }

        public DateTime LicenseExpirationDate
        {
            get
            {
                return licenseExpirationDate;
            }
        }

        public TIME_VALIDATION_METHOD TimeValidationMethod
        {
            get
            {
                return timeValidationMethod;
            }
            set
            {
                timeValidationMethod = value;
            }
        }

        public static DateTime GetCurrentTimeUTC(int timeout)
        {
            return NTPClient.GetCurrentTimeUTC(20000);
        }

        LicenseTemplate activationKeyTemplate;
        int productId = -1;
        string licenseKey;
        string hardwareId;
        string activationKey;
        byte[] licenseKeyValidationData;
        LICENSE_STATUS activationStatus;
        DateTime licenseExpirationDate;
        TIME_VALIDATION_METHOD timeValidationMethod = TIME_VALIDATION_METHOD.PreferInternetTime;
    }
}

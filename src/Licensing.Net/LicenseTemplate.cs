﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace SoftActivate.Licensing
{
    public class LicenseTemplate
    {
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
            }
        }

        public string ProductName
        {
            get
            {
                return productName;
            }
            set
            {
                productName = value;
            }
        }

        public string TrialLicenseKey
        {
            get
            {
                return trialLicenseKey;
            }
            set
            {
                trialLicenseKey = value;
            }
        }

        public string TrialLicenseKeyValidationData
        {
            get
            {
                return trialLicenseKeyValidationData;
            }
            set
            {
                trialLicenseKeyValidationData = value;
            }
        }

        public int TrialDuration
        {
            get
            {
                return trialDuration;
            }
            set
            {
                trialDuration = value;
            }
        }

        public KeyTemplate KeyTemplate
        {
            get
            {
                return keyTemplate;
            }
            set
            {
                keyTemplate = value;
            }
        }

        public string SaveXml()
        {
            TextWriter s = new Utf8StringWriter();
            XmlWriter x = XmlWriter.Create(s, new XmlWriterSettings { Indent = true });

            x.WriteStartDocument();
            x.WriteStartElement("LicenseTemplate");
            x.WriteAttributeString("version", version.ToString());

            x.WriteStartElement("General");
            x.WriteElementString("CompanyName", companyName);
            x.WriteElementString("ProductName", productName);
            x.WriteEndElement();

            x.WriteStartElement("Trial");

            if (!String.IsNullOrEmpty(trialLicenseKey))
                x.WriteElementString("TrialLicenseKey", trialLicenseKey);

            if (!String.IsNullOrEmpty(trialLicenseKeyValidationData))
                x.WriteElementString("TrialLicenseKeyValidationData", trialLicenseKeyValidationData);

            x.WriteElementString("TrialDuration", trialDuration.ToString());
            x.WriteEndElement();

            if (keyTemplate != null)
            {
                XmlDocument xk = new XmlDocument();
                xk.LoadXml(keyTemplate.SaveXml(false));

                x.WriteStartElement("LicenseKeyTemplate");
                x.WriteRaw(((XmlElement)(xk.GetElementsByTagName("LicenseKeyTemplate")[0])).InnerXml);
                x.WriteEndElement();
            }

            x.WriteEndElement();
            x.WriteEndDocument();

            x.Close();

            return s.ToString();
        }

        public void LoadXml(string xmlTemplate)
        {
            XmlDocument x = new XmlDocument();
            x.LoadXml(xmlTemplate);

            companyName = ((XmlElement)x.GetElementsByTagName("CompanyName")[0]).InnerText;
            productName = ((XmlElement)x.GetElementsByTagName("ProductName")[0]).InnerText;

            keyTemplate = new KeyTemplate("<LicenseKeyTemplate>" + ((XmlElement)x.GetElementsByTagName("LicenseKeyTemplate")[0]).InnerXml + "</LicenseKeyTemplate>");
        }

        private string trialLicenseKey;
        private string trialLicenseKeyValidationData;
        private int trialDuration;
        private string productName;
        private string companyName;
        private KeyTemplate keyTemplate;
        private int version = 1;

        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get
                {
                    return Encoding.UTF8;
                }
            }
        }
    }
}

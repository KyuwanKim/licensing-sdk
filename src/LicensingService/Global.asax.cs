﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebMatrix.WebData;
using LicensingService.Filters;

namespace LicensingService
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            Application["GeoIPService"] = new LookupService(Server.MapPath("~/") + "App_Data\\GeoLiteCity.dat");

            Application["TemplateMailer"] = new TemplateMailer(ConfigurationManager.AppSettings["SiteEmail"],
                                                               ConfigurationManager.AppSettings["SmtpServer"],
                                                               Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"]),
                                                               ConfigurationManager.AppSettings["SmtpUsername"],
                                                               ConfigurationManager.AppSettings["SmtpPassword"],
                                                               Boolean.Parse(ConfigurationManager.AppSettings["SmtpSsl"]));

            Application["SDKLicensingAuthorizer"] = new SDKLicensingAuthorizer("DXBUT-DLKR4-KHV6E-N6H5J-25VDD");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = base.Context;
            //LogUrl(context.Request.Url.OriginalString);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            string ip = (HttpContext.Current != null) ? HttpContext.Current.Request.UserHostAddress : null;
            string url = (HttpContext.Current != null) ? HttpContext.Current.Request.Url.AbsoluteUri : null;

            EventLog.Log("Unhandled exception caught", exception.Message + ", " + ((exception.InnerException != null) ? exception.InnerException.Message : ""), EventType.Error, "Global");
            LogUrl("[Unhandled exception caught]:" + url);
        }

        public static string LogPath()
        {
            string strDay = DateTime.UtcNow.ToLocalTime().ToShortDateString();
            string strPath = HttpContext.Current.Server.MapPath("~") + "/Log/" + strDay + ".txt";
            return strPath;
        }

        public static void LogUrl(string str)
        {
            return;
            string strPath = LogPath();
            
            string ip = (HttpContext.Current != null) ? HttpContext.Current.Request.UserHostAddress : null;
            string strLastFix = "";
            if (ip != null)
                strLastFix = "[IP]:" + ip;
            string strPreFix = "[" + DateTime.UtcNow.ToLocalTime().ToString() + "]:";
            StreamWriter sw = File.AppendText(strPath);
            sw.WriteLine(strPreFix + str + strLastFix);
            sw.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Licensing.Web.Models
{
    public class JsonHelper
    {
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public static string JsonSerializer<T>(T obj)
        {
            string json = JsonConvert.SerializeObject(obj);
            return json;
//             DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
//             MemoryStream ms = new MemoryStream();
//             ser.WriteObject(ms, t);
//             string jsonString = Encoding.UTF8.GetString(ms.ToArray());
//             ms.Close();
//             return jsonString;
        }  
        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static T JsonDeserialize<T> (string jsonString)
        {
            T obj = JsonConvert.DeserializeObject<T>(jsonString);
            return obj;
// 데이터가 많아 저서 문제가 발생할 경우 아래 코드 사용
//             MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
//             StreamReader sr = new StreamReader(ms);
//             using (JsonReader reader = new JsonTextReader(sr))
//             {
//                 JsonSerializer js = new JsonSerializer();
//                 T obj = js.Deserialize<T>(reader);
//                 return obj;
//             }
        }
    }
}
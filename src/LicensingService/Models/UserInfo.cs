﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Licensing.Web.Models
{
    public class UserInfo
    {
        public UserInfo()
        {
            Name = "";
            ActivationCnt = 0;
            IsHold = false;
            HoldDate = null;
            HoldCnt = 0;
        }

        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [Required]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime EditDate { get; set; }

        [Required]
        public int LicenseKeyId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int ActivationCnt { get; set; }

        [Required]
        public bool IsHold { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? HoldDate { get; set; }

        [Required]
        public int HoldCnt { get; set; }

        public virtual LicenseKey LicenseKey { get; set; }

    }
}
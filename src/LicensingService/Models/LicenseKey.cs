﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Licensing.Web.Models
{
    public class LicenseKey
    {
        public LicenseKey()
        {
            PerDeviceActivationLimit = 1;
            DeviceCount = 0;
            DeviceLimit = 1;
            ActivationCount = 0;
            ActivationLimit = 1;

            Active = true;
            UpdateActivation = true;
            UpdateDuration = 32;
            CheckUserName = false;
            IsGlobal = false;
            CountryCode = 1;
        }

        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(500, MinimumLength=1, ErrorMessage="License key is either too small or too big. Must have between 1 and 500 characters")]
        public string Key { get; set; }

        [StringLength(1000, ErrorMessage="Validation data is too large. Must have a maximum of 1000 characters")]
        public string ValidationData { get; set; }

        [Required]
        public int ProductId { get; set; }

        public int? OrderItemId { get; set; }
        
        [Range(0, 32767, ErrorMessage="If provided, the device limit must be between 0 and 32767")]
        public short? DeviceLimit { get; set; }

        public int? DeviceCount { get; set; }

        [Range(0, 32767, ErrorMessage="If provided, the activation limit must be between 0 and 32767")]
        public short? ActivationLimit { get; set; }

        public int? ActivationCount { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? LastActivationTime { get; set; }

        [Range(0, 32767, ErrorMessage="If provided, per-device activation limit must be between 0 and 32767")]
        public short? PerDeviceActivationLimit { get; set; }

        //[Required]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? LicenseDuration { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [Range(0, 32767, ErrorMessage="If provided, the license history duration must be less than 32767 days. Use 0 for infinite.")]
        public short? LicenseHistoryDuration { get; set; }

        public DateTime DateAdded { get; set; }

        public bool Active { get; set; }

        public virtual Product Product { get; set; }

        public virtual OrderItem OrderItem { get; set; }

        public int UpdateDuration { get; set; }

        public bool UpdateActivation { get; set; }

        public bool CheckUserName { get; set; }

        //[Required]
        public int UserNameId { get; set; }

        public bool AutoInactivation { get; set; }

        public string Password { get; set; }

        public string ProcessMode { get; set; }

        public bool IsGlobal { get; set; }

        /*
        enum CountryCode
        {
	        NONE = 0,
	        KR = 1,     // 한국
	        US = 2,     // 미국
	        JP = 4,     // 일본
	        CN = 8,     // 중국
	        VN = 16,    // 베트남

	        Default = KR,
	        ALL = KR | US | JP | CN | VN,
        };
        */

        public int CountryCode { get; set; }
        //public virtual UserName UserName { get; set; }
    }
}

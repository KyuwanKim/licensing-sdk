﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Licensing.Web.Models
{
    public class InActivation
    {
        public InActivation()
        {
            CheckAdmin = false;
        }

        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [Required]
        public DateTime ExpirationDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [Required]
        public DateTime InActivationDate { get; set; }

        [Required]
        public int LicenseKeyId { get; set; }

        [Required]
        [StringLength(64, MinimumLength = 1, ErrorMessage = "Hardware id is either too small or too long. Must be less than 64 characters")]
        public string HardwareId { get; set; }
        
        [Required]
        [StringLength(256, MinimumLength = 1, ErrorMessage = "Activation key is either too small or too long. Must be less than 256 characters")]
        public string ActivationKey { get; set; }

        [Required]
        public bool CheckAdmin { get; set; }

        public string Name { get; set; }

        public virtual LicenseKey LicenseKey { get; set; }

        public void SetInActivation(Activation Activation, DateTime Time, bool CheckAdmin)
        {
            this.LicenseKeyId = Activation.LicenseKeyId;
            this.ActivationKey = Activation.ActivationKey;
            this.HardwareId = Activation.HardwareId;
            this.InActivationDate = Time;
            this.ExpirationDate = Activation.ExpirationDate.Value;
            this.CheckAdmin = CheckAdmin;
            this.Name = Activation.Name;
        }
    }
}
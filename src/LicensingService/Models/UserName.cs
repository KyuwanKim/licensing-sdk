﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Licensing.Web.Models
{
    public class UserName
    {
        public UserName()
        {

        }
        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [Required]
        public DateTime EditDate { get; set; }

        [Required]
        public int LicenseKeyId { get; set; }

        //[Required]
        public string Names { get; set; }

        public string NamesActivationCnt { get; set; }

        public string NamesActivationCnt2
        {
            get
            {
                return Names + "\n" + NamesActivationCnt;
            }
        }

        public virtual LicenseKey LicenseKey { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Licensing.Web.DAL;
//using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using System.Globalization;
using System.Security.Cryptography;

namespace Licensing.Web.Models
{
    public class LicenseManagementInfo
    {
        public LicenseManagementInfo()
        {

        }

        public LicenseManagementInfo(LicenseKey Data, LicensingServiceDbContext db)
        {
            LicenseKey = Data.Key;
            ProductName = Data.Product.Name;
            Version = Data.OrderItem.Order.Version;
            CompanyName = Data.OrderItem.Order.Company;

            ActivationLimit = Data.ActivationLimit.Value;
            LicenseDuration = Data.LicenseDuration.Value;
            AutoInactivation = Data.AutoInactivation;

            UserNames = "";
            List<UserName> UseNameList = db.UserNames.Where(a => a.Id == Data.UserNameId).ToList();
            foreach (UserName UserNameData in UseNameList)
                UserNames += UserNameData.Names;

            ActivationInfos = new List<ActivationInfo>();
            List<Activation> ActList = db.Activations.Where(a => a.LicenseKeyId == Data.Id).ToList();
            foreach (Activation ActData in ActList)
            {
                ActivationInfo ActDataInfo = new ActivationInfo(ActData);
                ActivationInfos.Add(ActDataInfo);
            }
        }

        public string LicenseKey { get; set; }
        public string ProductName { get; set; }
        public string Version { get; set; }
        public string CompanyName { get; set; }
        public short ActivationLimit { get; set; }
        public DateTime? LicenseDuration { get; set; }
        public int UpdateDuration { get; set; }
        public bool AutoInactivation { get; set; }
        public string UserNames { get; set; }
        public List<ActivationInfo> ActivationInfos { get; set; }

        static public string GetJSonString(LicenseManagementInfo data)
        {
            string str = JsonHelper.JsonSerializer<LicenseManagementInfo>(data);
            System.Text.Encoding utf8 = System.Text.Encoding.UTF8;
            return utf8.GetString(utf8.GetBytes(str));
        }

        static public LicenseManagementInfo GetLicenseKeyInfoByJSonString(string data)
        {
            LicenseManagementInfo newLicenseKeyInfo = JsonHelper.JsonDeserialize<LicenseManagementInfo>(data);
            return newLicenseKeyInfo;
        }

        static public string GetSHA512Hash(string plainText)
        {
            SHA512 sha = new SHA512Managed();
            byte[] hashArray = sha.ComputeHash(System.Text.Encoding.ASCII.GetBytes(plainText));
            string hashText = Convert.ToBase64String(hashArray);
            return hashText;
        }
    }

    public class ActivationInfo
    {
        public ActivationInfo()
        {

        }
        public ActivationInfo(Activation Data)
        {
            ActivationKey = Data.ActivationKey;
            HardwareId = Data.HardwareId;
            ExpirationDate = Data.ExpirationDate;
            UserName = Data.Name;
            LoginDate = Data.LoginDate;
        }

        public string ActivationKey { get; set; }
        public string HardwareId { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? LoginDate { get; set; }
        public string UserName { get; set; }
    }
}
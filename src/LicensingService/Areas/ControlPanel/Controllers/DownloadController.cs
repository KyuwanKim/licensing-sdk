﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LicensingService.Areas.ControlPanel.Controllers
{
    public class DownloadController : Controller
    {
        //
        // GET: /ControlPanel/Download/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /ControlPanel/Download/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /ControlPanel/Download/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ControlPanel/Download/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ControlPanel/Download/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ControlPanel/Download/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ControlPanel/Download/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ControlPanel/Download/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Licensing.Web.Models;
using Licensing.Web.DAL;
using PagedList;
using LicensingService;
using LicensingService.Filters;

namespace Licensing.ControlPanel.Controllers
{
    public class InActivationController : ControlPanelController
    {
        //
        // GET: /ControlPanel/InActivation/
        private LicensingServiceDbContext db = new LicensingServiceDbContext();
        private GenericRepository<InActivation> InActivationsRepository;
        private GenericRepository<Product> productsRepository;

        public InActivationController()
        {
            InActivationsRepository = new GenericRepository<InActivation>(db);
            productsRepository = new GenericRepository<Product>(db);
        }

        //
        // GET: /InActivation/

        public ActionResult Index(DateTime? startDate, DateTime? endDate, int? selectedProduct, int? page, string CompanyName, string LicenseKey, string HardwareID, string GlobalList)
        {
            DateTime InitDate = new DateTime(2015, 1, 1);
            var products = productsRepository.Get(orderBy: q => q.OrderBy(d => d.Name));

            ViewBag.SelectedProduct = new SelectList(products, "Id", "Name", selectedProduct);
            ViewBag.StartDate = (startDate == null) ? InitDate : startDate;
            ViewBag.EndDate = (endDate == null) ? DateTime.UtcNow.ToLocalTime() : endDate;
            ViewBag.CompanyName = (CompanyName == null) ? "" : CompanyName;
            ViewBag.LicenseKey = (LicenseKey == null) ? "" : LicenseKey;
            ViewBag.HardwareID = (HardwareID == null) ? "" : HardwareID;
            ViewBag.GlobalList = new SelectList(new List<string>() { "TRUE", "FALSE" }, GlobalList);

            if (CompanyName == "") CompanyName = null;
            if (LicenseKey == "") LicenseKey = null;
            if (HardwareID == "") HardwareID = null;

            bool chkGlobal = false;
            bool IsGlobal = false;
            if (GlobalList == "TRUE")
            {
                chkGlobal = true;
                IsGlobal = true;
            }
            else if (GlobalList == "FALSE")
            {
                chkGlobal = true;
                IsGlobal = false;
            }

            int productId = selectedProduct.GetValueOrDefault();

            if (endDate.HasValue)
                endDate = endDate.Value.AddDays(1);

            var InActivations = InActivationsRepository.GetQuery(
                filter: s => (!selectedProduct.HasValue/* || s.LicenseKey.ProductId == productId*/)
                             && (startDate == null || s.InActivationDate >= startDate)
                             && (endDate == null || s.InActivationDate< endDate)
                             && (CompanyName == null || s.LicenseKey.OrderItem.Order.Company.Contains(CompanyName))
                             && (LicenseKey == null || s.LicenseKey.Key.Contains(LicenseKey))
                             && (HardwareID == null || s.HardwareId.Contains(HardwareID))
                             && (!chkGlobal || s.LicenseKey.IsGlobal == IsGlobal),
                orderBy: q => q.OrderByDescending(s => s.InActivationDate));

            int pageNumber = (Request.HttpMethod == "POST") ? 1 : (page ?? 1);

            return View(InActivations.ToPagedList(pageNumber, 20));
        }

        //
        // GET: /InActivation/Details/5

        public ActionResult Details(int id = 0)
        {
            InActivation InActivation = db.InActivations.Find(id);
            if (InActivation == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsReadOnly = true;

            return View("InActivationForm", InActivation);
        }

        //
        // GET: /Activation/Create

        [Authorize(Roles = "Administrators")]
        public ActionResult Create()
        {
            return View("InActivationForm");
        }

        //
        // POST: /Activation/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create([Bind(Exclude = "Id")]InActivation inActivation)
        {
            if (ModelState.IsValid)
            {
                LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == inActivation.LicenseKey.Key).FirstOrDefault();
                inActivation.LicenseKey = null;
                inActivation.LicenseKeyId = FindLicensekey.Id;
                db.InActivations.Add(inActivation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inActivation);
        }

        //
        // GET: /InActivation/Edit/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id = 0)
        {
            InActivation InActivation = db.InActivations.Find(id);
            if (InActivation == null)
            {
                return HttpNotFound();
            }

            return View("InActivationForm", InActivation);
        }

        //
        // POST: /InActivation/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(InActivation inActivation)
        {
            if (ModelState.IsValid)
            {
                LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == inActivation.LicenseKey.Key).FirstOrDefault();
                inActivation.LicenseKey = null;
                inActivation.LicenseKeyId = FindLicensekey.Id;
                db.Entry(inActivation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inActivation);
        }

        //
        // POST: /InActivation/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult DeleteConfirmed(int id)
        {
            InActivation InActivation = db.InActivations.Find(id);
            string strLicenseKey = "";
            string CompanyName = "";
            string strActKey = "Activation Key:" + InActivation.ActivationKey;
            string strHWID = "HWID:" + InActivation.HardwareId;
            if (InActivation.LicenseKey != null)
            {
                strLicenseKey = "LicenseKey Key:" + InActivation.LicenseKey.Key;
                CompanyName = InActivation.LicenseKey.OrderItem.Order.Company;
            }

            EventLog.Log("Delete InActivation Key", strLicenseKey, EventType.Information, "InActivation", strActKey, strHWID, null, CompanyName);
            db.InActivations.Remove(InActivation);
            db.SaveChanges();

            
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        [Authorize(Roles = "Administrators")]
        public ActionResult RemoveAllExpirationInActivation()
        {
            db.Database.ExecuteSqlCommand("delete from InActivations where ExpirationDate < GETDATE();");
            return RedirectToAction("Index");

            /*
            // 너무 오래 걸림 쿼리문으로 변경
            string strDefKey = "AAAAA-AAAAA-AAAAA-AAAAA-AAAAA";
            string strDefName = "AAAAA";
            string strActKey = "Remove ActivationKey: ";
            string strHWID = "Remove HardwareID: ";
            string strLKey = "Remove LicenseKey Key: ";
            string strCompanyName = "Remove Company Name: ";
            int RemoveCnt = 0;
            DateTime CurTime = DateTime.UtcNow.ToLocalTime();

            Dictionary<int, LicenseKey> LicenseKeyDic = new Dictionary<int, LicenseKey>();
            List<InActivation> InActList = db.InActivations.Where(a => a.ExpirationDate < CurTime).ToList();

            foreach (InActivation InAct in InActList)
            {
                LicenseKey LKey = InAct.LicenseKey;
                
                if (LKey == null)
                    strLKey += strDefKey + "/ ";
                else
                    strLKey += InAct.LicenseKey.Key + "/ ";

                strHWID += InAct.HardwareId + "/ ";
                strActKey += InAct.ActivationKey + "/ ";

                if (LKey == null || LKey.OrderItem == null || LKey.OrderItem.Order == null)
                    strCompanyName += strDefName + "/ ";
                else
                    strCompanyName += LKey.OrderItem.Order.Company + "/ ";

                db.InActivations.Remove(InAct);
                RemoveCnt++;
                if (RemoveCnt == 50)
                {
                    EventLog.Log("Remove All Expiration InActivation Keys", strLKey, EventType.Information, "Remove All Expiration InActivation Keys", strActKey, strHWID + strCompanyName, null);
                    RemoveCnt = 0;

                    strActKey = "Remove InActivationKey: ";
                    strHWID = "Remove HardwareID: ";
                    strLKey = "Remove LicenseKey Key: ";
                    strCompanyName = "Remove Company Name: ";
                }
            }

            if (RemoveCnt > 0)
            {
                db.SaveChanges();
                EventLog.Log("Remove All Expiration InActivation Keys", strLKey, EventType.Information, "Remove All Expiration InActivation Keys", strActKey, strHWID + strCompanyName, null);
            }
            db.SaveChanges();

            return RedirectToAction("Index");
            */
        }
    }
}

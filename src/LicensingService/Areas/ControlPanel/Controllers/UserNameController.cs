﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Licensing.Web.Models;
using Licensing.Web.DAL;
using PagedList;
using LicensingService;
using LicensingService.Filters;

namespace Licensing.ControlPanel.Controllers
{
    public class UserNameController : ControlPanelController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();
        private GenericRepository<UserName> UserNameRepository;
        private GenericRepository<Product> productsRepository;

        public UserNameController()
        {
            UserNameRepository = new GenericRepository<UserName>(db);
            productsRepository = new GenericRepository<Product>(db);
        }
        
        //
        // GET: /ControlPanel/UserName/

        public ActionResult Index(DateTime? startDate, DateTime? endDate, int? selectedProduct, int? page, string CompanyName, string LicenseKey)
        {
            DateTime InitDate = new DateTime(2015, 1, 1);
            var products = productsRepository.Get(orderBy: q => q.OrderBy(d => d.Name));

            ViewBag.SelectedProduct = new SelectList(products, "Id", "Name", selectedProduct);
            ViewBag.StartDate = (startDate == null) ? InitDate : startDate;
            ViewBag.EndDate = (endDate == null) ? DateTime.UtcNow.ToLocalTime() : endDate;
            ViewBag.CompanyName = (CompanyName == null) ? "" : CompanyName;
            ViewBag.LicenseKey = (LicenseKey == null) ? "" : LicenseKey;
            if (CompanyName == "") CompanyName = null;
            if (LicenseKey == "") LicenseKey = null;

            int productId = selectedProduct.GetValueOrDefault();

            if (endDate.HasValue)
                endDate = endDate.Value.AddDays(1);

            var UserNames = UserNameRepository.GetQuery(
                filter: s => (!selectedProduct.HasValue || s.LicenseKey.ProductId == productId)
                             && (startDate == null || s.EditDate >= startDate)
                             && (endDate == null || s.EditDate < endDate)
                             && (CompanyName == null || s.LicenseKey.OrderItem.Order.Company.Contains(CompanyName))
                             && (LicenseKey == null || s.LicenseKey.Key.Contains(LicenseKey)),
                orderBy: q => q.OrderByDescending(s => s.EditDate));

            int pageNumber = (Request.HttpMethod == "POST") ? 1 : (page ?? 1);

            return View(UserNames.ToPagedList(pageNumber, 20));
        }

        //
        // GET: /UserName/Details/5

        public ActionResult Details(int id = 0)
        {
            UserName username = db.UserNames.Find(id);
            if (username == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsReadOnly = true;

            return View("UserNameForm", username);
        }

        //
        // GET: /UserName/Create

//         [Authorize(Roles = "Administrators")]
//         public ActionResult Create()
//         {
//             return View("UserNameForm");
//         }

        [Authorize(Roles = "Administrators")]
        public ActionResult Create(int Id = 0)
        {
            LicenseKey LKey = db.LicenseKeys.Find(Id);
            UserName userName = new UserName();
            userName.LicenseKey = LKey;
            userName.LicenseKeyId = Id;
            userName.EditDate = DateTime.UtcNow.ToLocalTime();
            return View("UserNameForm", userName);
        }

        //
        // POST: /UserName/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create([Bind(Exclude = "Id")]UserName UserName)
        {
            if (ModelState.IsValid)
            {
                LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == UserName.LicenseKey.Key).FirstOrDefault();
                if(FindLicensekey == null)
                    return View(UserName);

                UserName.LicenseKey = FindLicensekey;
                UserName.LicenseKeyId = FindLicensekey.Id;
                UserName.Names = UserName.Names.Trim();
                UserName.Names = UserName.Names.Replace("\r\n", "");

                db.UserNames.Add(UserName);
                db.SaveChanges();

                FindLicensekey.CheckUserName = true;
                FindLicensekey.UserNameId = UserName.Id;
                db.Entry(FindLicensekey).State = EntityState.Modified;
                db.SaveChanges();

                string[] split = UserName.Names.Split(new char[] { '|', ',', '/' });
                foreach (string s in split)
                {
                    if (s.Length == 0) continue;

                    UserInfo userInfo = new UserInfo();
                    userInfo.EditDate = DateTime.UtcNow.ToLocalTime();
                    userInfo.LicenseKeyId = FindLicensekey.Id;
                    userInfo.Name = s;
                    userInfo.ActivationCnt = db.Activations.Where(a => a.LicenseKeyId == FindLicensekey.Id && a.Name == s).Count(); ;
                    userInfo.IsHold = false;
                    userInfo.HoldDate = null;
                    userInfo.HoldCnt = 0;
                    userInfo.LicenseKey = FindLicensekey;
                    db.UserInfoes.Add(userInfo);
                    db.SaveChanges();
                }

                DBHelper.CalcUserNamesActCnt(db, UserName.Id);
                return RedirectToAction("Index");
            }

            return View(UserName);
        }

        //
        // GET: /UserName/Edit/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id = 0)
        {
            UserName UserName = db.UserNames.Find(id);
            if (UserName == null)
            {
                return HttpNotFound();
            }

            return View("UserNameForm", UserName);
        }

        //
        // POST: /UserName/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(UserName UserName)
        {
            if (UserName.Names == null || UserName.Names == "")
                return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == UserName.LicenseKey.Key).FirstOrDefault();
                UserName.LicenseKey = null;
                UserName.LicenseKeyId = FindLicensekey.Id;
                UserName.Names = UserName.Names.Trim();
                UserName.Names = UserName.Names.Replace("\r\n", "");
                FindLicensekey.CheckUserName = true;
                FindLicensekey.UserNameId = UserName.Id;
                db.Entry(UserName).State = EntityState.Modified;
                db.SaveChanges();
                
                List<UserInfo> userInfoes = db.UserInfoes.Where(a => a.LicenseKeyId == FindLicensekey.Id).ToList();
                Dictionary<string, bool> DicFind = new Dictionary<string, bool>();
                Dictionary<string, UserInfo> DicUser = new Dictionary<string, UserInfo>();
                foreach (UserInfo u in userInfoes)
                {
                    DicFind.Add(u.Name, false);
                    DicUser.Add(u.Name, u);
                }

                string[] split = UserName.Names.Split(new char[] { '|', ',', '/' });
                foreach (string s in split)
                {
                    if (s.Length == 0) continue;
                    UserInfo findUserInfo;
                    if (DicUser.TryGetValue(s, out findUserInfo))
                    {
                        DicFind[s] = true;
                    }
                    else
                    {
                        UserInfo userInfo = new UserInfo();
                        userInfo.EditDate = DateTime.UtcNow.ToLocalTime();
                        userInfo.LicenseKeyId = FindLicensekey.Id;
                        userInfo.Name = s;
                        userInfo.ActivationCnt = db.Activations.Where(a => a.LicenseKeyId == FindLicensekey.Id && a.Name == s).Count();
                        userInfo.IsHold = false;
                        userInfo.HoldDate = null;
                        userInfo.HoldCnt = 0;
                        userInfo.LicenseKey = FindLicensekey;
                        db.UserInfoes.Add(userInfo);
                        db.SaveChanges();
                    }
                }

                foreach(KeyValuePair<string, bool> FindData in DicFind)
                {
                    if(FindData.Value == false)
                    {
                        UserInfo findUserInfo;
                        if (DicUser.TryGetValue(FindData.Key, out findUserInfo))
                        {
                            db.UserInfoes.Remove(findUserInfo);
                            db.SaveChanges();
                        }
                    }
                }

                DBHelper.CalcUserNamesActCnt(db, UserName.Id);
                return RedirectToAction("Index");
            }

            return View(UserName);
        }

        //
        // POST: /UserName/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult DeleteConfirmed(int id)
        {
            UserName UserName = db.UserNames.Find(id);
            string strLicenseKey = "";
            string CompanyName = "";
            string strActKey = UserName.Names;
            string strHWID = UserName.NamesActivationCnt;
            if (UserName.LicenseKey != null)
            {
                strLicenseKey = "LicenseKey Key:" + UserName.LicenseKey.Key;
                CompanyName = UserName.LicenseKey.OrderItem.Order.Company;

                UserName.LicenseKey.UserNameId = 0;
                UserName.LicenseKey.CheckUserName = false;
                db.Entry(UserName.LicenseKey).State = EntityState.Modified;
                db.SaveChanges();
            }

            List<UserInfo> userInfoes = db.UserInfoes.Where(a => a.LicenseKeyId == UserName.LicenseKeyId).ToList();
            foreach(UserInfo u in userInfoes)
                db.UserInfoes.Remove(u);
            db.SaveChanges();

            EventLog.Log("Delete UserName Key", strLicenseKey, EventType.Information, "UserName", strActKey, strHWID, null, CompanyName);
            db.UserNames.Remove(UserName);
            db.SaveChanges();


            return RedirectToAction("Index");
        }

//         void UpdateUserNamesActivationCnt(LicensingServiceDbContext db, UserName UserName)
//         {
//             List<Activation> ActList;
//             ActList = db.Activations.Where(a => a.LicenseKeyId == UserName.LicenseKeyId).ToList();
// 
//             string[] split = UserName.Names.Split(new char[]{'|', ',', '/'});
// 
//             string strNameActCnt = "";
// 
//             foreach(string s in split)
//             {
//                 if (s.Length == 0) continue;
//                 int nCnt = ActList.Where(a => a.Name == s).Count();
//                 strNameActCnt += nCnt.ToString() + "|";
//             }
//             UserName.NamesActivationCnt = strNameActCnt.Substring(0, strNameActCnt.Length - 1);
//         }
    }
}

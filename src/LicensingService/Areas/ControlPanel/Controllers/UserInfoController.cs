﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Licensing.Web.Models;
using Licensing.Web.DAL;
using PagedList;
using LicensingService;
using LicensingService.Filters;

namespace Licensing.ControlPanel.Controllers
{
    public class UserInfoController : ControlPanelController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();
        private GenericRepository<UserInfo> UserInfoRepository;
        private GenericRepository<Product> productsRepository;

        public UserInfoController()
        {
            UserInfoRepository = new GenericRepository<UserInfo>(db);
            productsRepository = new GenericRepository<Product>(db);
        }
        
        //
        // GET: /ControlPanel/UserInfo/

        public ActionResult Index(DateTime? startDate, DateTime? endDate, int? selectedProduct, int? page, string CompanyName, string LicenseKey)
        {
            DateTime InitDate = new DateTime(2015, 1, 1);
            var products = productsRepository.Get(orderBy: q => q.OrderBy(d => d.Name));

            ViewBag.SelectedProduct = new SelectList(products, "Id", "Name", selectedProduct);
            ViewBag.StartDate = (startDate == null) ? InitDate : startDate;
            ViewBag.EndDate = (endDate == null) ? DateTime.UtcNow.ToLocalTime() : endDate;
            ViewBag.CompanyName = (CompanyName == null) ? "" : CompanyName;
            ViewBag.LicenseKey = (LicenseKey == null) ? "" : LicenseKey;
            if (CompanyName == "") CompanyName = null;
            if (LicenseKey == "") LicenseKey = null;

            int productId = selectedProduct.GetValueOrDefault();

            if (endDate.HasValue)
                endDate = endDate.Value.AddDays(1);

            var UserInfos = UserInfoRepository.GetQuery(
                filter: s => (!selectedProduct.HasValue || s.LicenseKey.ProductId == productId)
                             && (startDate == null || s.EditDate >= startDate)
                             && (endDate == null || s.EditDate < endDate)
                             && (CompanyName == null || s.LicenseKey.OrderItem.Order.Company.Contains(CompanyName))
                             && (LicenseKey == null || s.LicenseKey.Key.Contains(LicenseKey)),
                orderBy: q => q.OrderByDescending(s => s.EditDate));

            int pageNumber = (Request.HttpMethod == "POST") ? 1 : (page ?? 1);

            return View(UserInfos.ToPagedList(pageNumber, 20));
        }

        //
        // GET: /UserInfo/Details/5

        public ActionResult Details(int id = 0)
        {
            UserInfo UserInfo = db.UserInfoes.Find(id);
            if (UserInfo == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsReadOnly = true;

            return View("UserInfoForm", UserInfo);
        }

        //
        // GET: /UserInfo/Create

//         [Authorize(Roles = "Administrators")]
//         public ActionResult Create()
//         {
//             return View("UserInfoForm");
//         }

        [Authorize(Roles = "Administrators")]
        public ActionResult Create(int Id = 0)
        {
            LicenseKey LKey = db.LicenseKeys.Find(Id);
            UserInfo UserInfo = new UserInfo();
            UserInfo.LicenseKey = LKey;
            UserInfo.LicenseKeyId = Id;
            UserInfo.EditDate = DateTime.UtcNow.ToLocalTime();
            return View("UserInfoForm", UserInfo);
        }

        //
        // POST: /UserInfo/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create([Bind(Exclude = "Id")]UserInfo UserInfo)
        {
            if (ModelState.IsValid)
            {
                LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == UserInfo.LicenseKey.Key).FirstOrDefault();
                if(FindLicensekey == null)
                    return View(UserInfo);

                UserInfo.LicenseKey = FindLicensekey;
                UserInfo.LicenseKeyId = FindLicensekey.Id;

                UserInfo.Name = UserInfo.Name.Trim();
                UserInfo.ActivationCnt = db.Activations.Where(a => a.LicenseKeyId == FindLicensekey.Id && a.Name == UserInfo.Name).Count();
                db.UserInfoes.Add(UserInfo);
                db.SaveChanges();

                DBHelper.CalcUserNamesNames(db, FindLicensekey.UserNameId);
                DBHelper.CalcUserNamesActCnt(db, FindLicensekey.UserNameId);

                return RedirectToAction("Index");
            }

            return View(UserInfo);
        }

        //
        // GET: /UserInfo/Edit/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id = 0)
        {
            UserInfo UserInfo = db.UserInfoes.Find(id);
            if (UserInfo == null)
            {
                return HttpNotFound();
            }

            return View("UserInfoForm", UserInfo);
        }

        //
        // POST: /UserInfo/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(UserInfo UserInfo)
        {
            if (ModelState.IsValid)
            {
                LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == UserInfo.LicenseKey.Key).FirstOrDefault();
                UserInfo.LicenseKey = null;
                UserInfo.LicenseKeyId = FindLicensekey.Id;
                UserInfo.Name = UserInfo.Name.Trim();

                List<Activation> ActList = db.Activations.Where(a => a.LicenseKeyId == FindLicensekey.Id && a.Name == UserInfo.Name).ToList();
                UserInfo.ActivationCnt = ActList.Count();

                if (UserInfo.IsHold && UserInfo.ActivationCnt > 0)
                {
                    foreach (Activation FindAct in ActList)
                    {
                        InActivation InActivation = new InActivation();
                        InActivation.SetInActivation(FindAct, DateTime.UtcNow.ToLocalTime(), false);

                        db.InActivations.Add(InActivation);
                        db.Activations.Remove(FindAct);
                    }
                    db.SaveChanges();
                    UserInfo.ActivationCnt = db.Activations.Where(a => a.LicenseKeyId == FindLicensekey.Id && a.Name == UserInfo.Name).Count();
                }

                db.Entry(UserInfo).State = EntityState.Modified;
                db.SaveChanges();

                DBHelper.CalcUserNamesNames(db, FindLicensekey.UserNameId);
                DBHelper.CalcUserNamesActCnt(db, FindLicensekey.UserNameId);
                return RedirectToAction("Index");
            }

            return View(UserInfo);
        }

        //
        // POST: /UserInfo/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult DeleteConfirmed(int id)
        {
            UserInfo UserInfo = db.UserInfoes.Find(id);
            LicenseKey licenseKey = UserInfo.LicenseKey;
            string strLicenseKey = "";
            string CompanyName = "";
            string strActKey = UserInfo.Name;
            string strHWID = UserInfo.HoldCnt.ToString();
            if (licenseKey != null)
            {
                strLicenseKey = "LicenseKey Key:" + licenseKey.Key;
                CompanyName = licenseKey.OrderItem.Order.Company;

                db.Entry(licenseKey).State = EntityState.Modified;
                db.SaveChanges();
            }

            EventLog.Log("Delete UserInfo" + UserInfo.Name, strLicenseKey, EventType.Information, "UserInfo", strActKey, strHWID, null, CompanyName);
            db.UserInfoes.Remove(UserInfo);
            db.SaveChanges();

            if (licenseKey != null)
            {
                DBHelper.CalcUserNamesNames(db, licenseKey.UserNameId);
                DBHelper.CalcUserNamesActCnt(db, licenseKey.UserNameId);
            }

            return RedirectToAction("Index");
        }

//         void UpdateUserInfosActivationCnt(LicensingServiceDbContext db, UserInfo UserInfo)
//         {
//             List<Activation> ActList;
//             ActList = db.Activations.Where(a => a.LicenseKeyId == UserInfo.LicenseKeyId).ToList();
// 
//             string[] split = UserInfo.Names.Split(new char[]{'|', ',', '/'});
// 
//             string strNameActCnt = "";
// 
//             foreach(string s in split)
//             {
//                 if (s.Length == 0) continue;
//                 int nCnt = ActList.Where(a => a.Name == s).Count();
//                 strNameActCnt += nCnt.ToString() + "|";
//             }
//             UserInfo.NamesActivationCnt = strNameActCnt.Substring(0, strNameActCnt.Length - 1);
//         }
    }
}

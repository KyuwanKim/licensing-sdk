﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Licensing.Web.Models;
using Licensing.Web.DAL;
using PagedList;
using LicensingService;
using LicensingService.Filters;

namespace Licensing.ControlPanel.Controllers
{
    public class LicenseKeyController : ControlPanelController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();
        private GenericRepository<Product> productsRepository;
        private GenericRepository<Order> ordersRepository;
        private GenericRepository<LicenseKey> licenseKeysRepository;

        public LicenseKeyController()
        {
            productsRepository = new GenericRepository<Product>(db);
            ordersRepository = new GenericRepository<Order>(db);
            licenseKeysRepository = new GenericRepository<LicenseKey>(db);
        }

        //
        // GET: /LicenseKey/

        public ActionResult Index(DateTime? startDate, DateTime? endDate, int? selectedProduct, int? page, string CompanyName, string LicenseKey, string GlobalList)
        {
            DateTime InitDate = new DateTime(2015, 1, 1);
            var products = productsRepository.Get(orderBy: q => q.OrderBy(d => d.Name));

            ViewBag.SelectedProduct = new SelectList(products, "Id", "Name", selectedProduct);
            ViewBag.StartDate = (startDate == null) ? InitDate : startDate;
            ViewBag.EndDate = (endDate == null) ? DateTime.UtcNow.ToLocalTime() : endDate;
            ViewBag.CompanyName = (CompanyName == null) ? "" : CompanyName;
            ViewBag.LicenseKey = (LicenseKey == null) ? "" : LicenseKey;
            ViewBag.GlobalList = new SelectList(new List<string>() { "TRUE", "FALSE" }, GlobalList);

            if (CompanyName == "") CompanyName = null;
            if (LicenseKey == "") LicenseKey = null;

            bool chkGlobal = false;
            bool IsGlobal = false;
            if (GlobalList == "TRUE")
            {
                chkGlobal = true;
                IsGlobal = true;
            }
            else if (GlobalList == "FALSE")
            {
                chkGlobal = true;
                IsGlobal = false;
            }

            int productId = selectedProduct.GetValueOrDefault();

            if (endDate.HasValue)
                endDate = endDate.Value.AddDays(1);
            
            var licenseKeys = licenseKeysRepository.GetQuery(
                filter: s => (!selectedProduct.HasValue || s.ProductId == productId)
                             && (startDate == null || s.DateAdded >= startDate)
                             && (endDate == null || s.DateAdded < endDate)
                             && (CompanyName == null || s.OrderItem.Order.Company.Contains(CompanyName))
                             && (LicenseKey == null || s.Key.Contains(LicenseKey))
                             && (!chkGlobal || s.IsGlobal == IsGlobal),
                orderBy: q => q.OrderByDescending(s => s.DateAdded),
                includeProperties: "OrderItem, Product");
                
            int pageNumber = (Request.HttpMethod == "POST") ? 1 : (page ?? 1);

            return View(licenseKeys.ToPagedList(pageNumber, 20));
        }

        //
        // GET: /LicenseKey/Details/5

        public ActionResult Details(int id = 0)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);
            if (licensekey == null)
            {
                return HttpNotFound();
            }

            db.Products.Load();
            ViewBag.SelectedProduct = new SelectList(db.Products, "Id", "Name", licensekey.ProductId.ToString());

            ViewBag.IsReadOnly = true;

            return View("LicenseKeyForm", licensekey);
        }

        //
        // GET: /LicenseKey/Create

        [Authorize(Roles = "Administrators")]
        public ActionResult Create()
        {
            db.Products.Load();
            ViewBag.SelectedProduct = new SelectList(db.Products, "Id", "Name");

            LicenseKey licensekey = new LicenseKey();
            licensekey.LicenseDuration = DateTime.UtcNow.ToLocalTime();

            return View("LicenseKeyForm", licensekey);
        }

        [Authorize(Roles = "Administrators")]
        public ActionResult ResetActivationcount()
        {
            List<LicenseKey> licenseKeyList = db.LicenseKeys.ToList();
            foreach (LicenseKey licensekey in licenseKeyList)
            {
                licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);
                db.Entry(licensekey).State = EntityState.Modified;
            }
            db.SaveChanges();
            
            return RedirectToAction("Index");
        }

        //
        // POST: /LicenseKey/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create([Bind(Exclude = "Id, ProductId, DateAdded")]LicenseKey licensekey, int SelectedProduct)
        {
            if (ModelState.IsValid)
            {
                licensekey.ProductId = SelectedProduct;
                licensekey.DateAdded = DateTime.UtcNow.ToLocalTime();
                db.LicenseKeys.Add(licensekey);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("LicenseKeyForm", licensekey);
        }

        //
        // GET: /LicenseKey/Edit/5

        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id = 0)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);
            if (licensekey == null)
            {
                return HttpNotFound();
            }

            db.Products.Load();
            ViewBag.SelectedProduct = new SelectList(db.Products, "Id", "Name", licensekey.ProductId.ToString());

            return View("LicenseKeyForm", licensekey);
        }

        //
        // POST: /LicenseKey/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(LicenseKey licensekey)
        {
            if (ModelState.IsValid)
            {
                OrderItem Orderit = db.OrderItems.Find(licensekey.OrderItemId);
                string strCompanyName = "";
                if (Orderit != null)
                    strCompanyName = Orderit.Order.Company;
                licensekey.OrderItem = null;
                licensekey.Product = null;
                licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);

                db.Entry(licensekey).State = EntityState.Modified;
                db.SaveChanges();

                string msg = " LicenseKey Key:" + licensekey.Key + " Key Count:" + licensekey.ActivationLimit.ToString() + " LicenseDuration:" + licensekey.LicenseDuration.ToString();
                EventLog.Log("Edit License Key", msg, EventType.Information, "Edit LicenseKey", null, null, null, strCompanyName);

                return RedirectToAction("Index");
            }

            return View("LicenseKeyForm", licensekey);
        }

        //
        // POST: /LicenseKey/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult DeleteConfirmed(int id)
        {
            ClearInActivation(id);
            LicenseKey licensekey = db.LicenseKeys.Find(id);

            int RemoveCnt = 0;
            List<Activation> ActList = db.Activations.Where(a => a.LicenseKeyId == licensekey.Id).ToList();
            string strActKey = "Remove ActivationKey: ";
            string strHWID = "Remove HardwareID: ";
            
            foreach (Activation actvation in ActList)
            {
                RemoveCnt++;
                strActKey += actvation.ActivationKey + "/ ";
                strHWID += actvation.HardwareId + "/ ";
                db.Activations.Remove(actvation);
            }
            
            UserName FindUserName = db.UserNames.Find(licensekey.UserNameId);
            if(FindUserName != null)
                db.UserNames.Remove(FindUserName);

            string strCompanyName = "";
            if (licensekey.OrderItem != null)
                strCompanyName = licensekey.OrderItem.Order.Company;
            string msg = " LicenseKey Key:" + licensekey.Key + " Limit Key Count:" + licensekey.ActivationLimit.ToString() + " RemoveCnt:" + RemoveCnt;

            EventLog.Log("Delete License Key", msg, EventType.Information, "Delete LicenseKey", strActKey, strHWID, null, strCompanyName);

            db.LicenseKeys.Remove(licensekey);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //
        // POST: /LicenseKey/Block/5

        [HttpPost, ActionName("Block")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult BlockConfirmed(int id)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);
            licensekey.Active = false;

            db.Entry(licensekey).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //
        // POST: /LicenseKey/Unblock/5

        [HttpPost, ActionName("Unblock")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult UnblockConfirmed(int id)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);
            licensekey.Active = true;

            db.Entry(licensekey).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //
        // POST: /LicenseKey/Unblock/5

        [HttpPost, ActionName("InActivationAll")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult InActivationAllConfirmed(int id)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);

            string strCompanyName = licensekey.OrderItem.Order.Company;
            string strActKey = "InActivationKey: ";
            string strHWID = "HardwareID: ";
            int RemoveCnt = 0;

            List<Activation> ActList = db.Activations.Where(a => a.LicenseKeyId == licensekey.Id).ToList();
            foreach (Activation activation in ActList)
            {
                InActivation InActivation = new InActivation();
                InActivation.SetInActivation(activation, DateTime.UtcNow.ToLocalTime(), false);
                db.InActivations.Add(InActivation);

                strActKey += activation.ActivationKey + "/ ";
                strHWID += activation.HardwareId + "/ ";
                db.Activations.Remove(activation);
                RemoveCnt++;
            }

            //licensekey.Key
            db.Entry(licensekey).State = EntityState.Modified;
            db.SaveChanges();

            licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);
            db.Entry(licensekey).State = EntityState.Modified;
            db.SaveChanges();

            DBHelper.CalcUserNamesActCnt(db, licensekey.UserNameId);

            string msg = "LicenseKey Key:" + licensekey.Key + " ActivationCount:" + licensekey.ActivationCount + " RemoveCnt: " + RemoveCnt;
            EventLog.Log("InActivation All Keys", msg, EventType.Information, "InActivation All Keys", strActKey, strHWID, null, strCompanyName);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("ClearExpAct")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult ClearExpActConfirmed(int id)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);

            string strCompanyName = licensekey.OrderItem.Order.Company;
            string strActKey = "Remove ActivationKey: ";
            string strHWID = "Remove HardwareID: ";
            int RemoveCnt = 0;

            //DateTime CurTime = DateTime.UtcNow.ToLocalTime();
            DateTime CurTime = new DateTime(DateTime.UtcNow.ToLocalTime().Year, DateTime.UtcNow.ToLocalTime().Month, DateTime.UtcNow.ToLocalTime().Day);
            List<Activation> ActList = db.Activations.Where(a => a.LicenseKeyId == licensekey.Id).ToList();
            foreach (Activation actvation in ActList)
            {
                DateTime ExpirationDate = new DateTime(actvation.ExpirationDate.Value.Year, actvation.ExpirationDate.Value.Month, actvation.ExpirationDate.Value.Day);
                if (ExpirationDate < CurTime)
                //if (ExpirationDate <= CurTime) // Old 버전을 위해서 삭제한다.
                {
                    strActKey += actvation.ActivationKey+ "/ ";
                    strHWID += actvation.HardwareId + "/ ";
                    db.Activations.Remove(actvation);
                    RemoveCnt++;
                }
            }

            //licensekey.Key
            db.Entry(licensekey).State = EntityState.Modified;
            db.SaveChanges();

            licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);
            db.Entry(licensekey).State = EntityState.Modified;
            db.SaveChanges();

            DBHelper.CalcUserNamesActCnt(db, licensekey.UserNameId);

            string msg = "LicenseKey Key:" + licensekey.Key + " ActivationCount:" + licensekey.ActivationCount + " RemoveCnt: " + RemoveCnt;
            EventLog.Log("Clear Expiration Activation Keys", msg, EventType.Information, "Clear Expiration Activation", strActKey, strHWID, null, strCompanyName);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        private void ClearInActivation(int id)
        {
            LicenseKey licensekey = db.LicenseKeys.Find(id);
            int RemoveCnt = 0;
            List<InActivation> ActList = db.InActivations.Where(a => a.LicenseKeyId == licensekey.Id).ToList();
            string strActKey = "Remove InActivationKey: ";
            string strHWID = "Remove HardwareID: ";

            foreach (InActivation inactvation in ActList)
            {
                RemoveCnt++;
                strActKey += inactvation.ActivationKey + "/ ";
                strHWID += inactvation.HardwareId + "/ ";
                db.InActivations.Remove(inactvation);
            }

            string strCompanyName = "";
            if (licensekey.OrderItem != null)
                strCompanyName = licensekey.OrderItem.Order.Company;
            string msg = " LicenseKey Key:" + licensekey.Key + " Limit Key Count:" + licensekey.ActivationLimit.ToString() + " RemoveCnt:" + RemoveCnt;

            EventLog.Log("Delete InActivation Key", msg, EventType.Information, "Delete InActivation", strActKey, strHWID, null, strCompanyName);
        }
    }
}

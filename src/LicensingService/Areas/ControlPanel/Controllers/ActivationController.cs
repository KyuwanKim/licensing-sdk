﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Licensing.Web.Models;
using Licensing.Web.DAL;
using PagedList;
using LicensingService;
using LicensingService.Filters;
using SoftActivate.Licensing;

namespace Licensing.ControlPanel.Controllers
{
    public class ActivationController : ControlPanelController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();
        private GenericRepository<Activation> activationsRepository;
        private GenericRepository<Product> productsRepository;

        public ActivationController()
        {
            activationsRepository = new GenericRepository<Activation>(db);
            productsRepository = new GenericRepository<Product>(db);
        }

        //
        // GET: /Activation/

        public ActionResult Index(DateTime? startDate, DateTime? endDate, int? selectedProduct, int? page, string CompanyName, string LicenseKey, string HardwareID, string GlobalList)
        {
            DateTime InitDate = new DateTime(2015, 1, 1);
            var products = productsRepository.Get(orderBy: q => q.OrderBy(d => d.Name));

            ViewBag.SelectedProduct = new SelectList(products, "Id", "Name", selectedProduct);
            ViewBag.StartDate = (startDate == null) ? InitDate : startDate;
            ViewBag.EndDate = (endDate == null) ? DateTime.UtcNow.ToLocalTime() : endDate;
            ViewBag.CompanyName = (CompanyName == null) ? "" : CompanyName;
            ViewBag.LicenseKey = (LicenseKey == null) ? "" : LicenseKey;
            ViewBag.HardwareID = (HardwareID == null) ? "" : HardwareID;
            ViewBag.GlobalList = new SelectList(new List<string>() { "TRUE", "FALSE" }, GlobalList);

            if (CompanyName == "") CompanyName = null;
            if (LicenseKey == "") LicenseKey = null;
            if (HardwareID == "") HardwareID = null;

            bool chkGlobal = false;
            bool IsGlobal = false;
            if (GlobalList == "TRUE")
            {
                chkGlobal = true;
                IsGlobal = true;
            }
            else if (GlobalList == "FALSE")
            {
                chkGlobal = true;
                IsGlobal = false;
            }

            int productId = selectedProduct.GetValueOrDefault();

            if (endDate.HasValue)
                endDate = endDate.Value.AddDays(1);

            var activations = activationsRepository.GetQuery(
                filter: s => (!selectedProduct.HasValue || s.LicenseKey.ProductId == productId)
                             && (startDate == null || s.ActivationDate >= startDate)
                             && (endDate == null || s.ActivationDate < endDate)
                             && (CompanyName == null || s.LicenseKey.OrderItem.Order.Company.Contains(CompanyName))
                             && (LicenseKey == null || s.LicenseKey.Key.Contains(LicenseKey))
                             && (HardwareID == null || s.HardwareId.Contains(HardwareID))
                             && (!chkGlobal || s.LicenseKey.IsGlobal == IsGlobal),
                orderBy: q => q.OrderByDescending(s => s.ActivationDate),
                includeProperties: "LicenseKey");

            int pageNumber = (Request.HttpMethod == "POST") ? 1 : (page ?? 1);

            return View(activations.ToPagedList(pageNumber, 20));
        }

        //
        // GET: /Activation/Details/5

        public ActionResult Details(int id = 0)
        {
            Activation activation = db.Activations.Find(id);
            if (activation == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsReadOnly = true;

            return View("ActivationForm", activation);
        }

        //
        // GET: /Activation/Create

        [Authorize(Roles = "Administrators")]
        public ActionResult Create()
        {
            return View("ActivationForm");
        }

        //
        // POST: /Activation/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create([Bind(Exclude="Id")]Activation activation)
        {
            if (ModelState.IsValid)
            {
                db.Activations.Add(activation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(activation);
        }

        //
        // GET: /Activation/Edit/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id = 0)
        {
            Activation activation = db.Activations.Find(id);
            if (activation == null)
            {
                return HttpNotFound();
            }

            return View("ActivationForm", activation);
        }

        //
        // POST: /Activation/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(Activation activation)
        {
            if (ModelState.IsValid)
            {
                int userNameId = activation.LicenseKey.UserNameId;
                activation.LicenseKey = null;

                db.Entry(activation).State = EntityState.Modified;
                db.SaveChanges();

                DBHelper.CalcUserNamesActCnt(db, userNameId);

                return RedirectToAction("Index");
            }

            return View(activation);
        }

        //
        // POST: /Activation/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult DeleteConfirmed(int id)
        {
            Activation activation = db.Activations.Find(id);
            LicenseKey licensekey = db.LicenseKeys.Find(activation.LicenseKeyId);
            string strLicenseKey = "";
            string strCompanyName = "";
            string strActKey = " Activation Key:" + activation.ActivationKey;
            string strHWID = " HWID:" + activation.HardwareId;

            if (licensekey != null)
            {
                strLicenseKey = "LicenseKey Key:" + licensekey.Key;
                strCompanyName = licensekey.OrderItem.Order.Company;
            }

            if (activation.Name!= null && activation.Name.Length > 0)
            {
                List<UserInfo> userInfList = db.UserInfoes.Where(a => a.LicenseKeyId == licensekey.Id && a.Name == activation.Name).ToList();
                foreach (UserInfo u in userInfList)
                    u.ActivationCnt = 0;
            }

            EventLog.Log("Delete Activation Key", strLicenseKey, EventType.Information, "Activation", strActKey, strHWID, null, strCompanyName);

            db.Activations.Remove(activation);
            db.SaveChanges();

            if (licensekey != null)
            {
                licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);
                db.Entry(licensekey).State = EntityState.Modified;
                db.SaveChanges();

                DBHelper.SetUserInfo_ActCnt(db, licensekey.Id, activation.Name, 0);
                DBHelper.CalcUserNamesActCnt(db, licensekey.UserNameId);
            }
            
            return RedirectToAction("Index");
        }

        //
        // POST: /Activation/InAct/5

        [HttpPost, ActionName("InActivation")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult InActivationConfirmed(int id)
        {
            Activation activation = db.Activations.Find(id);
            LicenseKey FindLicensekey = db.LicenseKeys.Find(activation.LicenseKeyId);

            InActivation InActivation = new InActivation();
            InActivation.SetInActivation(activation, DateTime.UtcNow.ToLocalTime(), false);

            db.InActivations.Add(InActivation);
            db.Activations.Remove(activation);
            db.SaveChanges();

            FindLicensekey.ActivationCount = DBHelper.GetActCnt(db, FindLicensekey) + DBHelper.GetTryActCnt(db, FindLicensekey);
            db.Entry(FindLicensekey).State = EntityState.Modified;
            db.SaveChanges();

            if (FindLicensekey != null)
            {
                DBHelper.SetUserInfo_ActCnt(db, FindLicensekey.Id, activation.Name, 0);
                DBHelper.CalcUserNamesActCnt(db, FindLicensekey.UserNameId);
            }

            string strSubject = "InActivation Key";
            string strActMsg = "InActivation Key:" + InActivation.ActivationKey + " ExpirationDate:" + InActivation.ExpirationDate.ToString();
            string strLMsg = "LicenseKey Key: " + FindLicensekey.Key + " ActKeyCnt: " + FindLicensekey.ActivationCount;
            string strHWMsg = "HardwareId Key: " + InActivation.HardwareId;
            EventLog.Log(strSubject, strActMsg, EventType.Information, "Activation", strLMsg, strHWMsg, null, FindLicensekey.OrderItem.Order.Company);
            
            return RedirectToAction("Index");
        }

        
        static public bool IsLicenseValid(LicensingServiceDbContext db, int ActivationID)
        {
            bool bRval = false;
            string strState = "FALSE";
            string strExpirationDate = "";
            Activation activation = db.Activations.Find(ActivationID);
            LicenseKey FindLicensekey = db.LicenseKeys.Find(activation.LicenseKeyId);
            Product product = db.Products.Find(FindLicensekey.ProductId);

            string xmlLicenseKeyTemplate = product.LicenseKeyTemplate;
            LicenseTemplate keyTemplate = new LicenseTemplate(xmlLicenseKeyTemplate);
            LicenseTemplate activationKeyTemplate = new LicenseTemplate(keyTemplate.NumberOfGroups,
                                                                keyTemplate.CharactersPerGroup,
                                                                keyTemplate.GetPublicKeyCertificate(),
                                                                keyTemplate.GetPrivateKey(),
                                                                keyTemplate.SignatureSize,
                                                                keyTemplate.DataSize,
                                                                "ExpirationDate");

            KeyValidator LKeyvalidator = new KeyValidator(keyTemplate);
            LKeyvalidator.SetValidationData("CustomerName", FindLicensekey.OrderItem.Order.Company);

            byte[] valldata = LKeyvalidator.QueryValidationData(null);

            string strLicenseKey = FindLicensekey.Key;
            string strHardwareId = activation.HardwareId;
            string strActivationKey = activation.ActivationKey;

            activationKeyTemplate.ValidationDataSize = (strLicenseKey.Length + strHardwareId.Length) * 8;

            activationKeyTemplate.AddValidationField("LicenseKey", LicenseKeyFieldType.String, 8 * strLicenseKey.Length, 0);
            activationKeyTemplate.AddValidationField("HardwareId", LicenseKeyFieldType.String, 8 * strHardwareId.Length, 8 * strLicenseKey.Length);

            if (valldata != null)
            {
                activationKeyTemplate.ValidationDataSize += valldata.Length * 8;
                activationKeyTemplate.AddValidationField("LicenseKeyValidationData", LicenseKeyFieldType.Raw, valldata.Length * 8, 8 * (strLicenseKey.Length + strHardwareId.Length));
            }

            // validate the generated key just to be sure
            KeyValidator validator = new KeyValidator(activationKeyTemplate, strActivationKey);

            validator.SetValidationData("LicenseKey", strLicenseKey);
            validator.SetValidationData("HardwareId", strHardwareId);

            if (valldata != null)
                validator.SetValidationData("LicenseKeyValidationData", valldata);

            if (validator.IsKeyValid())
            {
                strState = "OK";
                bRval = true;
            }

            DateTime licenseExpirationDate;
            TIME_VALIDATION_METHOD timeValidationMethod = TIME_VALIDATION_METHOD.PreferInternetTime;
            byte[] rawKeyData = validator.QueryKeyData("ExpirationDate");
            ushort keyData = (ushort)(((ushort)rawKeyData[0]) << 8 | rawKeyData[1]);

            if (keyData != 0)
            {
                licenseExpirationDate = new DateTime(2000 + (keyData >> 9), (keyData & 0x01FF) >> 5, keyData & 0x001F);

                DateTime now;

                if (timeValidationMethod != TIME_VALIDATION_METHOD.UseLocalTime)
                {
                    try
                    {
                        now = LicensingClient.GetCurrentTimeUTC(20000);
                        //System.Diagnostics.Trace.WriteLine("[LicensingSDK] The reported Internet time is " + now.ToString("D"));
                    }
                    catch (Exception)
                    {
                        if (timeValidationMethod == TIME_VALIDATION_METHOD.UseInternetTime)
                        {
                            throw;
                            // activationStatus = LICENSE_STATUS.Expired;
                            // return false;
                        }

                        now = DateTime.UtcNow.ToLocalTime();
                    }
                }
                else
                    now = DateTime.UtcNow.ToLocalTime();


                strExpirationDate = licenseExpirationDate.ToShortDateString();
                if (DateTime.Compare(licenseExpirationDate, now) < 0)
                {
                    bRval = false;
                    strState = "FALSE";
                }
            }

            string strSubject = "Check Activation Key State: " + strState;
            string strActMsg = "State: " + strState + " LicenseKey: " + strLicenseKey + " HardWareID: " + strHardwareId + " Activation Key:" + strActivationKey;
            string strLMsg = "ExpirationDate: " + strExpirationDate;
            string strHWMsg = "";
            EventLog.Log(strSubject, strActMsg, EventType.Information, "Activation", strLMsg, strHWMsg, null, FindLicensekey.OrderItem.Order.Company);




            return bRval;
        }
        

        [HttpPost, ActionName("CheckActivation")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult CheckActivationConfirmed(int id)
        {
            bool bRval = IsLicenseValid(db, id);
            /*
            string strState = "False";
            string strExpirationDate = "";

            Activation activation = db.Activations.Find(id);
            LicenseKey FindLicensekey = db.LicenseKeys.Find(activation.LicenseKeyId);
            Product product = db.Products.Find(FindLicensekey.ProductId);

            string xmlLicenseKeyTemplate = product.LicenseKeyTemplate;
            LicenseTemplate keyTemplate = new LicenseTemplate(xmlLicenseKeyTemplate);
            LicenseTemplate activationKeyTemplate = new LicenseTemplate(keyTemplate.NumberOfGroups,
                                                                keyTemplate.CharactersPerGroup,
                                                                keyTemplate.GetPublicKeyCertificate(),
                                                                keyTemplate.GetPrivateKey(),
                                                                keyTemplate.SignatureSize,
                                                                keyTemplate.DataSize,
                                                                "ExpirationDate");

            KeyValidator LKeyvalidator = new KeyValidator(keyTemplate);
            LKeyvalidator.SetValidationData("CustomerName", FindLicensekey.OrderItem.Order.Company);

            byte[] valldata = LKeyvalidator.QueryValidationData(null);

            string strLicenseKey = FindLicensekey.Key;
            string strHardwareId = activation.HardwareId;
            string strActivationKey = activation.ActivationKey;

            activationKeyTemplate.ValidationDataSize = (strLicenseKey.Length + strHardwareId.Length) * 8;

            activationKeyTemplate.AddValidationField("LicenseKey", LicenseKeyFieldType.String, 8 * strLicenseKey.Length, 0);
            activationKeyTemplate.AddValidationField("HardwareId", LicenseKeyFieldType.String, 8 * strHardwareId.Length, 8 * strLicenseKey.Length);

            if (valldata != null)
            {
                activationKeyTemplate.ValidationDataSize += valldata.Length * 8;
                activationKeyTemplate.AddValidationField("LicenseKeyValidationData", LicenseKeyFieldType.Raw, valldata.Length * 8, 8 * (strLicenseKey.Length + strHardwareId.Length));
            }

            // validate the generated key just to be sure
            KeyValidator validator = new KeyValidator(activationKeyTemplate, strActivationKey);

            validator.SetValidationData("LicenseKey", strLicenseKey);
            validator.SetValidationData("HardwareId", strHardwareId);

            if (valldata != null)
                validator.SetValidationData("LicenseKeyValidationData", valldata);

            if (validator.IsKeyValid())
            {
                strState = "OK";
                bRval = true;
            }

            DateTime licenseExpirationDate;
            TIME_VALIDATION_METHOD timeValidationMethod = TIME_VALIDATION_METHOD.PreferInternetTime;
            byte[] rawKeyData = validator.QueryKeyData("ExpirationDate");
            ushort keyData = (ushort)(((ushort)rawKeyData[0]) << 8 | rawKeyData[1]);

            if (keyData != 0)
            {
                licenseExpirationDate = new DateTime(2000 + (keyData >> 9), (keyData & 0x01FF) >> 5, keyData & 0x001F);

                DateTime now;

                if (timeValidationMethod != TIME_VALIDATION_METHOD.UseLocalTime)
                {
                    try
                    {
                        now = LicensingClient.GetCurrentTimeUTC(20000);
                        //System.Diagnostics.Trace.WriteLine("[LicensingSDK] The reported Internet time is " + now.ToString("D"));
                    }
                    catch (Exception)
                    {
                        if (timeValidationMethod == TIME_VALIDATION_METHOD.UseInternetTime)
                        {
                            throw;
                            // activationStatus = LICENSE_STATUS.Expired;
                            // return false;
                        }

                        now = DateTime.UtcNow.ToLocalTime();
                    }
                }
                else
                    now = DateTime.UtcNow.ToLocalTime();

                strExpirationDate = licenseExpirationDate.ToShortDateString();
                if (DateTime.Compare(licenseExpirationDate, now) < 0)
                {
                    bRval = false;
                }
            }

            string strSubject = "Check Activation Key State: " + strState;
            string strActMsg = "State: " + strState + " LicenseKey: " + strLicenseKey + " HardWareID: " + strHardwareId + " Activation Key:" + strActivationKey;
            string strLMsg = "ExpirationDate: " + strExpirationDate;
            string strHWMsg = "";
            EventLog.Log(strSubject, strActMsg, EventType.Information, "Activation", strLMsg, strHWMsg, null, FindLicensekey.OrderItem.Order.Company);
            */
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrators")]
        public ActionResult RemoveAllExpirationActivation()
        {
            string strDefKey = "AAAAA-AAAAA-AAAAA-AAAAA-AAAAA";
            string strDefName = "AAAAA";
            string strActKey = "Remove ActivationKey: ";
            string strHWID = "Remove HardwareID: ";
            string strLKey = "Remove LicenseKey Key: ";
            string strCompanyName = "Remove Company Name: ";
            int RemoveCnt = 0;
            DateTime CurTime = DateTime.UtcNow.ToLocalTime();

            Dictionary<int, LicenseKey> LicenseKeyDic = new Dictionary<int, LicenseKey>();
            List<Activation> ActList = db.Activations.Where(a => a.ExpirationDate < CurTime).ToList();
            foreach (Activation Act in ActList)
            {
                LicenseKey LKey = Act.LicenseKey;
                if (LKey != null && LKey.UpdateActivation == false)
                    continue;

                if (LKey == null)
                    strLKey += strDefKey + "/ ";
                else
                    strLKey += Act.LicenseKey.Key + "/ ";

                strHWID += Act.HardwareId + "/ ";
                strActKey += Act.ActivationKey + "/ ";

                if (LKey == null || LKey.OrderItem == null || LKey.OrderItem.Order == null)
                    strCompanyName += strDefName + "/ ";
                else
                    strCompanyName += LKey.OrderItem.Order.Company + "/ ";

                db.Activations.Remove(Act);
                RemoveCnt++;
                if(RemoveCnt == 50)
                {
                    EventLog.Log("Remove All Expiration Activation Keys", strLKey, EventType.Information, "Remove All Expiration Activation Keys", strActKey, strHWID + strCompanyName, null);
                    RemoveCnt = 0;
                    
                    strActKey = "Remove ActivationKey: ";
                    strHWID = "Remove HardwareID: ";
                    strLKey = "Remove LicenseKey Key: ";
                    strCompanyName = "Remove Company Name: ";
                }

                LicenseKey tmpKey;
                if (LKey != null)
                {
                    if (LicenseKeyDic.TryGetValue(LKey.Id, out tmpKey) == false)
                        LicenseKeyDic.Add(LKey.Id, LKey);
                }
            }

            if (RemoveCnt > 0)
            {
                db.SaveChanges();
                EventLog.Log("Remove All Expiration Activation Keys", strLKey, EventType.Information, "Remove All Expiration Activation Keys", strActKey, strHWID + strCompanyName, null);
            }


            foreach(var keyData in LicenseKeyDic)
            {
                LicenseKey ChangLKey = keyData.Value;
                ChangLKey.ActivationCount = DBHelper.GetActCnt(db, ChangLKey) + DBHelper.GetTryActCnt(db, ChangLKey);
                DBHelper.CalcUserNamesActCnt(db, ChangLKey.UserNameId);
                db.Entry(ChangLKey).State = EntityState.Modified;
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
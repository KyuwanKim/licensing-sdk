﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Licensing.Web.Models;
using Licensing.Web.DAL;
using PagedList;
using LicensingService;
using LicensingService.Filters;
using SoftActivate.Licensing;

namespace Licensing.ControlPanel.Controllers
{
    public class TrialActivationController : ControlPanelController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();
        private GenericRepository<TrialActivation> trialActivationsRepository;
        private GenericRepository<Product> productsRepository;

        public TrialActivationController()
        {
            trialActivationsRepository = new GenericRepository<TrialActivation>(db);
            productsRepository = new GenericRepository<Product>(db);
        }

        //
        // GET: /TrialActivation/

        public ActionResult Index(DateTime? startDate, DateTime? endDate, int? selectedProduct, int? page, string CompanyName, string LicenseKey, string HardwareID)
        {
            DateTime InitDate = new DateTime(2015, 1, 1);
            var products = productsRepository.Get(orderBy: q => q.OrderBy(d => d.Name));

            ViewBag.SelectedProduct = new SelectList(products, "Id", "Name", selectedProduct);
            ViewBag.StartDate = (startDate == null) ? InitDate : startDate;
            ViewBag.EndDate = (endDate == null) ? DateTime.UtcNow.ToLocalTime() : endDate;
            ViewBag.CompanyName = (CompanyName == null) ? "" : CompanyName;
            ViewBag.LicenseKey = (LicenseKey == null) ? "" : LicenseKey;
            ViewBag.HardwareID = (HardwareID == null) ? "" : HardwareID;
            if (CompanyName == "") CompanyName = null;
            if (LicenseKey == "") LicenseKey = null;
            if (HardwareID == "") HardwareID = null;


            int productId = selectedProduct.GetValueOrDefault();

            if (endDate.HasValue)
                endDate = endDate.Value.AddDays(1);

            var trailactivations = trialActivationsRepository.GetQuery(
                filter: s => (!selectedProduct.HasValue || s.LicenseKey.ProductId == productId)
                             && (startDate == null || s.ActivationDate >= startDate)
                             && (endDate == null || s.ActivationDate < endDate)
                             && (CompanyName == null || s.LicenseKey.OrderItem.Order.Company.Contains(CompanyName))
                             && (LicenseKey == null || s.LicenseKey.Key.Contains(LicenseKey))
                             && (HardwareID == null || s.HardwareId.Contains(HardwareID)),

                orderBy: q => q.OrderByDescending(s => s.ActivationDate),
                includeProperties: "LicenseKey");

            int pageNumber = (Request.HttpMethod == "POST") ? 1 : (page ?? 1);

            return View(trailactivations.ToPagedList(pageNumber, 20));
        }

        //
        // GET: /TrialActivation/Details/5

        public ActionResult Details(int id = 0)
        {
            TrialActivation trialactivation = db.TrialActivations.Find(id);
            if (trialactivation == null)
            {
                return HttpNotFound();
            }

            ViewBag.IsReadOnly = true;

            return View("TrialActivationForm", trialactivation);
        }

        //
        // GET: /TrialActivation/Create

        [Authorize(Roles = "Administrators")]
        public ActionResult Create()
        {
            return View("TrialActivationForm");
        }

        //
        // POST: /TrialActivation/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create([Bind(Exclude = "Id")]TrialActivation trialactivation)
        {
            if (ModelState.IsValid)
            {
                db.TrialActivations.Add(trialactivation);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(trialactivation);
        }

        //
        // GET: /TrialActivation/Edit/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id = 0)
        {
            TrialActivation trialactivation = db.TrialActivations.Find(id);
            if (trialactivation == null)
            {
                return HttpNotFound();
            }

            return View("TrialActivationForm", trialactivation);
        }

        //
        // POST: /TrialActivation/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(TrialActivation trialactivation)
        {
            if (ModelState.IsValid)
            {
                int userNameId = trialactivation.LicenseKey.UserNameId;
                trialactivation.LicenseKey = null;

                db.Entry(trialactivation).State = EntityState.Modified;
                db.SaveChanges();

                DBHelper.CalcUserNamesActCnt(db, userNameId);

                return RedirectToAction("Index");
            }

            return View(trialactivation);
        }

        //
        // POST: /TrialActivation/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrators")]
        public ActionResult DeleteConfirmed(int id)
        {
            TrialActivation trialactivation = db.TrialActivations.Find(id);
            LicenseKey licensekey = db.LicenseKeys.Find(trialactivation.LicenseKeyId);
            string strLicenseKey = "";
            string strCompanyName = "";
            string strActKey = " Trial Activation Key:" + trialactivation.ActivationKey;
            string strHWID = " HWID:" + trialactivation.HardwareId;

            if (licensekey != null)
            {
                strLicenseKey = "LicenseKey Key:" + licensekey.Key;
                strCompanyName = licensekey.OrderItem.Order.Company;
            }

            EventLog.Log("Delete TrialActivation Key", strLicenseKey, EventType.Information, "TrialActivation", strActKey, strHWID, null, strCompanyName);

            db.TrialActivations.Remove(trialactivation);
            db.SaveChanges();

            if (licensekey != null)
            {
                licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);
                db.Entry(licensekey).State = EntityState.Modified;
                db.SaveChanges();
                DBHelper.CalcUserNamesActCnt(db, licensekey.UserNameId);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using Licensing.Web.DAL;
using Licensing.Web.Models;
using LicensingService.Controllers;
using SoftActivate.Licensing;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;
using System.Data.Entity;
using Licensing.ControlPanel.Controllers;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace LicensingService
{
    public class DBHelper
    {
        public static void CalcUserNamesActCnt(LicensingServiceDbContext db, int userNameid)
        {
            UserName userName = db.UserNames.Find(userNameid);
            if (userName != null)
            {
                if (userName.LicenseKey.UpdateActivation == true)
                {
                    List<Activation> ActList;
                    ActList = db.Activations.Where(a => a.LicenseKeyId == userName.LicenseKeyId).ToList();

                    string[] split = userName.Names.Split(new char[] { '|', ',', '/' });

                    string strNameActCnt = "";

                    foreach (string s in split)
                    {
                        if (s.Length == 0) continue;
                        //                     int nCnt = ActList.Where(a => a.Name == s).Count();
                        //                     string strParm = string.Format("{{0,-{0}}}|", System.Text.Encoding.Default.GetBytes(s).Length);
                        //                     string strint = string.Format(strParm, nCnt);
                        //                     strNameActCnt += strint;

                        int nCnt = 0;

                        List<Activation> FindActList = ActList.Where(a => a.Name == s).ToList();
                        foreach (Activation FindAct in FindActList)
                            nCnt++;

                        List<UserInfo> UserInfoList = db.UserInfoes.Where(a => a.LicenseKeyId == userName.LicenseKeyId && a.Name == s).ToList();
                        foreach (UserInfo u in UserInfoList)
                            u.ActivationCnt = nCnt;

                        string strParm = string.Format("{{0,-{0}}}|", System.Text.Encoding.Default.GetBytes(s).Length);
                        string strint = string.Format(strParm, nCnt);
                        strNameActCnt += strint;
                    }
                    if (strNameActCnt.Length > 0)
                        userName.NamesActivationCnt = strNameActCnt.Substring(0, strNameActCnt.Length - 1);
                    db.Entry(userName).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    List<TrialActivation> ActList;
                    ActList = db.TrialActivations.Where(a => a.LicenseKeyId == userName.LicenseKeyId).ToList();

                    string[] split = userName.Names.Split(new char[] { '|', ',', '/' });

                    string strNameActCnt = "";

                    foreach (string s in split)
                    {
                        if (s.Length == 0) continue;
                        //                     int nCnt = ActList.Where(a => a.Name == s).Count();
                        //                     string strParm = string.Format("{{0,-{0}}}|", System.Text.Encoding.Default.GetBytes(s).Length);
                        //                     string strint = string.Format(strParm, nCnt);
                        //                     strNameActCnt += strint;

                        int nCnt = ActList.Where(a => a.Name == s).Count();
                        List<UserInfo> UserInfoList;
                        UserInfoList = db.UserInfoes.Where(a => a.LicenseKeyId == userName.LicenseKeyId && a.Name == s).ToList();
                        foreach (UserInfo u in UserInfoList)
                            nCnt += u.ActivationCnt;

                        string strParm = string.Format("{{0,-{0}}}|", System.Text.Encoding.Default.GetBytes(s).Length);
                        string strint = string.Format(strParm, nCnt);
                        strNameActCnt += strint;
                    }
                    if (strNameActCnt.Length > 0)
                        userName.NamesActivationCnt = strNameActCnt.Substring(0, strNameActCnt.Length - 1);
                    db.Entry(userName).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public static void CalcUserNamesNames(LicensingServiceDbContext db, int userNameid)
        {
            UserName userName = db.UserNames.Find(userNameid);
            if (userName != null)
            {
                List<UserInfo> UserInfoList;
                UserInfoList = db.UserInfoes.Where(a => a.LicenseKeyId == userName.LicenseKeyId).ToList();

                string[] split = userName.Names.Split(new char[] { '|', ',', '/' });

                string strNames = "";

                foreach (UserInfo u in UserInfoList)
                {
                    if (u.Name == null || u.Name.Length == 0) continue;
                    if (strNames.Length == 0)
                        strNames = u.Name;
                    else
                        strNames += "|" + u.Name;
                }
                userName.Names = strNames;
                db.Entry(userName).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static void SetUserInfo_ActCnt(LicensingServiceDbContext db, int licenseKeyId, string userName, int actCnt)
        {
            List<UserInfo> userInfoList = db.UserInfoes.Where(a => a.LicenseKeyId == licenseKeyId && a.Name == userName).ToList();
            foreach (UserInfo u in userInfoList)
            {
                u.ActivationCnt = actCnt;
                db.Entry(u).State = EntityState.Modified;
            }
            db.SaveChanges();
        }

        public static int GetActCnt(LicensingServiceDbContext db, LicenseKey licenseKey)
        {
            return db.Activations.Where(a => a.LicenseKeyId == licenseKey.Id).Count();
        }

        public static int GetTryActCnt(LicensingServiceDbContext db, LicenseKey licenseKey)
        {
            if (licenseKey.UpdateActivation == true)
                return 0;
            DateTime CurTime = new DateTime(DateTime.UtcNow.ToLocalTime().Year, DateTime.UtcNow.ToLocalTime().Month, DateTime.UtcNow.ToLocalTime().Day);
            DateTime CheckTime = CurTime;
            return db.TrialActivations.Where(a => a.LicenseKeyId == licenseKey.Id && a.ExpirationDate >= CheckTime).Count();
        }
    }

    public class ActivationController : LicensingServiceController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();

        private void ClearExpActConfirmed(LicenseKey licensekey)
        {
            // EXPIRED Date를 보정하기 위해 변경함. 사용자에게 보여주는 날짜 까지 사용 하도록 변경
            //DateTime CurTime = DateTime.UtcNow.ToLocalTime();
            DateTime CurTime = new DateTime(DateTime.UtcNow.ToLocalTime().Year, DateTime.UtcNow.ToLocalTime().Month, DateTime.UtcNow.ToLocalTime().Day);
            if (licensekey.UpdateActivation == false || licensekey.LicenseDuration < CurTime)
                return;

            string strCompanyName = licensekey.OrderItem.Order.Company;
            string strActKey = "Remove ActivationKey: ";
            string strHWID = "Remove HardwareID: ";
            int RemoveCnt = 0;
            
            List<Activation> ActList = db.Activations.Where(a => a.LicenseKeyId == licensekey.Id).ToList();
            foreach (Activation actvation in ActList)
            {
                DateTime ExpirationDate = new DateTime(actvation.ExpirationDate.Value.Year, actvation.ExpirationDate.Value.Month, actvation.ExpirationDate.Value.Day);
                // EXPIRED Date를 보정하기 위해 변경함. 사용자에게 보여주는 날짜 까지 사용 하도록 변경
                if (ExpirationDate < CurTime)
                //if (ExpirationDate <= CurTime) // Old 버전을 위해서 삭제한다.
                {
                    DBHelper.SetUserInfo_ActCnt(db, licensekey.Id, actvation.Name, 0);
                    strActKey += actvation.ActivationKey + "/ ";
                    strHWID += actvation.HardwareId + "/ ";
                    db.Activations.Remove(actvation);
                    RemoveCnt++;
                }
            }

            if (RemoveCnt > 0)
            {
                //licensekey.Key
                db.Entry(licensekey).State = EntityState.Modified;
                db.SaveChanges();

                licensekey.ActivationCount = DBHelper.GetActCnt(db, licensekey) + DBHelper.GetTryActCnt(db, licensekey);
                db.Entry(licensekey).State = EntityState.Modified;
                db.SaveChanges();

                UserName userName = db.UserNames.Find(licensekey.UserNameId);
                if (userName != null)
                    DBHelper.CalcUserNamesActCnt(db, userName.Id);

                string msg = "LicenseKey Key:" + licensekey.Key + " ActivationCount:" + licensekey.ActivationCount + " RemoveCnt: " + RemoveCnt;
                EventLog.Log("Clear Expiration Activation Keys By HttpMsg", msg, EventType.Information, "Clear Expiration Activation", strActKey, strHWID, null, strCompanyName);
            }
        }

        public bool GetUserInfo_IsHold(LicenseKey licenseKey, string userName = null)
        {
            if (userName != null && userName.Length == 0 || 
                licenseKey.CheckUserName == false)
                return false;

            List<UserInfo> userInfoes = db.UserInfoes.Where(a => a.LicenseKeyId == licenseKey.Id && a.Name == userName).ToList();
            DateTime CurTime = new DateTime(DateTime.UtcNow.ToLocalTime().Year, DateTime.UtcNow.ToLocalTime().Month, DateTime.UtcNow.ToLocalTime().Day);
            foreach (UserInfo u in userInfoes)
            {
                if (u.HoldDate != null && u.HoldDate < CurTime)
                {
                    u.IsHold = false;
                    db.Entry(u).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return u.IsHold;
            }

            return false;
        }

        public int GetUserInfo_HoldCnt(LicenseKey licenseKey, string userName = null)
        {
            if (userName != null && userName.Length == 0 ||
                licenseKey.CheckUserName == false)
                return 0;

            List<UserInfo> userInfoes = db.UserInfoes.Where(a => a.LicenseKeyId == licenseKey.Id && a.Name == userName).ToList();

            foreach (UserInfo u in userInfoes)
                return u.HoldCnt;
            
            return 0;
        }

        // GET api/<controller>
        public HttpResponseMessage Get(string licenseKey, string hardwareId, string licenseKeyValidationData = null, int productId = -1, string userName = null)
        {
            if (userName != null && userName.Length == 0)
                userName = null;

            // a license key and a hardware id must be provided in order to generate an activation key
            if (string.IsNullOrEmpty(hardwareId))
            {
                Abort(HttpStatusCode.BadRequest, "Invalid activation query (1)", "Null or empty hardware id provided");
            }

            // a license key and a hardware id must be provided in order to generate an activation key
            if (string.IsNullOrEmpty(licenseKey))
            {
                Abort(HttpStatusCode.BadRequest, "Invalid activation query (2)", "Null or empty license key provided");
            }

            if (!string.IsNullOrEmpty(licenseKeyValidationData))
            {
                licenseKeyValidationData = Uri.UnescapeDataString(licenseKeyValidationData);
            }

            byte[] rawLicenseKeyValidationData = null;

            if (!String.IsNullOrEmpty(licenseKeyValidationData))
            {
                rawLicenseKeyValidationData = Convert.FromBase64String(licenseKeyValidationData);
            }

            if (productId == 0)
            {
                productId = -1;
            }

            DateTime minActivationDate = new DateTime(1970, 1, 1);
            int maxActivations = 1;//Int16.Parse(ConfigurationManager.AppSettings["MaximumActivationsPerLicenseKey"]);
            int maxUniqueHardwareIds = 0;
            int maxActivationsPerHardwareId = 0;
            int expirationDays = 0;//short.Parse(ConfigurationManager.AppSettings["ActivationExpirationDays"]);
            string xmlLicenseKeyTemplate = ConfigurationManager.AppSettings["LicenseKeyTemplate"];

            TrialActivation LastTrialActivation = null;
            Activation LastActivation = null;

            Product product = db.Products.Find(productId);
            if (product != null)
            {
                if (product.DefaultMaxUniqueHardwareIdsPerLicenseKey != null)
                    maxUniqueHardwareIds = product.DefaultMaxUniqueHardwareIdsPerLicenseKey.Value;

                if (product.DefaultMaxActivationsPerHardwareId != null)
                    maxActivationsPerHardwareId = product.DefaultMaxActivationsPerHardwareId.Value;

                if (product.DefaultMaxActivationsPerLicenseKey != null)
                    maxActivations = product.DefaultMaxActivationsPerLicenseKey.Value;

                if (product.DefaultLicenseDuration != null)
                    expirationDays = product.DefaultLicenseDuration.Value;

                if (product.LicenseKeyTemplate != null)
                    xmlLicenseKeyTemplate = product.LicenseKeyTemplate;
            }

            DateTime firstActivationDate = default(DateTime);

            var licenseKeyRecord = db.LicenseKeys.Where(info => info.Key == licenseKey).FirstOrDefault();
            if(licenseKeyRecord == null)
                Abort(HttpStatusCode.BadRequest, "Using an unregistered License Key", "Using an unregistered License Key", "unregistered License");

            string strCompanyName = licenseKeyRecord.OrderItem.Order.Company;

            if (licenseKeyRecord != null)
            {
                ClearExpActConfirmed(licenseKeyRecord);

                if (!licenseKeyRecord.Active)
                    Abort(HttpStatusCode.Forbidden, "invalid license key", "license key is blocked", strCompanyName);

                if (product == null)
                    product = licenseKeyRecord.Product;

                if (product != null)
                    if (product.LicenseKeyTemplate != null)
                        xmlLicenseKeyTemplate = product.LicenseKeyTemplate;

                if (licenseKeyRecord.PerDeviceActivationLimit != null)
                    maxActivationsPerHardwareId = licenseKeyRecord.PerDeviceActivationLimit.Value;

                if (licenseKeyRecord.DeviceLimit != null)
                    maxUniqueHardwareIds = licenseKeyRecord.DeviceLimit.Value;

                if (licenseKeyRecord.ActivationLimit != null)
                    maxActivations = licenseKeyRecord.ActivationLimit.Value;

                if (licenseKeyRecord.LicenseDuration != null)
                {
                    TimeSpan RTime = licenseKeyRecord.LicenseDuration.Value - DateTime.UtcNow.ToLocalTime();
                    expirationDays = (int)Math.Ceiling(RTime.TotalDays);
                    //expirationDays = licenseKeyRecord.LicenseDuration.Value;
                }

                if (licenseKeyRecord.LicenseHistoryDuration != null)
                    minActivationDate = DateTime.UtcNow.ToLocalTime().AddDays(0 - licenseKeyRecord.LicenseHistoryDuration.Value);

                if (licenseKeyRecord.UpdateActivation)
                {
                    Activation firstActivation = db.Activations.Where(a => a.LicenseKeyId == licenseKeyRecord.Id && a.HardwareId == hardwareId).OrderBy(a => a.ActivationDate).FirstOrDefault();
                    if (firstActivation != null)
                        firstActivationDate = firstActivation.ActivationDate;
                    LastActivation = db.Activations.Where(a => a.LicenseKeyId == licenseKeyRecord.Id && a.HardwareId == hardwareId).OrderByDescending(a => a.ActivationDate).FirstOrDefault();
                }
                else
                {
                    TrialActivation TrialActivation = db.TrialActivations.Where(a => a.LicenseKeyId == licenseKeyRecord.Id && a.HardwareId == hardwareId).OrderBy(a => a.ActivationDate).FirstOrDefault();
                    if (TrialActivation != null)
                        firstActivationDate = TrialActivation.ActivationDate;

                    LastTrialActivation = db.TrialActivations.Where(a => a.LicenseKeyId == licenseKeyRecord.Id && a.HardwareId == hardwareId).OrderByDescending(a => a.ActivationDate).FirstOrDefault();
                }
                
            }

            int InActCnt = db.InActivations.Where(a => a.CheckAdmin == true && a.LicenseKey.Key == licenseKey && a.HardwareId == hardwareId).Count();
            if(InActCnt > 0)
                Abort(HttpStatusCode.Forbidden, "Used InActivation HardwareID By Admin", "Used InActivation HardwareID By Admin", strCompanyName);

            if (product == null)
                Abort(HttpStatusCode.BadRequest, "invalid activation query (3)", "invalid product id", strCompanyName);

            if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SDKLicenseKey"]))
                SDKRegistration.SetLicenseKey(ConfigurationManager.AppSettings["SDKLicenseKey"]);

            LicenseTemplate keyTemplate = new LicenseTemplate(xmlLicenseKeyTemplate);

            if (keyTemplate.DataSize < 16)
            {
                Abort(HttpStatusCode.InternalServerError, "Licensing server internal error (1)", "The data size of the license key template must be at least 16 bits in order to use activation", strCompanyName);
            }

            if (!IsLicenseKeyValid(keyTemplate, licenseKey, rawLicenseKeyValidationData))
            {
                Abort(HttpStatusCode.Forbidden, "Invalid license key", "Invalid license key", strCompanyName);
            }

            LicenseTemplate activationKeyTemplate = new LicenseTemplate(keyTemplate.NumberOfGroups,
                                                                keyTemplate.CharactersPerGroup,
                                                                keyTemplate.GetPublicKeyCertificate(),
                                                                keyTemplate.GetPrivateKey(),
                                                                keyTemplate.SignatureSize,
                                                                keyTemplate.DataSize,
                                                                "ExpirationDate");

            activationKeyTemplate.ValidationDataSize = (licenseKey.Length + hardwareId.Length) * 8;

            activationKeyTemplate.AddValidationField("LicenseKey", LicenseKeyFieldType.String, 8 * licenseKey.Length, 0);
            activationKeyTemplate.AddValidationField("HardwareId", LicenseKeyFieldType.String, 8 * hardwareId.Length, 8 * licenseKey.Length);

            if (licenseKeyValidationData != null)
            {
                activationKeyTemplate.ValidationDataSize += rawLicenseKeyValidationData.Length * 8;
                activationKeyTemplate.AddValidationField("LicenseKeyValidationData", LicenseKeyFieldType.Raw, rawLicenseKeyValidationData.Length * 8, 8 * (licenseKey.Length + hardwareId.Length));
            }

            KeyGenerator generator = new KeyGenerator(activationKeyTemplate);

            DateTime DurationDate = default(DateTime);
            DateTime expDate = default(DateTime);

            if(GetUserInfo_IsHold(licenseKeyRecord, userName))
                Abort(HttpStatusCode.Forbidden, "UserInfo States Is Hold.", "UserInfo States Is Hold UserName: " + userName, strCompanyName);


            int HoldCnt = GetUserInfo_HoldCnt(licenseKeyRecord, userName);

            //if (expirationDays > 0)
            if (licenseKeyRecord.LicenseDuration.Value != null)
            {
                // the license will expire in one year
                //expDate = (firstActivationDate != default(DateTime)) ? firstActivationDate.AddDays(expirationDays) : DateTime.UtcNow.ToLocalTime().AddDays(expirationDays);
                //DurationDate = licenseKeyRecord.LicenseDuration.Value;
                //expDate = DateTime.UtcNow.ToLocalTime().AddDays((double)(0 + licenseKeyRecord.UpdateDuration));
                DateTime CurTime = new DateTime(DateTime.UtcNow.ToLocalTime().Year, DateTime.UtcNow.ToLocalTime().Month, DateTime.UtcNow.ToLocalTime().Day);
                DurationDate = new DateTime(licenseKeyRecord.LicenseDuration.Value.Year, licenseKeyRecord.LicenseDuration.Value.Month, licenseKeyRecord.LicenseDuration.Value.Day);
                DurationDate = DurationDate.AddDays(HoldCnt);

                if (licenseKeyRecord.AutoInactivation == true)
                    expDate = CurTime;
                else
                    expDate = CurTime.AddDays((double)(0 + licenseKeyRecord.UpdateDuration));

                if (expDate > DurationDate)
                    expDate = DurationDate;

                // EXPIRED Date를 보정하기 위해 변경함. 사용자에게 보여주는 날짜 까지 사용 하도록 변경
                //if (expDate <= CurTime)
                if (expDate < CurTime)
                    Abort(HttpStatusCode.Forbidden, "Activation timeframe exceeded for this computer and license key", "Activation duration exceeded for this computer. First activation: " + firstActivationDate + ", expirationDays: " + expirationDays, strCompanyName);

                ushort expDateBits = (ushort)(((expDate.Year - 2000) << 9) | (expDate.Month << 5) | expDate.Day);
                byte[] rawExpDate = new byte[2] { (byte)(expDateBits >> 8), (byte)(expDateBits & 0xFF) };
                generator.SetKeyData("ExpirationDate", rawExpDate);
            }

            // 뷰어 버전인 경우 21.0.0.0 ~ 21.0.1.3는 사용 못하도록 처리 - start
            string orderVersion = null;
            if (licenseKeyRecord.OrderItem.Order.Version != null)
                orderVersion = licenseKeyRecord.OrderItem.Order.Version.ToUpper();

            if (licenseKeyRecord.ProductId / 100 == 2 && orderVersion == "VIEWER" && LastActivation?.BuildVersion != null)
            {
                string[] strUserVer = LastActivation.BuildVersion.Split('.');
                List<int> serverVerList = new List<int>() { 21, 0, 1, 3 };
                List<int> minServerVerList = new List<int>() { 21, 0, 0, 0 };
                int maxCnt = Math.Min(strUserVer.Count(), serverVerList.Count());
                if (maxCnt > 3) // 버전 3자리까지만 비교하도록 수정
                    maxCnt = 3;

                int containVersionCnt = 0;
                for (int i = 0; i < maxCnt; i++)
                {
                    int userVer = int.Parse(strUserVer[i]);
                    int serverVer = serverVerList[i];
                    int minServerVer = minServerVerList[i];

                    if (serverVer >= userVer && minServerVer <= userVer)
                        containVersionCnt++;
                }
                
                if(containVersionCnt == maxCnt)
                    Abort(HttpStatusCode.InternalServerError, "Activation failure. Please contact technical support", "Activation failure. Activation key validation failed.", strCompanyName);
            }
            // 뷰어 버전인 경우 21.0.1.3 이하는 사용 못하도록 처리 - end


            if (maxActivationsPerHardwareId > 0)
            {
                if (licenseKeyRecord.UpdateActivation)
                {
                    if (db.Activations.Count(a => a.LicenseKey.Key == licenseKey && a.HardwareId == hardwareId && a.ActivationDate > minActivationDate) >= maxActivationsPerHardwareId)
                    {
                        if (LastActivation == null)
                            Abort(HttpStatusCode.Forbidden, "Activation limit exceeded for this key", "Activation limit exceeded for this key (too many activations for the same hardware id)", strCompanyName);
                        else
                        {
                            if (licenseKeyRecord.UpdateActivation == false)
                                Abort(HttpStatusCode.Forbidden, "Update Activation Fail for this key(Used HardWareID)", "ActivationKey Update Fail. Used LicenseKey And HardWareID(" + LastActivation.HardwareId + ")", strCompanyName);
                            else
                            {
                                DateTime LastExpTime = expDate;
                                if (LastActivation.ExpirationDate != null)
                                    LastExpTime = new DateTime(LastActivation.ExpirationDate.Value.Year, LastActivation.ExpirationDate.Value.Month, LastActivation.ExpirationDate.Value.Day);

                                LastExpTime = LastExpTime.AddDays(1);

                                if (LastActivation.ExpirationDate != null && LastExpTime > DateTime.UtcNow.ToLocalTime())
                                {
                                    if (licenseKeyRecord.CheckUserName == true && userName == null)
                                        Abort(HttpStatusCode.Forbidden, "did not Find a user name.", "did not Find a user name(" + userName + ").", strCompanyName);

                                    if (LastActivation.Name == null || LastActivation.Name.Length == 0)
                                    {
                                        if (userName != null)
                                        {
                                            UserName userNameRec = null;
                                            userNameRec = db.UserNames.Find(licenseKeyRecord.UserNameId);
                                            long nUserNameType = CheckUserNameRecord(userNameRec, userName);
                                            if (nUserNameType == 1 || nUserNameType == 3)
                                                Abort(HttpStatusCode.Forbidden, "did not Find a user name.", "did not Find a user name(" + userName + ").", strCompanyName);
                                            else if (nUserNameType == 2)
                                                Abort(HttpStatusCode.Forbidden, "UserName limit exceeded for this key.", "UserName limit exceeded for this key(" + userName + ").", strCompanyName);

                                            LastActivation.Name = userName;
                                            db.Entry(LastActivation).State = EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }

                                    if (licenseKeyRecord.CheckUserName == true)
                                        DBHelper.CalcUserNamesActCnt(db, licenseKeyRecord.UserNameId);

                                    string strHttpMessage = "Activation limit exceeded for this key(ReSend Act Key)";
                                    string strLogMessage = "Activation limit exceeded for this key(Send Last Activation Key) \nExpirationDate:" + LastActivation.ExpirationDate.ToString();
                                    EventLog.Log(strHttpMessage, strLogMessage, EventType.Warning, "Activation", null, null, null, strCompanyName);
                                    return new HttpResponseMessage()
                                    {
                                        Content = new StringContent(LastActivation.ActivationKey, System.Text.Encoding.UTF8, "text/plain")
                                    };
                                }
                                else if (expDate > DateTime.UtcNow.ToLocalTime())
                                {
                                    string strHttpMessage = "Remove Activation Key";
                                    string strLogMessage = "Remove Activation Key \nExpirationDate:" + LastActivation.ExpirationDate.ToString() + "Act Key:" + LastActivation.ActivationKey;
                                    EventLog.Log(strHttpMessage, strLogMessage, EventType.Warning, "Activation", null, null, null, strCompanyName);
                                    db.Activations.Remove(LastActivation);
                                    db.SaveChanges();
                                    if (userName != null)
                                        DBHelper.SetUserInfo_ActCnt(db, licenseKeyRecord.Id, userName, 0);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (db.TrialActivations.Count(a => a.LicenseKey.Key == licenseKey && a.HardwareId == hardwareId && a.ActivationDate > minActivationDate) > 0)
                    {
                        if (licenseKeyRecord.CheckUserName == true && userName == null)
                            Abort(HttpStatusCode.Forbidden, "did not Find a user name.", "did not Find a user name(" + userName + ").", strCompanyName);

                        if (LastTrialActivation.Name != userName)
                            Abort(HttpStatusCode.Forbidden, "Using the trial version of the HardWareID key.", "Using the trial version of the HardWareID key.(HW: " + hardwareId + "/Name: " + userName + ").", strCompanyName);

                        if(LastTrialActivation != null)
                        {
                            return new HttpResponseMessage()
                            {
                                Content = new StringContent(LastTrialActivation.ActivationKey, System.Text.Encoding.UTF8, "text/plain")
                            };
                        }
                        else
                            Abort(HttpStatusCode.Forbidden, "Used Trail License Key.", "LicenseKey: " + licenseKey + "HardwareID: " + hardwareId, strCompanyName);
                    }
                }
                
            }

            if (maxActivations > 0)
            {
                bool IsExceededActivationKey = false;
                if (licenseKeyRecord.UpdateActivation == true)
                {
                    if (DBHelper.GetActCnt(db, licenseKeyRecord) >= maxActivations)
                        IsExceededActivationKey = true;
                }
                else
                {
                    if (DBHelper.GetTryActCnt(db, licenseKeyRecord) >= maxActivations)
                        IsExceededActivationKey = true;
                }

                if (IsExceededActivationKey)
                    Abort(HttpStatusCode.Forbidden, "Activation limit exceeded for this key", "Activation limit exceeded for this key (too many activations)", strCompanyName);
            }

            if (maxUniqueHardwareIds > 0)
            {
                bool IsExceededActivationKey = false;
                if (licenseKeyRecord.UpdateActivation == true)
                {
                    if (db.Activations.Where(a => a.LicenseKey.Key == licenseKey && a.ActivationDate > minActivationDate)
                                    .Select(a => a.HardwareId)
                                    .Distinct()
                                    .Count() >= maxUniqueHardwareIds)
                    {
                        IsExceededActivationKey = true;
                    }
                }
                else
                {
                    if (db.TrialActivations.Where(a => a.LicenseKey.Key == licenseKey && a.ActivationDate > minActivationDate)
                                    .Select(a => a.HardwareId)
                                    .Distinct()
                                    .Count() >= maxUniqueHardwareIds)
                    {
                        IsExceededActivationKey = true;
                    }
                }
                    if (IsExceededActivationKey)
                    Abort(HttpStatusCode.Forbidden, "Activation limit exceeded for this key", "Activation limit exceeded for this key (too many hardware ids)", strCompanyName);
            }

            generator.SetValidationData("LicenseKey", licenseKey);
            generator.SetValidationData("HardwareId", hardwareId);

            if (licenseKeyValidationData != null)
                generator.SetValidationData("LicenseKeyValidationData", rawLicenseKeyValidationData);

            string activationKey = generator.GenerateKey();

            // validate the generated key just to be sure
            KeyValidator validator = new KeyValidator(activationKeyTemplate, activationKey);

            validator.SetValidationData("LicenseKey", licenseKey);
            validator.SetValidationData("HardwareId", hardwareId);

            if (licenseKeyValidationData != null)
                validator.SetValidationData("LicenseKeyValidationData", rawLicenseKeyValidationData);

            if (!validator.IsKeyValid())
            {
                Abort(HttpStatusCode.InternalServerError, "Activation failure. Please contact technical support", "Activation failure. Activation key validation failed.", strCompanyName);
            }

            UserName userNameRecord = null;
            if (licenseKeyRecord.CheckUserName == true)
            {
                if (userName == null)
                    Abort(HttpStatusCode.Forbidden, "did not enter a user name.", "did not enter a user name.", strCompanyName);
                
                userNameRecord = db.UserNames.Find(licenseKeyRecord.UserNameId);
                if (userNameRecord == null)
                    Abort(HttpStatusCode.Forbidden, "did not Find a user name.", "did not Find a user name(" + userName + ").", strCompanyName);
                
                int nUserNameType = CheckUserNameRecord(userNameRecord, userName);

                if (nUserNameType == 1 || nUserNameType == 3)
                    Abort(HttpStatusCode.Forbidden, "did not Find a user name.", "did not Find a user name(" + userName + ").", strCompanyName);
                else if (nUserNameType == 2)
                    Abort(HttpStatusCode.Forbidden, "UserName limit exceeded for this key.", "UserName limit exceeded for this key.(" + userName + ").", strCompanyName);
            }

            string clientIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            if (clientIPAddress == null)
                clientIPAddress = "";

            // register the activation event into the database

            if (licenseKeyRecord == null)
            {
                licenseKeyRecord = new LicenseKey();
                licenseKeyRecord.Key = licenseKey;
                licenseKeyRecord.ValidationData = licenseKeyValidationData;
                licenseKeyRecord.ProductId = product.Id;
                licenseKeyRecord.DateAdded = DateTime.UtcNow.ToLocalTime();
                licenseKeyRecord.ActivationCount = 1;
                db.LicenseKeys.Add(licenseKeyRecord);
            }
            else
            {
                licenseKeyRecord.ActivationCount = (licenseKeyRecord.ActivationCount == null) ? 1 : licenseKeyRecord.ActivationCount + 1;
                licenseKeyRecord.LastActivationTime = DateTime.UtcNow.ToLocalTime();

                db.Entry(licenseKeyRecord).State = System.Data.Entity.EntityState.Modified;
            }

            db.SaveChanges();

            if(licenseKeyRecord.UpdateActivation == true)
            {
                Activation activationRecord = new Activation();
                activationRecord.ActivationKey = activationKey;
                activationRecord.LicenseKeyId = licenseKeyRecord.Id;
                activationRecord.HardwareId = hardwareId;
                activationRecord.ExpirationDate = expDate;
                activationRecord.ActivationDate = DateTime.UtcNow.ToLocalTime();
                activationRecord.IPAddress = clientIPAddress;
                if (userName != null)
                {
                    activationRecord.Name = userName;
                    DBHelper.SetUserInfo_ActCnt(db, licenseKeyRecord.Id, userName, 1);
                }

                db.Activations.Add(activationRecord);
            }
            else
            {
                TrialActivation activationRecord = new TrialActivation();
                activationRecord.ActivationKey = activationKey;
                activationRecord.LicenseKeyId = licenseKeyRecord.Id;
                activationRecord.HardwareId = hardwareId;
                activationRecord.ExpirationDate = expDate;
                activationRecord.ActivationDate = DateTime.UtcNow.ToLocalTime();
                activationRecord.IPAddress = clientIPAddress;
                if (userName != null)
                {
                    activationRecord.Name = userName;
                    DBHelper.SetUserInfo_ActCnt(db, licenseKeyRecord.Id, userName, 1);
                }

                db.TrialActivations.Add(activationRecord);
            }
            db.SaveChanges();

            if (licenseKeyRecord.CheckUserName == true && userNameRecord != null)
                DBHelper.CalcUserNamesActCnt(db, userNameRecord.Id);

            string httpMessage = "Successful Activation key";
            string logMessage = "Activation key =" + activationKey;

            EventLog.Log(httpMessage, logMessage, EventType.Information, "Activation", null, null, null, strCompanyName);

            return new HttpResponseMessage()
            {
                Content = new StringContent(activationKey, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {

        }

        /*
                public HttpResponseMessage Delete(string activationKey)
                {
                    Activation activation = db.Activations.Where(a => a.ActivationKey == activationKey).FirstOrDefault();

                    if (activation == null)
                    {
                        return new HttpResponseMessage(HttpStatusCode.Forbidden);
                    }

                    if (activation.IsActive == false)
                        return new HttpResponseMessage(HttpStatusCode.Forbidden);

                    if (activation.LicenseKey.ActivationCount > 0)
                        activation.LicenseKey.ActivationCount--;

                    activation.IsActive = false;
                    activation.ExpirationDate = DateTime.UtcNowUtcNowToLocalTime();

                    db.Entry(activation).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
        */

        private bool IsLicenseKeyValid(LicenseTemplate template, string licenseKey, byte[] licenseKeyValidationData)
        {
            KeyValidator validator = new KeyValidator(template, licenseKey);

            if (licenseKeyValidationData != null)
                validator.SetValidationData(null, licenseKeyValidationData);

            return validator.IsKeyValid();
        }

        void Abort(HttpStatusCode httpCode, string httpMessage, string logMessage, string strLocName = null)
        {
            MvcApplication.LogUrl("[httpMessage]:" + httpMessage + " [logMessage]:" + logMessage + " [strLocName]:" + strLocName);

            EventLog.Log(httpMessage, logMessage, EventType.Error, "Activation", null, null, null, strLocName);

            HttpResponseMessage response = new HttpResponseMessage(httpCode);
            response.ReasonPhrase = httpMessage;

            throw new HttpResponseException(response);
        }


        public HttpResponseMessage Get(string licenseKey, string hardwareId, string activationKey, string BuildVersion = null, int nType = 0)
        {
            // nType == null nType == 0
            // 클라이언트에서 인증 실패 이나 서버에서는 인증 성공인 경우
            string strKey = "licenseKey: " + licenseKey + " hardwareId: " + hardwareId + " activationKey: " + activationKey;
            string strInfo = "";

            string strState = "FALSE";
            if (nType == 0)
            {
                Activation activation = db.Activations.Where(a => a.ActivationKey == activationKey).FirstOrDefault();

                if (activation == null)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(strState, System.Text.Encoding.UTF8, "text/plain")
                    };
                }

                LicenseKey FindLicensekey = db.LicenseKeys.Find(activation.LicenseKeyId);
                Product product = db.Products.Find(FindLicensekey.ProductId);

                string strLicenseKey = FindLicensekey.Key;
                string strHardwareId = activation.HardwareId;
                string strActivationKey = activation.ActivationKey;

                if (strLicenseKey != licenseKey || strHardwareId != hardwareId)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(strState, System.Text.Encoding.UTF8, "text/plain")
                    };
                }
                strInfo = "Version Info: " + BuildVersion;
                EventLog.Log("Server by Activation", "Server by Activation", EventType.Information, "Activation", strKey, strInfo, null);

                strState = "OK";
            }
            else if(nType != 0) // 로그인 시간 체크
            {
                strInfo = "Version Info: " + BuildVersion;
                Activation activation = db.Activations.Where(a => a.ActivationKey == activationKey).FirstOrDefault();

                Product findProduct = null;
                if (activation != null)
                    findProduct = activation.LicenseKey.Product;
                else
                    findProduct = db.Products.Where(p => p.Id == nType).FirstOrDefault();

                if (activation == null)
                    EventLog.Log("LoginTime Check: Fail", "LoginTime Check: Fail", EventType.Information, "Activation", strKey, strInfo, null);

                if (activation != null)
                {
                    activation.LoginDate = DateTime.UtcNow.ToLocalTime();
                    activation.BuildVersion = BuildVersion;

                    db.Entry(activation).State = EntityState.Modified;
                    db.SaveChanges();
                }

                // 추후에 추가 클라이언트에 버전 체크 및 업데이트 다이얼로그 필요시...
                // strRecvArr
                // 0 : 성공 실패(TRUE / FALSE) 또는 인증서버에 등록된 프로젝트의 최신버전
                // 1 : DownLoadURL 정보
                // 2 : 인증자동회수(TRUE / FALSE)
                // 3 : ReleaseNoteURL 정보
                // 4 : NotificationURL 정보
                // 5 : ProcessMode 정보
                // 6 : 사용할수 있는 국가코드 // 20240726에 추가
                // 7 : OnlineManualURL 정보
                // 8 : GuideVideoURL 정보
                // 9 : ServiceDeskURL 정보
                //10 : DownLoadURL_JP 정보
                //11 : ReleaseNoteURL_JP 정보
                //12 : NotificationURL_JP 정보
                //13 : OnlineManualURL_JP 정보
                //14 : GuideVideoURL_JP 정보
                //15 : ServiceDeskURL_JP 정보


                if (findProduct != null)
                {
                    strState = findProduct.Version + "\n";

                    if (findProduct.DownLoadURL != null && findProduct.DownLoadURL.Length > 0)
                        strState += findProduct.DownLoadURL + "\n";
                    else
                        strState += "\n";

                    if (activation != null)
                        strState += activation.LicenseKey.AutoInactivation.ToString().ToUpper() + "\n";
                    else
                        strState += "FALSE" + "\n";

                    if (findProduct.ReleaseNoteURL != null && findProduct.ReleaseNoteURL.Length > 0)
                        strState += findProduct.ReleaseNoteURL + "\n";
                    else 
                        strState += "\n";

                    if (findProduct.NotificationURL != null && findProduct.NotificationURL.Length > 0)
                        strState += findProduct.NotificationURL + "\n";
                    else
                        strState += "\n";

                    if (activation.LicenseKey.ProcessMode != null && activation.LicenseKey.ProcessMode.Length > 0)
                    {
                        // 암호화를 해야 하나??
                        string strCompany = activation.LicenseKey.OrderItem.Order.Company;
                        if(strCompany.Count() >= 8)
                            strCompany = strCompany.Substring(0, 8);
                        else
                            strCompany = strCompany.PadRight(8, '0');

                        string strEncTxt = StringCipher.Encrypt(activation.LicenseKey.ProcessMode, strCompany);
                        strState += strEncTxt + "\n";
                        //strState += activation.LicenseKey.ProcessMode + "\n";
                    }
                    else
                        strState += "\n";

                    if (activation.LicenseKey.CountryCode != 0)
                    {
                        // 암호화를 해야 하나??
                        string strCompany = activation.LicenseKey.OrderItem.Order.Company;
                        if (strCompany.Count() >= 8)
                            strCompany = strCompany.Substring(0, 8);
                        else
                            strCompany = strCompany.PadRight(8, '0');

                        string strEncTxt = StringCipher.Encrypt(activation.LicenseKey.CountryCode.ToString(), strCompany);
                        strState += strEncTxt + "\n";
                        //strState += activation.LicenseKey.ProcessMode + "\n";
                    }
                    else
                        strState += "\n";

                    // 7 : OnlineManualURL 정보
                    if (findProduct.OnlineManualURL != null && findProduct.OnlineManualURL.Length > 0)
                        strState += findProduct.OnlineManualURL + "\n";
                    else
                        strState += "\n";
                    
                    // 8 : GuideVideoURL 정보
                    if (findProduct.GuideVideoURL != null && findProduct.GuideVideoURL.Length > 0)
                        strState += findProduct.GuideVideoURL + "\n";
                    else
                        strState += "\n";
                    
                    // 9 : ServiceDeskURL 정보
                    if (findProduct.ServiceDeskURL != null && findProduct.ServiceDeskURL.Length > 0)
                        strState += findProduct.ServiceDeskURL + "\n";
                    else
                        strState += "\n";
                    
                    //10 : DownLoadURL_JP 정보
                    if (findProduct.DownLoadURL_JP != null && findProduct.DownLoadURL_JP.Length > 0)
                        strState += findProduct.DownLoadURL_JP + "\n";
                    else
                        strState += "\n";

                    //11 : ReleaseNoteURL_JP 정보
                    if (findProduct.ReleaseNoteURL_JP != null && findProduct.ReleaseNoteURL_JP.Length > 0)
                        strState += findProduct.ReleaseNoteURL_JP + "\n";
                    else
                        strState += "\n";

                    //12 : NotificationURL_JP 정보
                    if (findProduct.NotificationURL_JP != null && findProduct.NotificationURL_JP.Length > 0)
                        strState += findProduct.NotificationURL_JP + "\n";
                    else
                        strState += "\n";

                    //13 : OnlineManualURL_JP 정보
                    if (findProduct.OnlineManualURL_JP != null && findProduct.OnlineManualURL_JP.Length > 0)
                        strState += findProduct.OnlineManualURL_JP + "\n";
                    else
                        strState += "\n";

                    //14 : GuideVideoURL_JP 정보
                    if (findProduct.GuideVideoURL_JP != null && findProduct.GuideVideoURL_JP.Length > 0)
                        strState += findProduct.GuideVideoURL_JP + "\n";
                    else
                        strState += "\n";

                    //15 : ServiceDeskURL_JP 정보
                    if (findProduct.ServiceDeskURL_JP != null && findProduct.ServiceDeskURL_JP.Length > 0)
                        strState += findProduct.ServiceDeskURL_JP + "\n";
                    else
                        strState += "\n";

                }

                //                 if (activation.LicenseKey != null && activation.LicenseKey.Product != null)
                //                 {
                //                      strState = activation.LicenseKey.Product.Version;
                //                      if (activation.LicenseKey.Product.DownLoadURL != null && activation.LicenseKey.Product.DownLoadURL.Length > 0)
                //                          strState += "\n" + activation.LicenseKey.Product.DownLoadURL;
                //                      strState += "\n" + activation.LicenseKey.AutoInactivation.ToString().ToUpper();
                //                 }

                // 로그가 너무 많이 쌓여 일시적으로 삭제함 20220624
                //EventLog.Log("Client Log Info", "Client Log Info", EventType.Information, "Activation", strKey, strInfo, null);
            }
//             else if (nType != null && nType == 200) // 클라이언트 로그 수집
//             {
//                 strInfo = "Info: " + BuildVersion;
//                 EventLog.Log("Client Log Info", "Client Log Info", EventType.Information, "Activation", strKey, strInfo, null);
//             }

            return new HttpResponseMessage()
            {
                Content = new StringContent(strState, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        // return 0 : 사용 가능
        // return 1 : 사용자가 등록되지 않음
        // return 2 : 사용자 갯수 초과
        // return 3 : 데이타 에러
        private int CheckUserNameRecord(UserName un, string checkName)
        {
            if (un == null || un.LicenseKey.CheckUserName == false)
                return 0;

//             checkName = checkName.Trim();
//             string[] splitNames = un.Names.Split(new char[] { '|', ',', '/' });
//             int idx = -1;
//             bool bFindName = false;
//             foreach (string s in splitNames)
//             {
//                 idx++;
//                 if (s.Length == 0) continue;
//                 if (s.Trim().CompareTo(checkName) == 0)
//                 {
//                     bFindName = true;
//                     break;
//                 }
//             }
//             if (bFindName == false)
//                 return 1;
// 
//             string[] splitActCnts = un.NamesActivationCnt.Split(new char[] { '|', ',', '/' });
//             if (splitActCnts.Length > idx)
//             {
//                 int Cnt = int.Parse(splitActCnts[idx]);
//                 if (Cnt > 1)
//                     return 2;
//                 else
//                     return 0;
//             }
//            return 3;

            List<UserInfo> userInfos = db.UserInfoes.Where(a => a.LicenseKeyId == un.LicenseKeyId && a.Name == checkName).ToList();
            if (userInfos.Count() == 0)
                return 1;
            else
            {
                int actCnt = 0;
                foreach (UserInfo userInfo in userInfos)
                    actCnt += userInfo.ActivationCnt;
                if (actCnt > 1)
                    return 2;
                else
                    return 0;
            }
        }

//         private void UpdateUserNamesActivationCnt(UserName userName)
//         {
//             List<Activation> ActList;
//             ActList = db.Activations.Where(a => a.LicenseKeyId == userName.LicenseKeyId).ToList();
// 
//             string[] split = userName.Names.Split(new char[] { '|', ',', '/' });
// 
//             string strNameActCnt = "";
// 
//             foreach (string s in split)
//             {
//                 if (s.Length == 0) continue;
//                 int nCnt = ActList.Where(a => a.Name == s).Count();
//                 strNameActCnt += nCnt.ToString() + "|";
//             }
//             userName.NamesActivationCnt = strNameActCnt.Substring(0, strNameActCnt.Length - 1);
//         }
    }


    public static class StringCipher
    {
        // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes("1234567812345678");
        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;
        public static string Encrypt(string plainText, string passPhrase)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        public static string Decrypt(string cipherText, string passPhrase)
        {
            try
            {
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
                using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
                {
                    byte[] keyBytes = password.GetBytes(keysize / 8);
                    using (RijndaelManaged symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.Mode = CipherMode.CBC;
                        using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                        {
                            using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                            {
                                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return "";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;

namespace LicensingService.Controllers {

    public class PayPalController : PaymentServiceController
    {
        public class OrderNotificationParameters
        {
            public string txn_type { get; set; }
            public string txn_id { get; set; }
            public string item_number { get; set; }
            public string payer_email { get; set; }
            public int quantity { get; set; }
            public decimal mc_gross { get; set; }
            public string mc_currency { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string payer_business_name { get; set; }
            public string contact_phone { get; set; }
            public string address_street { get; set; }
            public string address_zip { get; set; }
            public string address_city { get; set; }
            public string address_state { get; set; }
            public string residence_country { get; set; }
        }

        public void OrderNotification([FromBody] OrderNotificationParameters p)
        {
            if (p.txn_type != "web_accept")
                return;

            if (p.quantity == 0)
                p.quantity = 1;

            ProcessKeyGenerationRequest(p.txn_id, p.item_number, p.quantity, p.mc_gross, p.mc_currency, p.first_name, p.last_name, p.payer_business_name, p.payer_email, p.contact_phone, p.address_street, p.address_zip, p.address_city, p.address_state, p.residence_country, true);
        }

        bool ValidatePayPalRequest(OrderNotificationParameters p)
        {
            return true;
#if (false)
            //
            // Check with PayPal to see if this payment notification is legit
            //            

            //Post back to either sandbox or live
            //string strSandbox = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            string strLive = "https://www.paypal.com/cgi-bin/webscr";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strLive);

            //Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            byte[] param = context.Request.BinaryRead(HttpContext.Current.Request.ContentLength);
            string strRequest = System.Text.Encoding.ASCII.GetString(param);
            strRequest += "&cmd=_notify-validate";
            req.ContentLength = strRequest.Length;

            //for proxy
            //WebProxy proxy = new WebProxy(new Uri("http://url:port#"));
            //req.Proxy = proxy;

            StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            streamOut.Write(strRequest);
            streamOut.Close();
            StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
            string strResponse = streamIn.ReadToEnd();
            streamIn.Close();

            // uncomment the following comment when in production ! For testing purposes we don't check the result
            if ((strResponse != "VERIFIED") && (ConfigurationManager.AppSettings["Testing"] != "true"))
                throw new Exception("PayPal request authentication error");

            if (context.Request.Form["payment_status"] != "Completed")
            {
                context.Response.StatusCode = 200;
                return;
            }
#endif
        }
    }
}

﻿using Licensing.Web.DAL;
using Licensing.Web.Models;
using LicensingService.Controllers;
using SoftActivate.Licensing;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Cryptography;

namespace LicensingService
{
    public class InActivationController : LicensingServiceController
    {
        private LicensingServiceDbContext db = new LicensingServiceDbContext();

        // GET: /InActivation/
        public HttpResponseMessage Get(string licenseKey, string activationKey, string hardwareId, string companyName, string UserName)
        {
            string strResponseMsg = "FALSE";

            LicenseKey FindLicensekey = db.LicenseKeys.Where(a => a.Key == licenseKey).FirstOrDefault();
            if(FindLicensekey == null)
                return new HttpResponseMessage() { Content = new StringContent(strResponseMsg, System.Text.Encoding.UTF8, "text/plain") };

            string strVersion = FindLicensekey.OrderItem.Order.Version.ToUpper();
            if (FindLicensekey.UpdateActivation == false || strVersion == "SITE" || strVersion == "ACADEMY")
            {
                strResponseMsg = "Trial Key";
                return new HttpResponseMessage() { Content = new StringContent(strResponseMsg, System.Text.Encoding.UTF8, "text/plain") };
            }

            Activation FindAct = db.Activations.Where(a => a.ActivationKey == activationKey && a.LicenseKey.Key == licenseKey).FirstOrDefault();
            if (FindAct == null)
                return new HttpResponseMessage(){Content = new StringContent(strResponseMsg, System.Text.Encoding.UTF8, "text/plain")};

            InActivation FindExpAct = db.InActivations.Where(a => a.LicenseKeyId == FindLicensekey.Id && a.HardwareId == hardwareId && a.ActivationKey == activationKey).FirstOrDefault();
            if (FindExpAct != null)
                return new HttpResponseMessage() { Content = new StringContent(strResponseMsg, System.Text.Encoding.UTF8, "text/plain") };

            string strExpirationDate = FindAct.ExpirationDate.ToString();

            InActivation InActivation = new InActivation();
            InActivation.SetInActivation(FindAct, DateTime.UtcNow.ToLocalTime(), false);
            db.InActivations.Add(InActivation);
            db.Activations.Remove(FindAct);
            
            db.SaveChanges();

            FindLicensekey.ActivationCount = DBHelper.GetActCnt(db, FindLicensekey) + DBHelper.GetTryActCnt(db, FindLicensekey);
            db.Entry(FindLicensekey).State = EntityState.Modified;
            db.SaveChanges();

            DBHelper.SetUserInfo_ActCnt(db, FindLicensekey.Id, FindAct.Name, 0);
            DBHelper.CalcUserNamesActCnt(db, FindLicensekey.UserNameId);

            string strSubject = (FindAct.HardwareId == hardwareId) ? "InActivation Key UserName: " + UserName : "InActivation Key UserName(HWID Check): " + UserName;
            string strActMsg = "InActivation Key:" + activationKey + " ExpirationDate:" + strExpirationDate;
            string strLMsg = "LicenseKey Key: " + licenseKey + " ActKeyCnt: " + FindLicensekey.ActivationCount;
            string strHWMsg = (FindAct.HardwareId == hardwareId) ? "HardwareId Key: " + hardwareId : "HardwareId Key: " + FindAct.HardwareId+ "/" + hardwareId;
            EventLog.Log(strSubject, strActMsg, EventType.Information, "Activation", strLMsg, strHWMsg, null, companyName);

            strResponseMsg = "OK";
            return new HttpResponseMessage()
            {
                Content = new StringContent(strResponseMsg, System.Text.Encoding.UTF8, "text/plain")
            };
        }
        
        public HttpResponseMessage Get(string hardwareId)
        {
            string strResponseMessage = "";
            if (hardwareId == null)
            {
                foreach (var ExpAct in db.InActivations)
                {
                    if (ExpAct.LicenseKey == null)
                        continue;
                    strResponseMessage += ExpAct.LicenseKey.Key + " ";
                    strResponseMessage += ExpAct.HardwareId + " ";
                    strResponseMessage += ExpAct.ActivationKey + "\n";
                }
            }
            else
            {
                foreach (var ExpAct in db.InActivations.Where(a => a.HardwareId == hardwareId))
                {
                    if (ExpAct.LicenseKey == null)
                        continue;
                    strResponseMessage += ExpAct.LicenseKey.Key + " ";
                    strResponseMessage += ExpAct.HardwareId + " ";
                    strResponseMessage += ExpAct.ActivationKey + "\n";
                }
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(strResponseMessage, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        public HttpResponseMessage Get(string licenseKey, int productID, string UserName)
        {
            string strResponseMessage = "";

            var licenseKeyRecord = db.LicenseKeys.Where(info => info.Key == licenseKey && info.ProductId == productID).FirstOrDefault();

            int ActCount = 0;
            int ActLimit = 0;
            if(licenseKeyRecord.ActivationCount != null)
                ActCount = licenseKeyRecord.ActivationCount.Value;
            if(licenseKeyRecord.ActivationLimit != null)
                ActLimit = licenseKeyRecord.ActivationLimit.Value;

            strResponseMessage = ActCount.ToString() + "/" + ActLimit.ToString();

            return new HttpResponseMessage()
            {
                Content = new StringContent(strResponseMessage, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        public HttpResponseMessage Get(string type, string licenseKey, int productID, string CompanyName)
        {
            string strResponseMessage = "FAIL";

            if (type == "LicenseKeyInfo")
            {
                var licenseKeyRecord = db.LicenseKeys.Where(info => info.Key == licenseKey && info.ProductId == productID && info.OrderItem.Order.Company == CompanyName).FirstOrDefault();
                if (licenseKeyRecord != null)
                {
                    LicenseManagementInfo LicenseInfo = new LicenseManagementInfo(licenseKeyRecord, db);
                    strResponseMessage = LicenseManagementInfo.GetJSonString(LicenseInfo);
                }
            }
            else if (type == "UsedManagement")
            {
                var licenseKeyRecord = db.LicenseKeys.Where(info => info.Key == licenseKey && info.ProductId == productID && info.OrderItem.Order.Company == CompanyName).FirstOrDefault();
                if (licenseKeyRecord != null)
                {
                    if(licenseKeyRecord.Password.Length != 0)
                        strResponseMessage = "TRUE";
                }

            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(strResponseMessage, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        public HttpResponseMessage Get(string licenseKey, int productID, string companyName, string hvalue)
        {
            string strResponseMessage = "FAIL";

            var licenseKeyRecord = db.LicenseKeys.Where(info => info.Key == licenseKey && info.ProductId == productID && info.OrderItem.Order.Company == companyName).FirstOrDefault();
            if (licenseKeyRecord != null && licenseKeyRecord.Password != null)
            {
                string strSHA512Hash = LicenseManagementInfo.GetSHA512Hash(licenseKeyRecord.Password);
                if (strSHA512Hash.CompareTo(hvalue) == 0)
                    strResponseMessage = "TRUE";
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(strResponseMessage, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        public HttpResponseMessage Get(string licenseKey, int productID, string companyName, string hvalue, string newpw)
        {
            string strResponseMessage = "FAIL";

            var licenseKeyRecord = db.LicenseKeys.Where(info => info.Key == licenseKey && info.ProductId == productID && info.OrderItem.Order.Company == companyName).FirstOrDefault();
            if (licenseKeyRecord != null && licenseKeyRecord.Password != null)
            {
                string strSHA512Hash = LicenseManagementInfo.GetSHA512Hash(licenseKeyRecord.Password);
                if (strSHA512Hash.CompareTo(hvalue) == 0)
                {
                    licenseKeyRecord.Password = newpw;
                    strResponseMessage = "TRUE";
                    db.Entry(licenseKeyRecord).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(strResponseMessage, System.Text.Encoding.UTF8, "text/plain")
            };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {

        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {

        }
    }
}

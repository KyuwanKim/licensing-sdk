﻿CREATE TABLE [dbo].[ExpiredActivations]
(
	[Id]             INT           IDENTITY (1, 1) NOT NULL,
    [ActivationKey]  VARCHAR (256) NOT NULL,
    [LicenseKeyId]   INT           NOT NULL,
    [HardwareId]     VARCHAR (64)  NOT NULL,
    [ExpirationDate] SMALLDATETIME NULL,
    [UnLicenseDate] SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_ExpiredActivations] PRIMARY KEY CLUSTERED ([Id] ASC)
)

SoftActivate Licensing SDK Readme
=================================

Overview
--------

SoftActivate Licensing SDK uses the latest advances in cryptography to provide you 
with one of the most secure and flexible license key management solutions on the market. 

Features
--------

Generates digitally signed, secure license keys yet compact and easy to type or dictate.
Digital signature guarantees that no unauthorized party can generate license keys. 
No illegal keygens are possible. 

Since common public key cryptography like RSA cannot be used to generate compact keys 
due to high signature length, SoftActivate SDK uses the latest public key cryptography algorithms 
using elliptic curves to ensure the highest levels of security and compactness. 

Allows you to generate keys bound to a specific computer and/or to a specific 
registration name, thus minimizing piracy. Unlike competition, you can also make this optional 
so that you can distribute your keys in large quantities, print them on retail boxes or CD's. 

You can store multiple fields of different types of data in the generated license keys. 
You can also validate your license keys against any information you choose, not just registration names 
or specific computers. 

Unlike competition, with SoftActivate SDK you have total control over the license keys. 
You can choose how your license keys look like and make tradeoffs between key security, 
key compactness and the amount of data contained by the key.

Use of XML license key templates greatly simplifies development and integration with your products. 
Of course, you can also do everything programatically. 

We realize that there are some highly demanding software companies that need absolute transparency 
over the licensing process. For select customers we can license the SDK source code, 
giving you access to the best of the best in license key management technologies. 

System Requirements
-------------------

SoftActivate Licensing SDK is built using both non-managed C++ and managed C#. 
It is portable and Unicode-compatible. It works on Windows XP/Vista/7. 
It does not need any third party components installed. 
To build the SDK samples, at least Microsoft Visual C++ 2008 Express is recommended 
(you can download it free of charge from Microsoft). 
Of course, Microsoft Visual Studio 2008 Standard or better is just as good.
Visual Studio 2010 is also supported.

For more information, please visit http://www.softactivate.com/
